/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CSector.hpp"
#include "CCell.hpp"


/**
 * Construit un secteur vide.
 */

CSector::CSector() :
m_blocsCount (0),
m_cells      (nullptr)
{

}


/**
 * Libère la mémoire occupée par le secteur.
 */

CSector::~CSector()
{
    delete m_cells;
}


/**
 * Donne le nombre de cellules non vides du secteur.
 *
 * \return Nombre de cellules pleines.
 */

int CSector::getBlocsCount() const
{
    return m_blocsCount;
}


/**
 * Indique si le secteur est vide.
 *
 * \return True si le secteur est vide, false sinon.
 */

bool CSector::isEmpty() const
{
    return (m_cells == nullptr);
}


/**
 * Réinitialise le secteur.
 */

void CSector::reset()
{
    delete m_cells;
    m_cells = nullptr;
    m_blocsCount = 0;
}


/**
 * Retourne une cellule.
 *
 * \param x Coordonnée X de la cellule dans le secteur.
 * \param y Coordonnée Y de la cellule dans le secteur.
 * \param z Coordonnée Z de la cellule dans le secteur.
 * \return Pointeur sur la cellule, ou nullptr si le secteur est vide.
 */

CCell * CSector::getCell(int x, int y, int z) const
{
    if (isEmpty())
    {
        return nullptr;
    }

    T_ASSERT(x >= 0);
    T_ASSERT(x < ChunkSize);
    T_ASSERT(y >= 0);
    T_ASSERT(y < ChunkSize);
    T_ASSERT(z >= 0);
    T_ASSERT(z < ChunkSize);

    return &m_cells[x + y * ChunkSize + z * ChunkSize * ChunkSize];
}


/**
 * Donne le contenu d'une cellule.
 */

TCellContent CSector::getCellContent(int x, int y, int z) const
{
    if (isEmpty())
    {
        return ContentEmpty;
    }

    T_ASSERT(x >= 0);
    T_ASSERT(x < ChunkSize);
    T_ASSERT(y >= 0);
    T_ASSERT(y < ChunkSize);
    T_ASSERT(z >= 0);
    T_ASSERT(z < ChunkSize);

    return static_cast<TCellContent>(m_cells[x + y * ChunkSize + z * ChunkSize * ChunkSize].content);
}


bool CSector::isCellEmpty(int x, int y, int z, bool waterIsEmpty, bool lavaIsEmpty) const
{
    if (isEmpty())
    {
        return true;
    }

    T_ASSERT(x >= 0);
    T_ASSERT(x < ChunkSize);
    T_ASSERT(y >= 0);
    T_ASSERT(y < ChunkSize);
    T_ASSERT(z >= 0);
    T_ASSERT(z < ChunkSize);

    if (m_cells[x + y * ChunkSize + z * ChunkSize * ChunkSize].content == ContentEmpty)
    {
        return true;
    }

    if (waterIsEmpty && m_cells[x + y * ChunkSize + z * ChunkSize * ChunkSize].isWater())
    {
        return true;
    }

    if (lavaIsEmpty && m_cells[x + y * ChunkSize + z * ChunkSize * ChunkSize].isLava())
    {
        return true;
    }

    return false;
}


void CSector::setCellContent(int x, int y, int z, TCellContent content, int data)
{
    T_ASSERT(x >= 0);
    T_ASSERT(x < ChunkSize);
    T_ASSERT(y >= 0);
    T_ASSERT(y < ChunkSize);
    T_ASSERT(z >= 0);
    T_ASSERT(z < ChunkSize);

    if (isEmpty())
    {
        if (content != ContentEmpty)
        {
            m_cells = new CCell[ChunkSize * ChunkSize * ChunkSize];
            m_cells[x + y * ChunkSize + z * ChunkSize * ChunkSize].content = content;
            m_cells[x + y * ChunkSize + z * ChunkSize * ChunkSize].data = data;
            m_blocsCount = 1;
        }

        return;
    }

    if (content != getCellContent(x, y, z))
    {
        if (content == ContentEmpty)
        {
            m_cells[x + y * ChunkSize + z * ChunkSize * ChunkSize].content = ContentEmpty;
            --m_blocsCount;

            // Destruction du secteur
            if (m_blocsCount == 0)
            {
                delete m_cells;
                m_cells = nullptr;
            }
        }
        else
        {
            if (getCellContent(x, y, z) == ContentEmpty)
            {
                ++m_blocsCount;
            }

            m_cells[x + y * ChunkSize + z * ChunkSize * ChunkSize].content = content;
            m_cells[x + y * ChunkSize + z * ChunkSize * ChunkSize].data = data;
        }
    }
}


int CSector::getCellLighting(int x, int y, int z) const
{
    if (isEmpty())
    {
        return 15;
    }

    T_ASSERT(x >= 0);
    T_ASSERT(x < ChunkSize);
    T_ASSERT(y >= 0);
    T_ASSERT(y < ChunkSize);
    T_ASSERT(z >= 0);
    T_ASSERT(z < ChunkSize);

    return m_cells[x + y * ChunkSize + z * ChunkSize * ChunkSize].lighting;
}


void CSector::setCellLighting(int x, int y, int z, int lighting)
{
    if (isEmpty())
    {
        return;
    }

    T_ASSERT(x >= 0);
    T_ASSERT(x < ChunkSize);
    T_ASSERT(y >= 0);
    T_ASSERT(y < ChunkSize);
    T_ASSERT(z >= 0);
    T_ASSERT(z < ChunkSize);

         if (lighting < 0)  lighting = 0;
    else if (lighting > 15) lighting = 15;

    m_cells[x + y * ChunkSize + z * ChunkSize * ChunkSize].lighting = lighting;
}


int CSector::getCellWaterLevel(int x, int y, int z) const
{
    if (isEmpty())
    {
        return false;
    }

    T_ASSERT(x >= 0);
    T_ASSERT(x < ChunkSize);
    T_ASSERT(y >= 0);
    T_ASSERT(y < ChunkSize);
    T_ASSERT(z >= 0);
    T_ASSERT(z < ChunkSize);

    return m_cells[x + y * ChunkSize + z * ChunkSize * ChunkSize].getWaterLevel();
}


void CSector::setWaterLevel(int x, int y, int z, int level)
{
    if (isEmpty())
    {
        return;
    }

    T_ASSERT(x >= 0);
    T_ASSERT(x < ChunkSize);
    T_ASSERT(y >= 0);
    T_ASSERT(y < ChunkSize);
    T_ASSERT(z >= 0);
    T_ASSERT(z < ChunkSize);

    m_cells[x + y * ChunkSize + z * ChunkSize * ChunkSize].data = ((level & 0x7) << 1);
}


int CSector::getCellLavaLevel(int x, int y, int z) const
{
    if (isEmpty())
    {
        return false;
    }

    T_ASSERT(x >= 0);
    T_ASSERT(x < ChunkSize);
    T_ASSERT(y >= 0);
    T_ASSERT(y < ChunkSize);
    T_ASSERT(z >= 0);
    T_ASSERT(z < ChunkSize);

    return m_cells[x + y * ChunkSize + z * ChunkSize * ChunkSize].getLavaLevel();
}


void CSector::setLavaLevel(int x, int y, int z, int level)
{
    if (isEmpty())
    {
        return;
    }

    T_ASSERT(x >= 0);
    T_ASSERT(x < ChunkSize);
    T_ASSERT(y >= 0);
    T_ASSERT(y < ChunkSize);
    T_ASSERT(z >= 0);
    T_ASSERT(z < ChunkSize);

    m_cells[x + y * ChunkSize + z * ChunkSize * ChunkSize].data = ((level & 0x3) << 2);
}
