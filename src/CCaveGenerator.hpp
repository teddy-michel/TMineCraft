/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef T_FILE_CCAVEGENERATOR_HPP
#define T_FILE_CCAVEGENERATOR_HPP

#include "IWorldGenerator.hpp"
#include "CPerlin.hpp"
#include "CWorld.hpp"


class CCaveGenerator : public IWorldGenerator
{
public:

    CCaveGenerator(int seed);
    virtual ~CCaveGenerator();

    virtual CCell getCellContent(int x, int y, int z) const;
    virtual TBiome getBiome(int x, int y) const;

private:

    CPerlin m_perlinCave1;
    CPerlin m_perlinCave2;
    CPerlin m_perlinCaveTurbX;
    CPerlin m_perlinCaveTurbY;
    CPerlin m_perlinCaveTurbZ;
};

#endif // T_FILE_CCAVEGENERATOR_HPP
