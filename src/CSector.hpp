/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef T_FILE_CSECTOR_HPP
#define T_FILE_CSECTOR_HPP

#include "CWorld.hpp"


class CCell;


/**
 * Un secteur est un cube de 16x16x16 cellules qui divise un chunk verticalement.
 */

class CSector
{
public:

    CSector();
    ~CSector();

    int getBlocsCount() const;
    bool isEmpty() const;
    void reset();

    CCell * getCell(int x, int y, int z) const;
    TCellContent getCellContent(int x, int y, int z) const;
    bool isCellEmpty(int x, int y, int z, bool waterIsEmpty = true, bool lavaIsEmpty = true) const;
    void setCellContent(int x, int y, int z, TCellContent content, int data = 0);

    // Luminosité
    int getCellLighting(int x, int y, int z) const;
    void setCellLighting(int x, int y, int z, int lighting);

    // Fluides
    int getCellWaterLevel(int x, int y, int z) const;
    void setWaterLevel(int x, int y, int z, int level);
    int getCellLavaLevel(int x, int y, int z) const;
    void setLavaLevel(int x, int y, int z, int level);

    // Utilisé uniquement pour la sauvegarde
    inline CCell * getCells() const { return m_cells; }

private:

    int m_blocsCount; ///< Nombre de blocs solides.
    CCell * m_cells;  ///< Cellules du secteur.
};

#endif // T_FILE_CSECTOR_HPP
