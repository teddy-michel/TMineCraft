/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CGame.hpp"
#include "CWorld.hpp"
#include "CPlayer.hpp"
#include "Core/Maths/CMatrix4.hpp"
#include "Core/Maths/MathsUtils.hpp"
#include <GL/glew.h>
#include <il.h>


using namespace Ted;


const double PiOver360 = 0.008726646259971f; ///< Pi sur 360.


/**
 * Constructeur par défaut.
 ******************************/

CGame::CGame() :
m_displayMode      (DisplayTexture),
m_mode             (ModeMenu),
m_verticalSynchro  (true),
m_displayAllChunks (true),
m_world            (nullptr),
m_player           (nullptr)
{
    // Touches par défaut
    m_keyActions[ActionForward]     = TKey(sf::Keyboard::Z);
    m_keyActions[ActionBackward]    = TKey(sf::Keyboard::S);
    m_keyActions[ActionStrafeLeft]  = TKey(sf::Keyboard::Q);
    m_keyActions[ActionStrafeRight] = TKey(sf::Keyboard::D);
    m_keyActions[ActionTurnLeft]    = TKey(sf::Keyboard::Left);
    m_keyActions[ActionTurnRight]   = TKey(sf::Keyboard::Right);
    m_keyActions[ActionJump]        = TKey(sf::Keyboard::Space);
    m_keyActions[ActionFly]         = TKey(sf::Keyboard::F);
    m_keyActions[ActionCrunch]      = TKey(sf::Keyboard::LShift);
    m_keyActions[ActionFire1]       = TKey(sf::Mouse::Left);
    m_keyActions[ActionFire2]       = TKey(sf::Mouse::Right);
    m_keyActions[ActionInventory]   = TKey(sf::Keyboard::E);

    m_keyActions[ActionModeTexture]     = TKey(sf::Keyboard::F5);
    m_keyActions[ActionModeLighting]    = TKey(sf::Keyboard::F6);
    m_keyActions[ActionModeBiome]       = TKey(sf::Keyboard::F7);
    m_keyActions[ActionModeTemperature] = TKey(sf::Keyboard::F8);
    m_keyActions[ActionModeHumidity]    = TKey(sf::Keyboard::F9);
}


/**
 * Libère la mémoire occupée par le jeu.
 */

CGame::~CGame()
{
    if (m_world)
    {
        m_world->saveWorld("saves");

        delete m_player;
        delete m_world;
    }
}


/**
 * Crée la fenêtre du jeu.
 *
 * \param width      Largeur de la fenêtre.
 * \param height     Hauteur de la fenêtre.
 * \param fullScreen True si l'application est en plein écran.
 */

void CGame::createWindow(int width, int height, bool fullScreen)
{
    if (fullScreen)
    {
        m_window.create(sf::VideoMode(width, height), "TMineCraft", sf::Style::Fullscreen);
    }
    else
    {
        m_window.create(sf::VideoMode(width, height), "TMineCraft");
    }

    m_window.setVerticalSyncEnabled(m_verticalSynchro);
    m_window.setMouseCursorVisible(m_mode != ModeGame);
}


int CGame::getWindowWidth() const
{
    return m_window.getSize().x;
}


int CGame::getWindowHeight() const
{
    return m_window.getSize().y;
}


/**
 * Modifie le mode d'affichage.
 *
 * \todo Recharger les chunks déjà chargés.
 */

void CGame::setDisplayMode(::TDisplayMode mode)
{
    if (mode != m_displayMode)
    {
        switch (mode)
        {
            default:
            case DisplayTexture:
                Game::renderer->setMode(Ted::Normal);
                break;
            case DisplayLighting:
                Game::renderer->setMode(Ted::Colored);
                break;
            case DisplayBiome:
                Game::renderer->setMode(Ted::Colored);
                break;
            case DisplayTemperature:
                Game::renderer->setMode(Ted::Colored);
                break;
            case DisplayHumidity:
                Game::renderer->setMode(Ted::Colored);
                break;
        }

        m_displayMode = mode;
        m_world->updateBuffers();
    }
}


void CGame::mainLoop()
{
    sf::ContextSettings settings = m_window.getSettings();

    std::cout << "Informations:" << std::endl;
    std::cout << " - Window size:        " << m_window.getSize().x << "x" << m_window.getSize().y << std::endl;
    std::cout << " - Vertical synchro:   " << (m_verticalSynchro ? "yes" : "no") << std::endl;
    std::cout << " - OpenGL version:     " << settings.majorVersion << "." << settings.minorVersion << std::endl;
    std::cout << " - Depth bits:         " << settings.depthBits << std::endl;
    std::cout << " - Stencil bits:       " << settings.stencilBits << std::endl;
    std::cout << " - Antialiasing level: " << settings.antialiasingLevel << std::endl;
    std::cout << std::endl;


    m_font.loadFromFile("textures/font.png");


    // Génération du monde
    m_world = new CWorld(Ted::RandInt(1, std::numeric_limits<int>::max()), this);
    //m_world->displayAllChunks(m_displayAllChunks);
    m_world->setNumVisibleChunks(2);
    m_world->build();

    // Création du joueur
    m_player = m_world->addPlayer("Player");


    double ratio = (double)m_window.getSize().x / (double)m_window.getSize().y;

    sf::Clock clock;

    sf::Vector2i cursorPos = sf::Mouse::getPosition(m_window);

    // Boucle évènementielle
    while (m_window.isOpen())
    {
        sf::Event event;

        while (m_window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                std::cout << "Fermeture du jeu" << std::endl;
                m_window.close();
            }
            else if (event.type == sf::Event::Resized)
            {
                glViewport(0, 0, event.size.width, event.size.height);
                ratio = (double)event.size.width / (double)event.size.height;
            }
            // Mouvement de la molette de la souris
            else if (event.type == sf::Event::MouseWheelMoved)
            {
                if (m_mode == ModeGame)
                {
                    if (event.mouseWheel.delta > 0)
                    {
                        m_player->setCurrentItem(m_player->getCurrentItem() - 1);
                    }
                    else
                    {
                        m_player->setCurrentItem(m_player->getCurrentItem() + 1);
                    }
                }
            }
            else if (event.type == sf::Event::KeyPressed)
            {
                // F4 : Quitter
                if (event.key.code == sf::Keyboard::F4)
                {
                    std::cout << "F4 was pressed: exit the application" << std::endl;
                    m_window.close();
                }
                // Echap : menu/jeu
                else if (event.key.code == sf::Keyboard::Escape)
                {
                    cursorPos = sf::Vector2i(m_window.getSize().x / 2, m_window.getSize().y / 2);
                    sf::Mouse::setPosition(cursorPos, m_window);

                    if (m_mode == ModeMenu || m_mode == ModeInventory)
                    {
                        m_window.setMouseCursorVisible(false);
                        m_mode = ModeGame;
                    }
                    else
                    {
                        m_window.setMouseCursorVisible(true);
                        m_mode = ModeMenu;
                    }
                }
                // Changement rapide d'élément dans l'inventaire
                else if (event.key.code >= sf::Keyboard::Num1 && event.key.code <= sf::Keyboard::Num9)
                {
                    if (m_mode == ModeGame)
                    {
                        m_player->setCurrentItem(event.key.code - sf::Keyboard::Num1);
                    }
                }
                // Touche E : inventaire
                else if (m_keyActions[ActionInventory].isKey && event.key.code == m_keyActions[ActionInventory].key)
                {
                    if (m_mode != ModeMenu)
                    {
                        cursorPos = sf::Vector2i(m_window.getSize().x / 2, m_window.getSize().y / 2);
                        sf::Mouse::setPosition(cursorPos, m_window);

                        if (m_mode == ModeInventory)
                        {
                            m_window.setMouseCursorVisible(false);
                            m_mode = ModeGame;
                        }
                        else
                        {
                            m_window.setMouseCursorVisible(true);
                            m_mode = ModeInventory;
                        }
                    }
                }
                // Touche : Fly
                else if (m_keyActions[ActionFly].isKey && event.key.code == m_keyActions[ActionFly].key && m_player)
                {
                    std::cout << "Player: FLY" << std::endl;
                    m_player->setFlying(!m_player->isFlying());
                }
                // Touche F1 : mode texturé
                else if (m_keyActions[ActionModeTexture].isKey && event.key.code == m_keyActions[ActionModeTexture].key)
                {
                    std::cout << "Set DisplayMode : texture" << std::endl;
                    setDisplayMode(DisplayTexture);
                }
                // Touche F2 : mode texturé
                else if (m_keyActions[ActionModeLighting].isKey && event.key.code == m_keyActions[ActionModeLighting].key)
                {
                    std::cout << "Set DisplayMode : lighting" << std::endl;
                    setDisplayMode(DisplayLighting);
                }
                // Touche F3 : mode texturé
                else if (m_keyActions[ActionModeBiome].isKey && event.key.code == m_keyActions[ActionModeBiome].key)
                {
                    std::cout << "Set DisplayMode : biome" << std::endl;
                    setDisplayMode(DisplayBiome);
                }
                // Touche F4 : mode texturé
                else if (m_keyActions[ActionModeTemperature].isKey && event.key.code == m_keyActions[ActionModeTemperature].key)
                {
                    std::cout << "Set DisplayMode : temperature" << std::endl;
                    setDisplayMode(DisplayTemperature);
                }
                // Touche F5 : mode texturé
                else if (m_keyActions[ActionModeHumidity].isKey && event.key.code == m_keyActions[ActionModeHumidity].key)
                {
                    std::cout << "Set DisplayMode : humidity" << std::endl;
                    setDisplayMode(DisplayHumidity);
                }
            }
            else if (event.type == sf::Event::LostFocus)
            {
                if (m_mode != ModeMenu)
                {
                    cursorPos = sf::Vector2i(m_window.getSize().x / 2, m_window.getSize().y / 2);
                    sf::Mouse::setPosition(cursorPos, m_window);
                    m_window.setMouseCursorVisible(true);
                    m_mode = ModeMenu;
                }
            }
            else if (event.type == sf::Event::MouseMoved)
            {
                sf::Vector2i cursorNewPos = sf::Vector2i(event.mouseMove.x, event.mouseMove.y);
                sf::Vector2i cursorRel = cursorNewPos - cursorPos;

                if (m_mode != ModeGame)
                {
                    cursorPos = cursorNewPos;
                }
                // On place le curseur au centre de l'écran
                else if (cursorRel != sf::Vector2i(0, 0))
                {
                    sf::Vector2i windowCenter(m_window.getSize().x / 2, m_window.getSize().y / 2);
                    sf::Mouse::setPosition(windowCenter, m_window);

                    m_player->updateDirection(cursorRel.x, cursorRel.y);
                }
            }
            else if (event.type == sf::Event::MouseButtonPressed)
            {
                if (m_mode == ModeMenu)
                {
                    clickOnMenu(event.mouseButton.x, event.mouseButton.y, event.mouseButton.button);
                }
                else if (m_mode == ModeInventory)
                {
                    m_player->clickOnInventory(event.mouseButton.x, event.mouseButton.y, event.mouseButton.button);
                }
            }
        }

        // Durée de la frame
        unsigned int frameTime = clock.restart().asMilliseconds();


        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        _gluPerspective(70, ratio, 0.1f, 10000.0f);

        // On efface les buffers
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        if (m_mode == ModeGame)
        {
            m_world->update(frameTime);
            m_player->doActions(frameTime);
        }

        // Déplacement de la caméra
        m_player->updateCamera();


        // Affichage du monde
        if (m_world && m_player)
        {
            m_world->display();
            m_player->display();
            m_world->displayBlend();

            drawRepere(1.0f);
        }

        begin2D();

        if (m_world && m_player)
            m_player->displayFluid();

        // Affichage du HUD
        if (m_mode == ModeMenu)
        {
            displayMenu();
        }
        else if (m_world && m_player)
        {
            m_player->displayHUD();
/*
            if (m_mode == ModeInventory)
            {
                //...
            }
*/
        }

        end2D();

        // Inversion des buffers
        glFlush(); // Apparemment ça ne sert à rien (sous Windows en tout cas)

        // Mise-à-jour des textures
        Game::textureManager->doPendingTasks();

        m_window.display();
    }
}


bool CGame::initOpenGL() const
{
    if (glewInit() != GLEW_OK)
    {
        std::cerr << "Error with glew init" << std::endl;
        return false;
    }

    // Test de profondeur
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    // Couleurs
    glEnable(GL_COLOR_MATERIAL);

    // Textures
    glEnable(GL_TEXTURE_2D);
    glHint(GL_GENERATE_MIPMAP_HINT, GL_NICEST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    // Transparence
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0.0f);

    // Autres paramètres
    glShadeModel(GL_SMOOTH);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glHint(GL_TEXTURE_COMPRESSION_HINT, GL_FASTEST);

    glCullFace(GL_FRONT);


    // Initialisation de DevIL
    ilInit();
    ilOriginFunc(IL_ORIGIN_UPPER_LEFT);
    ilEnable(IL_ORIGIN_SET);
    ilEnable(IL_FILE_OVERWRITE);
    ilSetInteger(IL_FORMAT_MODE, IL_RGBA);
    ilEnable(IL_FORMAT_SET);

    return true;
}


/**
 * Donne l'action associée à une touche.
 *
 * \param key Touche recherchée.
 * \return Action associée.
 ******************************/

TAction CGame::getActionForKey(TKey key) const
{
    for (std::map<TAction, TKey>::const_iterator it = m_keyActions.begin(); it != m_keyActions.end(); ++it)
    {
        if (it->second == key)
        {
            return it->first;
        }
    }

    return ActionNone;
}


/**
 * Donne la touche associée à une action.
 *
 * \param action Action recherchée.
 * \return Touche associée.
 ******************************/

TKey CGame::getKeyForAction(TAction action) const
{
    std::map<TAction, TKey>::const_iterator it = m_keyActions.find(action);

    if (it != m_keyActions.end())
    {
        return it->second;
    }

    return TKey(sf::Keyboard::Unknown);
}


/**
 * Indique si la touche associée à une action est actuellement enfoncée.
 *
 * \param action Action recherchée.
 * \return Booléen.
 ******************************/

bool CGame::isActionActive(TAction action) const
{
    if (action == ActionNone)
    {
        return false;
    }

    std::map<TAction, TKey>::const_iterator it = m_keyActions.find(action);

    if (it != m_keyActions.end())
    {
        if (it->second.isKey)
        {
            return sf::Keyboard::isKeyPressed(it->second.key);
        }
        else
        {
            return sf::Mouse::isButtonPressed(it->second.button);
        }
    }

    return false;
}


/**
 * Change la touche associée à une action.
 *
 * \param action Action à modifier.
 * \param key    Touche à utiliser.
 ******************************/

void CGame::setKeyForAction(TAction action, TKey key)
{
    // On cherche si une autre action utilise cette touche
    for (std::map<TAction, TKey>::iterator it = m_keyActions.begin(); it != m_keyActions.end(); ++it)
    {
        if (it->second == key)
        {
            it->second = TKey(sf::Keyboard::Unknown);
        }
    }

    m_keyActions[action] = key;
}


/**
 * Affiche le repère en 3D.
 * L'axe X est en rouge, l'axe Y en vert, l'axe Z en bleu.
 *
 * \param length Longueur de chaque axe.
 */

void CGame::drawRepere(float length) const
{
    glPushAttrib(GL_ALL_ATTRIB_BITS);
    glPushMatrix();

    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);
    glEnable(GL_LINE_SMOOTH);
    glLineWidth(2);

    // Repère
    glBegin(GL_LINES);

        glColor3ub(255, 0, 0);
        glVertex3f(  0.0f,   0.0f,   0.0f);
        glVertex3f(length,   0.0f,   0.0f);

        glColor3ub(0, 255, 0);
        glVertex3f(  0.0f,   0.0f,   0.0f);
        glVertex3f(  0.0f, length,   0.0f);

        glColor3ub(0, 0, 255);
        glVertex3f(  0.0f,   0.0f,   0.0f);
        glVertex3f(  0.0f,   0.0f, length);


    glEnd();

    glPopMatrix();
    glPopAttrib();
}


void CGame::begin2D() const
{
    // On désactive les textures et le test de profondeur
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_TEXTURE_2D);

    // Sauvegarde de la matrice
    glPushMatrix();

    // On met en projection orthogonale
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0, m_window.getSize().x, m_window.getSize().y, 0, -1, 1);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}


void CGame::end2D() const
{
    // On revient à une projection en perspective
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // Restauration de la matrice précedemment enregistrée
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);
}


void CGame::displayMenu() const
{
    // Fond gris transparent
    glBegin(GL_TRIANGLES);

        glColor4ub(0, 0, 0, 150);

        glVertex2i(0, 0);
        glVertex2i(m_window.getSize().x, 0);
        glVertex2i(0, m_window.getSize().y);

        glVertex2i(0, m_window.getSize().y);
        glVertex2i(m_window.getSize().x, 0);
        glVertex2i(m_window.getSize().x, m_window.getSize().y);

    glEnd();

    m_font.displayText("New world",  100, 100, CColor::Blue);
    m_font.displayText("Load world", 100, 140, CColor::Blue);
    m_font.displayText("Exit",       100, 180, CColor::Blue);
}


/**
 * Déplace la caméra. Cette fonction est équivalente à gluLookAt.
 *
 * \todo Pouvoir préciser l'axe vertical.
 *
 * \param position Position de la caméra.
 * \param view     Direction d'observation.
 ******************************/

void _gluLookAt(const TVector3F& position, const TVector3F& view)
{
    TVector3F f = view - position;
    f.normalize();

    TVector3F s = VectorCross(f, TVector3F(0.0f, 0.0f, 1.0f));
    s.normalize();

    TVector3F u = VectorCross(s, f);
    u.normalize();

    TMatrix4F M(s.X ,  u.X, -f.X, 0.0f,
                s.Y ,  u.Y, -f.Y, 0.0f,
                s.Z ,  u.Z, -f.Z, 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f);

    glMultMatrixf(&M[0]);
    glTranslatef(-position.X, -position.Y, -position.Z);
}


/**
 * Modifie les options de perspective. Cette fonction est équivalente à gluPerpective.
 *
 * \param fovy   Angle de vue.
 * \param aspect Ratio.
 * \param znear  Distance minimale visible.
 * \param zfar   Distance maximale visible.
 ******************************/

void _gluPerspective(double fovy, double aspect, double znear, double zfar)
{
    if (std::abs(aspect) < std::numeric_limits<float>::epsilon())
    {
        return;
    }

    double deltaZ = zfar - znear;

    if (std::abs(deltaZ) < std::numeric_limits<float>::epsilon())
    {
        return;
    }

    double radians = fovy * PiOver360;
    double sine = ::sin(radians);

    if (std::abs(sine) < std::numeric_limits<float>::epsilon())
    {
        return;
    }

    double cotangent = ::cos(radians) / sine;

    // Make m an identity matrix
    double m[4][4] = { { 0.0f } };

    m[0][0] = cotangent / aspect;
    m[1][1] = cotangent;
    m[2][2] = -(zfar + znear) / deltaZ;
    m[2][3] = -1.0f;
    m[3][2] = -2.0f * znear * zfar / deltaZ;

    glMultMatrixd(&m[0][0]);
}


void CGame::clickOnMenu(int x, int y, sf::Mouse::Button button)
{
    // TODO
}
