/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef T_FILE_CONTAINER_HPP
#define T_FILE_CONTAINER_HPP

#include <stdint.h>


const int InventoryItemSize = 32;


#include "Core/struct_alignment_start.h"

struct CInventorySlot
{
    uint32_t type:12; ///< Type d'objet.
    uint32_t data:12; ///< Données supplémentaires.
    uint32_t count:8; ///< Nombre d'objets.

    CInventorySlot() : type(0), data(0), count (0) { }
};

#include "Core/struct_alignment_end.h"


class CWorld;
class CFont;


void displayContainerSlot(CWorld * world, const CFont * font, int x, int y, int type, int count, int data = 0);
void drawBloc(CWorld * world, int x, int y, int type, int data = 0);
void drawEntity(CWorld * world, int x, int y, int type, int data = 0);


#endif // T_FILE_CONTAINER_HPP
