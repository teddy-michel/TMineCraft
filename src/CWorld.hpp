/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef T_FILE_CWORLD_HPP
#define T_FILE_CWORLD_HPP

#include <vector>
#include <queue>
#include <map>

#include "Core/Maths/CVector3.hpp"
#include "Graphic/CBuffer.hpp"
#include "CCell.hpp"


// Paramètres du monde
const float CellSize = 1.0f;
const int ChunkSize = 16; // SYNCHRO
const int SectorsInChunk = 16; // SYNCHRO
const int ChunkHeight = ChunkSize * SectorsInChunk; // SYNCHRO

const int WaterLevel = 64;

const int UpdatePeriod = 500;


/// Liste des entités.
enum TEntity
{
    EntityUnknown  = 0x00
};


#include "Biome.hpp"

Ted::CString getBiomeName(TBiome biome);


/// Liste des textures.
enum TTexture
{
    TextureNone        = 0x00,
    TextureRock        = 0x01,
    TextureDirt        = 0x02,
    TextureGems        = 0x03,
    TextureGrass       = 0x04,
    TextureGrassSide   = 0x05,
    TextureWater       = 0x06,
    TextureLava        = 0x07,
    TextureSand        = 0x08,
    TextureBrick       = 0x09,
    TextureBedRock     = 0x0A,
    TextureCobbleStone = 0x0B,
    TextureWood        = 0x0C,
    TextureWoodSide    = 0x0D,
    TextureWoodenPlank = 0x0E,
    TextureFurnaceFace = 0x0F,
    TextureFurnaceSide = 0x10,
    TextureFurnaceTop  = 0x11,

    // Nombre de textures
    TextureCount
};


///< Liste des faces d'un cube.
enum TFace
{
    FaceUnknown = 0, ///< Face inconnue.
    FaceNorth   = 1, ///< Face nord (y > 0).
    FaceEast    = 2, ///< Face est (x > 0).
    FaceSouth   = 3, ///< Face sud (y < 0).
    FaceWest    = 4, ///< Face ouest (x < 0).
    FaceTop     = 5, ///< Face supérieure (z > 0).
    FaceBottom  = 6  ///< Face inférieure (z < 0).
};


class CGame;
class CChunk;
class CPlayer;
class IWorldGenerator;
class CSector;


/**
 * Classe représentant le monde, avec ses blocs et ses objets.
 */

class CWorld
{
public:

    CWorld(int seed, CGame * game);
    ~CWorld();

    void build();
    void updateBuffers();
    void display();
    void displayBlend();
    void update(int frameTime);

    inline CGame * getGame() const;
    int getSeed() const;
    inline Ted::TTextureId getTextureId(TTexture texture) const;

    CChunk * loadChunk(int x, int y);
    CChunk * getChunk(int x, int y) const;
    CChunk * getChunk(int x, int y, bool load = false);
    bool isChunkLoaded(int x, int y) const;

    void setCurrentChunk(int chunkX, int chunkY);

    inline void displayAllChunks(bool display);
    inline void setNumVisibleChunks(int numVisibleChunks);

    CCell * getCell(int x, int y, int z) const;
    TCellContent getCellContent(int x, int y, int z) const;
    bool isCellEmpty(int x, int y, int z, bool waterIsEmpty = true) const;
    bool isUnderWater(const Ted::TVector3F& position) const;
    bool isUnderLava(const Ted::TVector3F& position) const;
    int getHeight(int x, int y) const;

    bool findIntersection(const Ted::TVector3F& origin, const Ted::TVector3F& direction, Ted::TVector3F& iPoint,
                          TFace& iFace, Ted::TVector3I& iCube, double maxLength = 4.0f * CellSize);
    int findFirstZ(int x, int y, int z) const;

    TCellContent removeCell(int x, int y, int z);
    bool addCell(int x, int y, int z, TCellContent content, int data = 0);

    bool loadWorld(const Ted::CString& folder);
    bool saveWorld(const Ted::CString& folder) const;

    CPlayer * addPlayer(const Ted::CString& name);

    inline bool isRunning() const;

    // Génération du terrain
/*
    int getElevation(int x, int y) const
    {
        return m_generator->getElevation(x, y);
    }
*/
    void buildChunk(int chunkX, int chunkY, CSector sectors[SectorsInChunk], uint8_t * heightMap);
    CCell buildCellContent(int x, int y, int z) const;
    float getTemperature(int x, int y) const;
    float getHumidity(int x, int y) const;
    TBiome getBiome(int x, int y) const;

private:

    void resetWorld();
    void loadTextures();
    void savePlayers(const Ted::CString& folder) const;

    static void loadChunksAsynchronous(CWorld * world);


    // Format du fichier

    struct THeader
    {
        int32_t ident;          ///< Identifiant du format de fichier (TMCW).
        int32_t version;        ///< Version du format de fichier (1).
        char name[64];          ///< Nom du monde.
        int32_t seed;           ///< Graine utilisée pour générer le monde.
        Ted::TVector3I spawn;   ///< Coordonnées du point d'apparition.
        uint32_t time;          ///< Heure dans le monde en millisecondes.
        uint32_t numChunks;     ///< Nombre de chunks dans le fichier.
        uint32_t offsetChunks;  ///< Offset de la liste des chunks.
    };

    struct TChunk
    {
        int32_t x;
        int32_t y;
        int16_t sectors;        ///< 1 bit pour indiquer si chaque secteur est vide (0 = vide).
        uint32_t offsetSectors; ///< Offset de la liste des secteurs.
        uint8_t heightMap[ChunkSize * ChunkSize];
    };

    struct TSector
    {
        int16_t cells[ChunkSize * ChunkSize * ChunkSize]; ///< Cellules du secteur.
    };


    CGame * m_game;
    IWorldGenerator * m_generator;

    sf::Thread m_loadThread;
    sf::Mutex m_loadMutex;
    bool m_isRunning;

    Ted::CString m_name;     ///< Nom du monde.
    bool m_displayAllChunks; ///< Indique si on doit afficher tous les chunks chargés.
    int m_numVisibleChunks;  ///< Nombre de chunks visibles.
    Ted::TVector3I m_spawn;  ///< Coordonnées du point d'apparition.

    int m_currentChunkX;     ///< Coordonnée X du chunk courant. (déplacer vers CPlayer ?)
    int m_currentChunkY;     ///< Coordonnée Y du chunk courant. (déplacer vers CPlayer ?)
    uint32_t m_time;         ///< Heure dans le jeu (en millisecondes, représente 28 jours).
    Ted::TTextureId m_texturesId[TextureCount];
    std::list<CPlayer *> m_players; ///< Joueurs présents dans le monde.

    // TODO: fusionner ces deux attributs en un seul
    std::map<int, std::map<int, CChunk *> > m_chunks; ///< Chunks chargés en mémoire.
    std::queue<CChunk *> m_chunksUpdate; ///< Chunks à charger.
};


inline CGame * CWorld::getGame() const
{
    return m_game;
}


inline Ted::TTextureId CWorld::getTextureId(TTexture texture) const
{
    return m_texturesId[texture];
}


inline void CWorld::displayAllChunks(bool dis)
{
    m_displayAllChunks = dis;
}


inline void CWorld::setNumVisibleChunks(int numVisibleChunks)
{
    if (numVisibleChunks < 1)
    {
        numVisibleChunks = 1;
    }
    else if (numVisibleChunks > 32)
    {
        numVisibleChunks = 32;
    }

    m_numVisibleChunks = numVisibleChunks;
}


inline bool CWorld::isRunning() const
{
    return m_isRunning;
}

#endif // T_FILE_CWORLD_HPP
