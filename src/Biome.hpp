/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef T_FILE_BIOME_HPP
#define T_FILE_BIOME_HPP


/// Liste des biomes.
enum TBiome
{
    BiomePlains         = 0x01,
    BiomeDesert         = 0x02,
    BiomeForest         = 0x03,
    BiomeSavanna        = 0x04,
    BiomeTundra         = 0x05,
    BiomeRainForest     = 0x06,
    BiomeIceDesert      = 0x07,
    BiomeTaiga          = 0x08,
    BiomeSeasonalForest = 0x09,
    BiomeSwampland      = 0x0A,
    BiomeShrubland      = 0x0B,
    BiomeOcean          = 0x0C,
    BiomeBeach          = 0x0D
};

#endif // T_FILE_BIOME_HPP
