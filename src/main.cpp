/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CGame.hpp"
#include "Core/CApplication.hpp"
#include "Graphic/CRenderer.hpp"

#include "CPerlin.hpp"
#include "Core/Maths/MathsUtils.hpp"
#include "Graphic/CImage.hpp"


using namespace Ted;


int main(int argc, char * argv[])
{
    /// \todo Utiliser les arguments du programme :
    ///        -w : largeur de la fenêtre
    ///        -h : hauteur de la fenêtre
    ///        -f : plein écran
    ///        -v : synchro verticale
    ///        -s : seed


    CApplication app("TMineCraft");

    // Liste des arguments
    app.sendArgs(argc, argv);
    new CRenderer();

/*
    // Test : enregistrement d'une image
    CPerlin perlin(1, 0.01f, 1, Ted::RandInt(1, std::numeric_limits<int>::max()));
    CImage image(1024, 1024);
    for (int x = 0; x < 1024; ++x)
    {
        for (int y = 0; y < 1024; ++y)
        {
            float v = perlin.Get(x, y);
            //std::cout << v << std::endl;
            if (v > -0.02f && v < 0.02f)
                image.setPixel(x, y, CColor(255, 255, 255));
            else
                image.setPixel(x, y, CColor(0, 0, 0));
        }
    }
    image.saveToFile("image.png");
    return EXIT_SUCCESS;
*/

    CGame game;

    game.createWindow(800, 600, false);

    if (!game.initOpenGL())
    {
        return EXIT_FAILURE;
    }

    game.mainLoop();

    return EXIT_SUCCESS;
}
