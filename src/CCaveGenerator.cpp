/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CCaveGenerator.hpp"


CCaveGenerator::CCaveGenerator(int seed) :
IWorldGenerator (seed),
m_perlinCave1     (1, 0.01f, 1, m_seed + 0),
m_perlinCave2     (1, 0.01f, 1, m_seed + 1),
m_perlinCaveTurbX (3, 0.3f, 1, m_seed + 2),
m_perlinCaveTurbY (3, 0.3f, 1, m_seed + 3),
m_perlinCaveTurbZ (3, 0.3f, 1, m_seed + 4)
{

}


CCaveGenerator::~CCaveGenerator()
{

}


CCell CCaveGenerator::getCellContent(int x, int y, int z) const
{
    if (z <= 0)
    {
        return CCell(ContentBedRock);
    }

    if (z >= ChunkHeight)
    {
        return CCell(ContentEmpty);
    }
/*
    float valCave = m_perlinCave1.Get(x, y, z);

    if (valCave > -0.02f && valCave < 0.02f)
        return CCell(ContentCobbleStone);
    else
        return CCell(ContentEmpty);
*/
    float valCave1 = m_perlinCave1.Get(x + m_perlinCaveTurbX.Get(x) * 3, y + m_perlinCaveTurbY.Get(y) * 3, z + m_perlinCaveTurbZ.Get(z) * 3);
    float valCaveBase1 = 0.0f;
    if (valCave1 > -0.02f && valCave1 < 0.02f)
        valCaveBase1 = 1.0f;
    else
        valCaveBase1 = 0.0f;

    float valCave2 = m_perlinCave2.Get(x + m_perlinCaveTurbX.Get(x) * 3, y + m_perlinCaveTurbY.Get(y) * 3, z + m_perlinCaveTurbZ.Get(z) * 3);
    float valCaveBase2 = 0.0f;
    if (valCave2 > -0.02f && valCave2 < 0.02f)
        valCaveBase2 = 1.0f;
    else
        valCaveBase2 = 0.0f;

    float valCaveBase = valCaveBase1 * valCaveBase2;
    if (valCaveBase > 0.0f)
        return CCell(ContentCobbleStone);
    else
        return CCell(ContentEmpty);
/*
    float valCave1 = m_perlinCave1.Get(x, y, z) + 1.0f;
    float valCave2 = m_perlinCave2.Get(x, y, z) + 1.0f;
    float valCave = valCave1 * valCave2 * 0.25f - 0.5f;

    if (valCave > -0.02f && valCave < 0.02f)
        return CCell(ContentCobbleStone);
    else
        return CCell(ContentEmpty);
*/
}


/**
 * Donne le biome d'une cellule du monde.
 *
 * \param x Coordonnée X de la cellule dans le monde.
 * \param y Coordonnée Y de la cellule dans le monde.
 * \return Biome auquel appartient la cellule, déterminé à partir de
 *         sa température et de son humidité.
 */

TBiome CCaveGenerator::getBiome(int x, int y) const
{
    return BiomeForest;
}
