/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CFont.hpp"
#include <GL/glew.h>
#include "Graphic/CImage.hpp"
#include "Graphic/CRenderer.hpp"
#include "Graphic/CTextureManager.hpp"


using namespace Ted;


CFont::CFont() :
m_textureId (0),
m_size      (0)
{
    //...
}


CFont::~CFont()
{
    std::cout << "Suppression de la police de caractere" << std::endl;

    // Suppression de la texture
    if (m_textureId != 0)
    {
        glDeleteTextures(1, &m_textureId);
    }
}


bool CFont::loadFromFile(const CString& fileName)
{
    std::cout << "Chargement de la police de caractere" << std::endl;

    // Suppression de la texture
    if (m_textureId != 0)
    {
        glDeleteTextures(1, &m_textureId);
    }

    GLenum format = GL_RGBA;
    CImage image;

    if (image.loadFromFile(fileName))
    {
        glGenTextures(1, &m_textureId);
        glBindTexture(GL_TEXTURE_2D, m_textureId);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

        glTexImage2D(GL_TEXTURE_2D, 0, format, image.getWidth(), image.getHeight(), 0, format, GL_UNSIGNED_BYTE, image.getPixels());
/*
#if defined(GL_ARB_framebuffer_object) // OpenGL > 3.0 : glGenerateMipmap
//#   if defined(GL_ARB_texture_storage) //glTexStorage2D
//        glTexStorage2D(GL_TEXTURE_2D, 2, GL_RGBA8UI, image.getWidth(), image.getHeight());
//        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, image.getWidth(), image.getHeight(), format, GL_UNSIGNED_BYTE, image.getPixels());
//#   else
        glTexImage2D(GL_TEXTURE_2D, 0, format, image.getWidth(), image.getHeight(), 0, format, GL_UNSIGNED_BYTE, image.getPixels());
//#   endif
        glGenerateMipmap(GL_TEXTURE_2D);
#elif defined(GL_VERSION_1_4) // OpenGL > 1.4 : GL_GENERATE_MIPMAP
        glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
        glTexImage2D(GL_TEXTURE_2D, 0, format, image.getWidth(), image.getHeight(), 0, format, GL_UNSIGNED_BYTE, image.getPixels());
#else
        // On utilise gluBuild2DMipmaps si on n'a vraiment pas le choix...
        if (int error = gluBuild2DMipmaps(GL_TEXTURE_2D, format, image.getWidth(), image.getHeight(), format, GL_UNSIGNED_BYTE, image.getPixels()))
        {
            std::cerr << "Error with texture mipmap generation" << std::endl;
        }
#endif
*/
        m_size = image.getHeight() / 16;

        return true;
    }
    else
    {
        std::cerr << "impossible de charger la texture " << fileName << std::endl;
        return false;
    }
}


/**
 * Affiche du texte à l'écran.
 *
 * \param text  Texte à afficher.
 * \param x     Coordonnée horizontale.
 * \param y     Coordonnée verticale.
 * \param color Couleur du texte.
 */

void CFont::displayText(const CString& text, int xt, int yt, const CColor& color) const
{
    if (m_textureId == 0 || text.isEmpty())
    {
        return;
    }

    glEnable(GL_TEXTURE_2D);
    glActiveTextureARB(GL_TEXTURE0_ARB);
    glClientActiveTextureARB(GL_TEXTURE0_ARB);
    Game::textureManager->bindTexture(0);
    glBindTexture(GL_TEXTURE_2D, m_textureId);

    glColor4ub(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());

    float posX = xt;
    float posY = yt;

    // Parcours de la chaîne de caractères
    for (CString::const_iterator it = text.cbegin(); it != text.cend(); ++it)
    {
        unsigned char c = it->toLatin1();

        switch (c)
        {
            // Saut de ligne
            case '\n':
            case '\r':
                posX = xt;
                posY += m_size;

            // Espace
            case ' ':
                posX += m_size;
                continue;

            // Tabulation horizontale
            case '\t':
                posX += 4.0f * m_size;
                continue;

            default:
            {
                float t1 = (c % 16) * 0.0625f;
                float t2 = (c / 16) * 0.0625f;

                glBegin(GL_TRIANGLES);

                    glTexCoord2f(t1 + 0.0000f, t2 + 0.0000f); glVertex2i(posX, posY);
                    glTexCoord2f(t1 + 0.0625f, t2 + 0.0000f); glVertex2i(posX + m_size, posY);
                    glTexCoord2f(t1 + 0.0000f, t2 + 0.0625f); glVertex2i(posX, posY + m_size);

                    glTexCoord2f(t1 + 0.0000f, t2 + 0.0625f); glVertex2i(posX, posY + m_size);
                    glTexCoord2f(t1 + 0.0625f, t2 + 0.0000f); glVertex2i(posX + m_size, posY);
                    glTexCoord2f(t1 + 0.0625f, t2 + 0.0625f); glVertex2i(posX + m_size, posY + m_size);

                glEnd();

                break;
            }
        }

        // Incrémentation de la position
        posX += m_size;
    }

    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
}
