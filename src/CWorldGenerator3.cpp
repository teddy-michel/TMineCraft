/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CWorldGenerator3.hpp"
#include "CSector.hpp"


const int HeightMin = 32;
const int HeightMax = 128;


CWorldGenerator3::CWorldGenerator3(int seed) :
IWorldGenerator     (seed),
m_perlinDensity     (16, 0.0048f, 1, m_seed + 0),
m_perlinTemperature (16, 0.0024f, 1, m_seed + 1),
m_perlinHumidity    (16, 0.0024f, 1, m_seed + 2),
m_perlinCave1       (1, 0.01f, 1, m_seed + 3),
m_perlinCave2       (1, 0.01f, 1, m_seed + 4),
m_perlinCaveTurbX   (3, 0.3f, 1, m_seed + 5),
m_perlinCaveTurbY   (3, 0.3f, 1, m_seed + 6),
m_perlinCaveTurbZ   (3, 0.3f, 1, m_seed + 7)
{

}


CWorldGenerator3::~CWorldGenerator3()
{

}


void CWorldGenerator3::buildChunk(int chunkMinX, int chunkMinY, CSector sectors[SectorsInChunk], uint8_t * heightMap)
{
    for (int x = 0; x < ChunkSize; ++x)
    {
        for (int y = 0; y < ChunkSize; ++y)
        {
            int height = 0;

            for (int sector = 0; sector < SectorsInChunk; ++sector)
            {
                for (int z = 0; z < ChunkSize; ++z)
                {
                    TCellContent content = ContentEmpty;

                    if (sector * ChunkSize + z <= 0)
                    {
                        content = ContentBedRock;
                    }
                    else if (sector * ChunkSize + z < HeightMin)
                    {
                        content = ContentRock;
                    }
                    else if (sector * ChunkSize + z > HeightMax)
                    {
                        content = ContentEmpty;
                    }
                    else
                    {
                        float density = getDensity(chunkMinX + x, chunkMinY + y, sector * ChunkSize + z);

                        if (density > 0.0f)
                        {
                            content = ContentRock;
                        }
                        else
                        {
                            if (sector * ChunkSize + z <= WaterLevel)
                            {
                                content = ContentWaterSource;
                            }
                            else
                            {
                                content = ContentEmpty;
                            }
                        }
                    }

                    sectors[sector].setCellContent(x, y, z, content);

                    if (content != ContentEmpty && content != ContentWater && content != ContentWaterSource)
                    {
                        height = sector * ChunkSize + z;
                    }
                }
            }

            {
                int sector = height / ChunkSize;
                int z = height % ChunkSize;
                CCell * cell = sectors[sector].getCell(x, y, z);
                if (cell != nullptr && !cell->isEmpty() && !cell->isWater())
                {
                    if (sector * ChunkSize + z < WaterLevel)
                    {
                        sectors[sector].setCellContent(x, y, z, ContentDirt);
                    }
                    else
                    {
                        sectors[sector].setCellContent(x, y, z, ContentGrass);
                    }
                }
            }
            {
                int sector = (height - 1) / ChunkSize;
                int z = (height - 1) % ChunkSize;
                CCell * cell = sectors[sector].getCell(x, y, z);
                if (cell != nullptr && !cell->isEmpty() && !cell->isWater())
                {
                    sectors[sector].setCellContent(x, y, z, ContentDirt);
                }
            }

            // Cavernes
            for (int sector = 0; sector < SectorsInChunk; ++sector)
            {
                for (int z = 0; z < ChunkSize; ++z)
                {
                    if (isCave(chunkMinX + x, chunkMinY + y, sector * ChunkSize + z))
                    {
                        TCellContent content = sectors[sector].getCellContent(x, y, z);
                        if (content != ContentEmpty && content != ContentWater && content != ContentWaterSource)
                        {
                            sectors[sector].setCellContent(x, y, z, ContentEmpty);
                        }
                    }
                }
            }

            // Calcul de la heightmap
            height = 0;

            for (int sector = 0; sector < SectorsInChunk; ++sector)
            {
                for (int z = 0; z < ChunkSize; ++z)
                {
                    TCellContent content = sectors[sector].getCellContent(x, y, z);
                    if (content != ContentEmpty && content != ContentWater && content != ContentWaterSource)
                    {
                        height = sector * ChunkSize + z;
                    }
                }
            }

            heightMap[x + y * ChunkSize] = height;
        }
    }
}


bool CWorldGenerator3::isCave(int x, int y, int z) const
{
    T_ASSERT(z >= 0);
    T_ASSERT(z <= ChunkHeight);

    float grad = 0.0f;

    if (z >= (100 + 2) / 2)
    {
        grad = static_cast<float>(100 - 1 - z) / static_cast<float>((100 - 2) / 2 - 1);
    }
    else
    {
        grad = static_cast<float>(z - 2) / static_cast<float>((100 + 1 - 2) / 2 - 1);
    }

    if (grad <= 0.0f)
    {
        return false;
    }

    float valCave1 = m_perlinCave1.Get(x + m_perlinCaveTurbX.Get(x) * 3,
                                       y + m_perlinCaveTurbY.Get(y) * 3,
                                       z + m_perlinCaveTurbZ.Get(z) * 3);
    float valCaveBase1 = 0.0f;
    if (valCave1 > -0.02f && valCave1 < 0.02f)
    {
        valCaveBase1 = (0.02f - std::abs(valCave1)) / 0.02f;
    }
    else
    {
        return false;
    }

    float valCave2 = m_perlinCave2.Get(x + m_perlinCaveTurbX.Get(x) * 3,
                                       y + m_perlinCaveTurbY.Get(y) * 3,
                                       z + m_perlinCaveTurbZ.Get(z) * 3);
    float valCaveBase2 = 0.0f;
    if (valCave2 > -0.02f && valCave2 < 0.02f)
    {
        valCaveBase2 = (0.02f - std::abs(valCave2)) / 0.02f;
    }
    else
    {
        return false;
    }

    float valCaveBase = grad * valCaveBase1 * valCaveBase2;
    if (valCaveBase > 0.1f)
    {
        return true;
    }
}


float CWorldGenerator3::getDensity(int x, int y, int z) const
{
    T_ASSERT(z >= 0);
    T_ASSERT(z <= ChunkHeight);

    float grad = static_cast<float>(HeightMax - z + HeightMin - z) / static_cast<float>(HeightMax - HeightMin);
    float density = m_perlinDensity.Get(x, y, z);
    return (grad + density);
}


int CWorldGenerator3::getElevation(int x, int y) const
{
    int elevation = HeightMin;

    for (int z = HeightMin; z <= HeightMax; ++z)
    {
        float density = getDensity(x, y, z);
        if (density > 0.0f)
        {
            elevation = z;
        }
    }

    return elevation;
}


CCell CWorldGenerator3::getCellContent(int x, int y, int z) const
{
    if (z <= 0)
    {
        return CCell(ContentBedRock);
    }

    if (z < HeightMin)
    {
        return CCell(ContentRock);
    }

    if (z > HeightMax)
    {
        return CCell(ContentEmpty);
    }

    float density = getDensity(x, y, z);

    if (density > 0.0f)
    {
        int elevation = getElevation(x, y);

        // Herbe au sommet
        if (z == elevation)
        {
            if (elevation >= WaterLevel)
            {
                return CCell(ContentGrass);
            }
            else
            {
                return CCell(ContentDirt);
            }
        }
        // Puis une couche de terre
        else if (z == elevation - 1)
        {
            return CCell(ContentDirt);
        }
        // Puis de la pierre
        else
        {
            return CCell(ContentRock);
        }
    }
    else
    {
        if (z <= WaterLevel)
        {
            return CCell(ContentWaterSource);
        }
        else
        {
            return CCell(ContentEmpty);
        }
    }
}


/**
 * Donne la température d'une cellule du monde.
 *
 * \param x Coordonnée X de la cellule dans le monde.
 * \param y Coordonnée Y de la cellule dans le monde.
 * \return Température de la cellule (entre 0 et 1).
 */

float CWorldGenerator3::getTemperature(int x, int y) const
{
    float val = m_perlinTemperature.Get(x, y);
    return (val + 1.0f) * 0.5f;
}


/**
 * Donne l'humidité d'une cellule du monde.
 *
 * \param x Coordonnée X de la cellule dans le monde.
 * \param y Coordonnée Y de la cellule dans le monde.
 * \return Humidité de la cellule (entre 0 et 1).
 */

float CWorldGenerator3::getHumidity(int x, int y) const
{
    float val = m_perlinHumidity.Get(x, y);
    return (val + 1.0f) * 0.5f;
}


/**
 * Donne le biome d'une cellule du monde.
 *
 * \param x Coordonnée X de la cellule dans le monde.
 * \param y Coordonnée Y de la cellule dans le monde.
 * \return Biome auquel appartient la cellule, déterminé à partir de
 *         sa température et de son humidité.
 */

TBiome CWorldGenerator3::getBiome(int x, int y) const
{/*
    float valE = m_perlinElevation.Get(x, y);
    if (valE <= OceanPerlinValue)
        return BiomeOcean;
    if (valE <= BeachPerlinValue)
        return BiomeBeach;
*/
    float temperature = getTemperature(x, y);
    float humidy = getHumidity(x, y);
    float precipitation = humidy * temperature;

    if (temperature < 0.1f)
        return BiomeIceDesert;

    if (precipitation < 0.2f)
    {
        if (temperature < 0.5f)
            return BiomeTundra;
        if (temperature < 0.95f)
            return BiomeSavanna;
        return BiomeDesert;
    }

    if ((precipitation > 0.5f) && (temperature < 0.7f))
        return BiomeSwampland;

    if (temperature < 0.5f)
        return BiomeTaiga;

    if (temperature < 0.97f)
    {
        if (precipitation < 0.35f)
            return BiomeShrubland;
        return BiomeForest;
    }

    if (precipitation < 0.45f)
        return BiomePlains;

    if (precipitation < 0.9f)
        return BiomeSeasonalForest;

    return BiomeRainForest;
}
