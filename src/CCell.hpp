/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef T_FILE_CCELL_HPP
#define T_FILE_CCELL_HPP

#include <stdint.h>
#include "Core/Exceptions.hpp"


const float FluidTopHeight = 0.875f; ///< Pourcentage de hauteur d'eau dans un bloc situé sous un bloc vide.


const int CELL_FURNACE_NORTH = 0x0;
const int CELL_FURNACE_EAST  = 0x1;
const int CELL_FURNACE_SOUTH = 0x2;
const int CELL_FURNACE_WEST  = 0x3;


/// Liste des blocs.
enum TCellContent
{
    ContentEmpty       = 0x00, ///< Air.
    ContentWater       = 0x01, ///< Eau.
    ContentWaterSource = 0x02, ///< Source d'eau.
    ContentLava        = 0x03, ///< Lave.
    ContentLavaSource  = 0x04, ///< Source de lave.
    ContentRock        = 0x05, ///< Roche.
    ContentDirt        = 0x06, ///< Terre.
    ContentGrass       = 0x07, ///< Herbe.
    ContentIronOre     = 0x08, ///< Minerai de fer.
    ContentSand        = 0x09, ///< Sable.
    ContentBrick       = 0x0A, ///< Briques.
    ContentBedRock     = 0x0B, ///< Bedrock (fond de la map).
    ContentCobbleStone = 0x0C, ///< Pierre.
    ContentWood        = 0x0D, ///< Bois
    ContentWoodenPlank = 0x0E, ///< Planches de bois.
    ContentFurnace     = 0x0F, ///< Fourneau.

    ContentCount
};


#include "Core/struct_alignment_start.h"

struct CCell
{
    inline CCell(TCellContent c = ContentEmpty) :
        content  (c),
        lighting (0),
        data     (0)
    { }

    inline TCellContent getCellContent() const
    {
        return static_cast<TCellContent>(content);
    }

    inline bool isEmpty() const
    {
        return (content == ContentEmpty);
    }

    inline bool isWater() const
    {
        return (content == ContentWater || content == ContentWaterSource);
    }

    inline bool isLava() const
    {
        return (content == ContentLava || content == ContentLavaSource);
    }

    inline int getWaterLevel() const
    {
        T_ASSERT(content == ContentWater);
        return ((data & 0xE) >> 1);
    }

    inline int getLavaLevel() const
    {
        T_ASSERT(content == ContentLava);
        return ((data & 0xC) >> 2);
    }


    uint16_t content:8;  ///< Contenu de la cellule.
    uint16_t lighting:4; ///< Niveau de lumière.
    uint16_t data:4;     ///< Données supplémentaires.
};

#include "Core/struct_alignment_end.h"

#endif // T_FILE_CCELL_HPP
