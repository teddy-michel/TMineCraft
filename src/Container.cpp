/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Container.hpp"
#include "CWorld.hpp"
#include "CFont.hpp"
#include "Core/Exceptions.hpp"
#include <GL/glew.h>


using namespace Ted;


/**
 * Affiche un emplacement d'un conteneur.
 *
 * \todo Utiliser le champ data.
 *
 * \param world
 * \param font  Police de caractères.
 * \param x     Coordonnée X.
 * \param y     Coordonnée Y.
 * \param type  Type d'objet.
 * \param count Nombre d'objets dans l'emplacement.
 * \param data  Données supplémentaires.
 */

void displayContainerSlot(CWorld * world, const CFont * font, int x, int y, int type, int count, int data)
{
    T_ASSERT(world != nullptr);

    // Bordure
    glBegin(GL_TRIANGLES);

        glColor3ub(139, 139, 139);

        glVertex2i(x - 2, y - 2);
        glVertex2i(x + InventoryItemSize + 2, y - 2);
        glVertex2i(x - 2, y + InventoryItemSize + 2);

        glVertex2i(x - 2, y + InventoryItemSize + 2);
        glVertex2i(x + InventoryItemSize + 2, y - 2);
        glVertex2i(x + InventoryItemSize + 2, y + InventoryItemSize + 2);

        glColor3ub(55, 55, 55);

        glVertex2i(x - 2, y - 2);
        glVertex2i(x + InventoryItemSize, y - 2);
        glVertex2i(x - 2, y + InventoryItemSize);

        glVertex2i(x - 2, y + InventoryItemSize);
        glVertex2i(x + InventoryItemSize, y - 2);
        glVertex2i(x + InventoryItemSize, y + InventoryItemSize);

        glColor3ub(255, 255, 255);

        glVertex2i(x, y);
        glVertex2i(x + InventoryItemSize + 2, y);
        glVertex2i(x, y + InventoryItemSize + 2);

        glVertex2i(x, y + InventoryItemSize + 2);
        glVertex2i(x + InventoryItemSize + 2, y);
        glVertex2i(x + InventoryItemSize + 2, y + InventoryItemSize + 2);

        glColor3ub(139, 139, 139);

        glVertex2i(x, y);
        glVertex2i(x + InventoryItemSize, y);
        glVertex2i(x, y + InventoryItemSize);

        glVertex2i(x, y + InventoryItemSize);
        glVertex2i(x + InventoryItemSize, y);
        glVertex2i(x + InventoryItemSize, y + InventoryItemSize);

    glEnd();

    if (count > 0)
    {
        glEnable(GL_TEXTURE_2D);
        glActiveTextureARB(GL_TEXTURE0_ARB);
        glClientActiveTextureARB(GL_TEXTURE0_ARB);

        // Bloc
        if (type < ContentCount)
        {
            drawBloc(world, x, y, type, data);
        }
        // Entités
        else
        {
            drawEntity(world, x, y, type, data);
        }

        glDisable(GL_TEXTURE_2D);

        // Nombre d'objets dans le slot
        if (count > 1)
        {
            font->displayText(CString::fromNumber(count),
                              x + InventoryItemSize - (count < 10 ? 10 : 18),
                              y + InventoryItemSize - 10,
                              CColor::White);
        }
    }
}


void drawBloc(CWorld * world, int x, int y, int type, int data)
{
    T_ASSERT(world != nullptr);
    T_ASSERT(type < ContentCount);

    switch (type)
    {
        case ContentRock:
            Game::textureManager->bindTexture(world->getTextureId(TextureRock));
            break;

        case ContentSand:
            Game::textureManager->bindTexture(world->getTextureId(TextureSand));
            break;

        case ContentBedRock:
            // Théoriquement ce type de bloc ne peut pas être obtenu
            Game::textureManager->bindTexture(world->getTextureId(TextureBedRock));
            break;

        case ContentCobbleStone:
            Game::textureManager->bindTexture(world->getTextureId(TextureCobbleStone));
            break;

        case ContentWoodenPlank:
            Game::textureManager->bindTexture(world->getTextureId(TextureWoodenPlank));
            break;

        case ContentWood:
            Game::textureManager->bindTexture(world->getTextureId(TextureWood));
            break;

        case ContentBrick:
            Game::textureManager->bindTexture(world->getTextureId(TextureBrick));
            break;

        case ContentDirt:
            Game::textureManager->bindTexture(world->getTextureId(TextureDirt));
            break;

        case ContentGrass:
            Game::textureManager->bindTexture(world->getTextureId(TextureGrass));
            break;

        case ContentIronOre:
            Game::textureManager->bindTexture(world->getTextureId(TextureGems));
            break;

        case ContentFurnace:
            Game::textureManager->bindTexture(world->getTextureId(TextureFurnaceTop));
            break;
    }

    // Face supérieure
    glBegin(GL_TRIANGLES);

        glColor3ub(200, 200, 200);
        glTexCoord2i(0, 0); glVertex2f(x + 0.09f * InventoryItemSize, y + 0.25f * InventoryItemSize);
        glTexCoord2i(1, 0); glVertex2f(x + 0.50f * InventoryItemSize, y);
        glTexCoord2i(0, 1); glVertex2f(x + 0.50f * InventoryItemSize, y + 0.50f * InventoryItemSize);

        glTexCoord2i(0, 1); glVertex2f(x + 0.50f * InventoryItemSize, y + 0.50f * InventoryItemSize);
        glTexCoord2i(1, 0); glVertex2f(x + 0.50f * InventoryItemSize, y);
        glTexCoord2i(1, 1); glVertex2f(x + 0.91f * InventoryItemSize, y + 0.25f * InventoryItemSize);

    glEnd();

    // Faces latérales
    if (type == ContentGrass)
    {
        Game::textureManager->bindTexture(world->getTextureId(TextureGrassSide));
    }
    else if (type == ContentWood)
    {
        Game::textureManager->bindTexture(world->getTextureId(TextureWoodSide));
    }
    else if (type == ContentFurnace)
    {
        Game::textureManager->bindTexture(world->getTextureId(TextureFurnaceFace));
    }

    glBegin(GL_TRIANGLES);

        // Face de gauche
        glColor3ub(180, 180, 180);

        glTexCoord2i(0, 0); glVertex2f(x + 0.09f * InventoryItemSize, y + 0.25f * InventoryItemSize);
        glTexCoord2i(1, 0); glVertex2f(x + 0.50f * InventoryItemSize, y + 0.50f * InventoryItemSize);
        glTexCoord2i(0, 1); glVertex2f(x + 0.09f * InventoryItemSize, y + 0.75f * InventoryItemSize);

        glTexCoord2i(0, 1); glVertex2f(x + 0.09f * InventoryItemSize, y + 0.75f * InventoryItemSize);
        glTexCoord2i(1, 0); glVertex2f(x + 0.50f * InventoryItemSize, y + 0.50f * InventoryItemSize);
        glTexCoord2i(1, 1); glVertex2f(x + 0.50f * InventoryItemSize, y + InventoryItemSize);

        if (type == ContentFurnace)
        {
            glEnd();
            Game::textureManager->bindTexture(world->getTextureId(TextureFurnaceSide));
            glBegin(GL_TRIANGLES);
        }

        // Face de droite
        glColor3ub(160, 160, 160);

        glTexCoord2i(0, 0); glVertex2f(x + 0.50f * InventoryItemSize, y + 0.50f * InventoryItemSize);
        glTexCoord2i(1, 0); glVertex2f(x + 0.91f * InventoryItemSize, y + 0.25f * InventoryItemSize);
        glTexCoord2i(0, 1); glVertex2f(x + 0.50f * InventoryItemSize, y + InventoryItemSize);

        glTexCoord2i(0, 1); glVertex2f(x + 0.50f * InventoryItemSize, y + InventoryItemSize);
        glTexCoord2i(1, 0); glVertex2f(x + 0.91f * InventoryItemSize, y + 0.25f * InventoryItemSize);
        glTexCoord2i(1, 1); glVertex2f(x + 0.91f * InventoryItemSize, y + 0.75f * InventoryItemSize);

    glEnd();
}


void drawEntity(CWorld * world, int x, int y, int type, int data)
{
    T_ASSERT(world != nullptr);
    T_ASSERT(type >= ContentCount);

    glColor3ub(255, 255, 255);

/*
    switch (type)
    {
        case EntityABC:
            Game::textureManager->bindTexture(world->getTextureId(TextureABC));
            break;
    }

    glBegin(GL_TRIANGLES);

        glTexCoord2i(0, 0); glVertex2i(x, y);
        glTexCoord2i(1, 0); glVertex2i(x + InventoryItemSize, y);
        glTexCoord2i(0, 1); glVertex2i(x, y + InventoryItemSize);

        glTexCoord2i(0, 1); glVertex2i(x, y + InventoryItemSize);
        glTexCoord2i(1, 0); glVertex2i(x + InventoryItemSize, y);
        glTexCoord2i(1, 1); glVertex2i(x + InventoryItemSize, y + InventoryItemSize);

    glEnd();
*/
}
