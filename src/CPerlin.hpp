/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef T_FILE_CPERLIN_HPP
#define T_FILE_CPERLIN_HPP

//#include <stdlib.h> // TODO : voir si on peut supprimer cette ligne

#define SAMPLE_SIZE 1024


class CPerlin
{
public:

    CPerlin(int octaves, float freq, float amp, int seed);

    float Get(float x) const;
    float Get(float x, float y) const;
    float Get(float x, float y, float z) const;

private:

    float perlin_noise_1D(float vec) const;
    float perlin_noise_2D(float vec[2]) const;
    float perlin_noise_3D(float vec[3]) const;

    float noise1(float arg) const;
    float noise2(float vec[2]) const;
    float noise3(float vec[3]) const;
    void normalize2(float v[2]) const;
    void normalize3(float v[3]) const;
    void init() const;
    int random() const;

    int m_octaves;
    float m_frequency;
    float m_amplitude;
    int m_seed;
    mutable bool m_start;

    mutable int p[SAMPLE_SIZE + SAMPLE_SIZE + 2];
    mutable float g3[SAMPLE_SIZE + SAMPLE_SIZE + 2][3];
    mutable float g2[SAMPLE_SIZE + SAMPLE_SIZE + 2][2];
    mutable float g1[SAMPLE_SIZE + SAMPLE_SIZE + 2];
};

#endif // T_FILE_CPERLIN_HPP
