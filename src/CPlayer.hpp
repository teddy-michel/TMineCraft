/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef T_FILE_CPLAYER_HPP
#define T_FILE_CPLAYER_HPP

#include <SFML/Window/Mouse.hpp>

#include "Core/Maths/CVector3.hpp"
#include "Container.hpp"
#include "CWorld.hpp"


const int DefaultTimeAction = 500;

// Inventaire
const int InventoryRows = 4;
const int InventoryCols = 9;
const int InventoryMaxItem = 64;

// Dimensions du joueur
const float PlayerHeight = 1.8f * CellSize;
const float PlayerEyesHeight = 1.6f * CellSize;
const float PlayerHalfSize = 0.3f * CellSize;

// Faim
const int TimeBetweenLifeUpdate = 4000;
const float LifeUpdateRegenerateFoodLevel = 90.0f;
const float LifeUpdateRegeneratePoints = 5.0f;
const float LifeUpdateRegenerateExhaustion = 75.0f;
const float LifeUpdateDepletePoints = -5.0f;

// HUD
const int HUD_LifeBarWidth = 150;
const int HUD_LifeBarHeight = 12;
const int HUD_FoodBarWidth = 150;
const int HUD_FoodBarHeight = 12;


class CGame;


/**
 * Classe représentant un joueur dans le jeu.
 */

class CPlayer
{
public:

    CPlayer(CGame * game, const Ted::TVector3F& position = Ted::Origin3F, const Ted::TVector3F& direction = Ted::TVector3F(1.0f, 0.0f, 0.0f));
    ~CPlayer();

    // Nom
    inline Ted::CString getName() const { return m_name; }
    inline void setName(const Ted::CString& name) { m_name = name; }

    inline void setPosition(const Ted::TVector3F& position)
    {
        m_position = position;
        m_direction = m_position + m_forward;
    }

    inline void setDirection(const Ted::TVector3F& direction)
    {
        if (m_position == direction)
        {
            m_direction = m_position + Ted::TVector3F(1.0f, 0.0f, 0.0f);
        }
        else
        {
            m_direction = direction;
        }

        anglesFromVectors();
    }

    inline Ted::TVector3F getPosition() const
    {
        return m_position;
    }

    inline Ted::TVector3F getDirection() const
    {
        return m_direction;
    }

    inline Ted::TVector3F getEyesPosition() const
    {
        return m_position + Ted::TVector3F(0.0f, 0.0f, PlayerEyesHeight);
        //TODO : déplacer les yeux sur la face avant de la tête ?
    }

    inline bool isFlying() const { return m_isFlying; }
    inline void setFlying(bool flying = true) { m_isFlying = flying; }

    void updateDirection(int xrel, int yrel);
    void doActions(unsigned int frameTime);
    void updateCamera();
    void display() const;
    void displayHUD() const;
    void displayFluid() const;

    // Points de vie
    float getLife() const;
    void setLife(float life);
    void addLife(float add);

    // Inventaire
    bool addInventoryItem(int content, int count = 1, int data = 0);
    void removeInventoryItem(int position, int count = 1);
    bool setInventoryItem(int position, int content, int& count, int data = 0);
    bool getInventoryItem(int position, int& type, int& count, int& data) const;
    int getInventoryItem(int position) const;
    inline int getCurrentItem() const { return m_currentItem; }
    void setCurrentItem(int currentItem);

    bool loadPlayer(const Ted::CString& folder);
    bool savePlayer(const Ted::CString& folder) const;

    void clickOnInventory(int x, int y, sf::Mouse::Button button);

private:

    static const int ItemSpacing = 4;
    static const int WindowMargin = 10;
    static const int SpaceSeparating = 8;

    void eatFood(float food, float saturation);
    void increaseExhaustion(float points);

    void displayInventory(int width, int height) const;

    void displayLifeBar(int left, int top) const;
    void displayFoodBar(int left, int top) const;

    void vectorsFromAngles();
    void anglesFromVectors();

    Ted::TVector3F getCorrectPosition(const Ted::TVector3F& position, const Ted::TVector3F& move) const;

    void actionInventorySlotsMouse1(CInventorySlot * source, CInventorySlot * dest);
    void actionInventorySlotsMouse2(CInventorySlot * source, CInventorySlot * dest);
    void updateInventoryCraft();


    // Format du fichier

    struct THeader
    {
        int32_t ident;             ///< Identifiant du format de fichier (TMCP).
        int32_t version;           ///< Version du format de fichier (1).
        char name[64];             ///< Nom du joueur.
        Ted::TVector3F position;   ///< Position du joueur dans le monde.
        Ted::TVector3F direction;  ///< Direction d'observation.
        float life;                ///< Points de vie (0 = mort, 100 = maximum).
        float foodLevel;           ///< Niveau de faim (entre 0 et 100).
        float foodSaturationLevel; ///< Niveau de saturation (entre 0 et 100).
        float foodExhaustionLevel; ///< Niveau d'épuisement (entre 0 et 100).
        uint8_t currentItem;       ///< Élément sélectionné dans l'inventaire (entre 0 et 8).

        // Inventaire
        CInventorySlot objects[InventoryRows * InventoryCols]; ///< Objets dans l'inventaire.
        CInventorySlot armor[4];                               ///< Armure.
        CInventorySlot crafting[4];                            ///< Éléments pour le crafting.
        CInventorySlot craftingResult;
        CInventorySlot selectedObject;
    };


    Ted::CString m_name;         ///< Nom du joueur.
    CGame * m_game;
    CWorld * m_world;
    Ted::TVector3F m_position;   ///< Position du joueur dans le monde.
    Ted::TVector3F m_direction;  ///< Direction d'observation.
    Ted::TVector3F m_motion;     ///< Vitesse de déplacement.
    bool m_isFlying;             ///< Indique si le joueur est en train de voler.
    bool m_isOnGround;           ///< Indique si le joueur est sur le sol.
    float m_life;                ///< Points de vie (0 = mort, 100 = maximum).
    float m_foodLevel;           ///< Niveau de faim (entre 0 et 100).
    float m_foodSaturationLevel; ///< Niveau de saturation (entre 0 et 100).
    float m_foodExhaustionLevel; ///< Niveau d'épuisement (entre 0 et 100).
    // TODO: Ajouter la respiration.
    // TODO: Ajouter l'expérience.
    // TODO: Ajouter l'armure. (calculé automatiquement)
    int m_timeAction;


    // Inventaire
    CInventorySlot m_slots[InventoryRows * InventoryCols]; ///< Objets dans l'inventaire.
    CInventorySlot m_armor[4];    ///< Armure.
    CInventorySlot m_crafting[4]; ///< Éléments pour le crafting.
    CInventorySlot m_craftingResult;
    CInventorySlot m_selectedObject;
    int m_currentItem;            ///< Élément sélectionné dans l'inventaire.


    // Caméra
    Ted::TVector3F m_forward; ///< Déplacement avant ou arrière.
    Ted::TVector3F m_left;    ///< Déplacement sur le côté.
    float m_theta;            ///< Angle Theta.
    float m_phi;              ///< Angle Phi.
};

#endif // T_FILE_CPLAYER_HPP
