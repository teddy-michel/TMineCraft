/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CChunk.hpp"
#include "Graphic/CBuffer.hpp"
#include "CWorld.hpp"
#include "CGame.hpp"
#include "CCell.hpp"
#include "Core/Maths/MathsUtils.hpp"
#include <algorithm>


using namespace Ted;


const float lightingMin = 0.2f;
const float lightingMax = 0.8f;


/**
 * Construit un chunk vide.
 */

CChunk::CChunk(int x, int y, CWorld * world) :
m_world            (world),
m_chunkX           (x),
m_chunkY           (y),
m_buffer           (nullptr),
m_bufferBlend      (nullptr),
m_isModified       (false),
m_isBuilding       (false),
m_isBuild          (false),
m_needBufferUpdate (false)
{
    m_buffer = new CBuffer();
    m_bufferBlend = new CBuffer();
    memset(m_heightMap, 0, sizeof(m_heightMap));
}


/**
 * Libère la mémoire occupée par le chunk.
 */

CChunk::~CChunk()
{
    delete m_bufferBlend;
    delete m_buffer;
}


/**
 * Retourne une cellule.
 *
 * \param x Coordonnée X de la cellule dans le chunk.
 * \param y Coordonnée Y de la cellule dans le chunk.
 * \param z Coordonnée Z de la cellule dans le chunk.
 * \return Pointeur sur la cellule, ou nullptr.
 */

CCell * CChunk::getCell(int x, int y, int z) const
{
    if (z < 0 || z >= ChunkHeight)
    {
        return nullptr;
    }

    return m_sectors[z / ChunkSize].getCell(x, y, z % ChunkSize);
}


TCellContent CChunk::getCellContent(int x, int y, int z) const
{
    T_ASSERT(z >= 0);
    T_ASSERT(z < ChunkHeight);

    return m_sectors[z / ChunkSize].getCellContent(x, y, z % ChunkSize);
}


bool CChunk::isCellEmpty(int x, int y, int z, bool waterIsEmpty, bool lavaIsEmpty) const
{
    T_ASSERT(z >= 0);
    T_ASSERT(z < ChunkHeight);

    return m_sectors[z / ChunkSize].isCellEmpty(x, y, z % ChunkSize, waterIsEmpty, lavaIsEmpty);
}


/**
 * Retourne la luminosité d'une cellule.
 *
 * \param x Coordonnée X de la cellule dans le chunk.
 * \param y Coordonnée Y de la cellule dans le chunk.
 * \param z Coordonnée Z de la cellule dans le chunk.
 * \return Luminosité (entre 0 et 15).
 */

int CChunk::getCellLighting(int x, int y, int z) const
{
    T_ASSERT(z >= 0);
    T_ASSERT(z < ChunkHeight);

    return m_sectors[z / ChunkSize].getCellLighting(x, y, z % ChunkSize);
}


bool CChunk::isCellNeedUpdate(int x, int y, int z) const
{
    T_ASSERT(z >= 0);
    T_ASSERT(z < ChunkHeight);

    TCellCoords coords(x, y, z);
    return (std::find(m_cellsUpdate.begin(), m_cellsUpdate.end(), coords) != m_cellsUpdate.end());
}


int CChunk::getCellWaterLevel(int x, int y, int z) const
{
    T_ASSERT(z >= 0);
    T_ASSERT(z < ChunkHeight);

    return m_sectors[z / ChunkSize].getCellWaterLevel(x, y, Modulo(z, ChunkSize));
}


int CChunk::getCellLavaLevel(int x, int y, int z) const
{
    T_ASSERT(z >= 0);
    T_ASSERT(z < ChunkHeight);

    return m_sectors[z / ChunkSize].getCellLavaLevel(x, y, Modulo(z, ChunkSize));
}


/**
 * Supprime le contenu d'une cellule du chunk.
 *
 * \param x Coordonnée X de la cellule dans le chunk.
 * \param y Coordonnée Y de la cellule dans le chunk.
 * \param z Coordonnée Z de la cellule dans le chunk.
 * \return Contenu de la cellule.
 */

TCellContent CChunk::removeCell(int x, int y, int z)
{
    // Vérification des bornes
    T_ASSERT(x >= 0);
    T_ASSERT(x < ChunkSize);
    T_ASSERT(y >= 0);
    T_ASSERT(y < ChunkSize);
    T_ASSERT(z >= 0);
    T_ASSERT(z < ChunkHeight);

    TCellContent content = getCellContent(x, y, z);

    if (isCellEmpty(x, y, z))
    {
        return content;
    }

    // L'herbe ne peut pas être récupérée
    if (content == ContentGrass)
    {
        content = ContentDirt;
    }

    bool isFluidAbove = (z < ChunkHeight - 1 && (getCell(x, y, z + 1)->isWater() || getCell(x, y, z + 1)->isLava()));

    setCellContent(x, y, z, ContentEmpty);

    if (isFluidAbove)
    {
        setCellNeedUpdate(x, y, z);
        setCellNeedUpdate(x, y, z + 1);
    }


    m_isModified = true;


    // Chunks adjacents
    CChunk * chunkW = nullptr;
    CChunk * chunkE = nullptr;
    CChunk * chunkS = nullptr;
    CChunk * chunkN = nullptr;

    // Bordure ouest
    if (x <= 0)
    {
        chunkW = m_world->getChunk(m_chunkX - 1, m_chunkY);
    }
    // Bordure est
    else if (x >= ChunkSize - 1)
    {
        chunkE = m_world->getChunk(m_chunkX + 1, m_chunkY);
    }

    // Bordure sud
    if (y <= 0)
    {
        chunkS = m_world->getChunk(m_chunkX, m_chunkY - 1);
    }
    // Bordure nord
    else if (y >= ChunkSize - 1)
    {
        chunkN = m_world->getChunk(m_chunkX, m_chunkY + 1);
    }


    // Mise à jour de la luminosité
    int maxLighting = 1;

    // La cellule était la plus haute de la colonne
    if (z == m_heightMap[x + y * ChunkSize])
    {
        // Mise à jour de la height map
        --m_heightMap[x + y * ChunkSize];
        updateCellLighting(x, y, z, 15); // Cellules exposées au soleil

        int zMax = z - 1;

        if (zMax > 0)
        {
            if (isCellEmpty(x, y, zMax))
            {
                setCellNeedUpdate(x, y, zMax);
            }

            while (zMax > 0)
            {
                if (getCellContent(x, y, zMax) == ContentEmpty)
                {
                    --zMax;
                    --m_heightMap[x + y * ChunkSize];
                    updateCellLighting(x, y, zMax, 15);
                }
                else
                {
                    break;
                }
            }
        }
    }
    else
    {
        // Luminosités des cellules adjacentes
        float lightingW = (x > 0 ? getCellLighting(x - 1, y, z) : 0.0f);
        float lightingE = (x < (ChunkSize - 1) ? getCellLighting(x + 1, y, z) : 0.0f);
        float lightingS = (y > 0 ? getCellLighting(x, y - 1, z) : 0.0f);
        float lightingN = (y < (ChunkSize - 1) ? getCellLighting(x, y + 1, z) : 0.0f);
        float lightingB = (z > 0 ? getCellLighting(x, y, z - 1) : 0.0f);
        float lightingT = (z < (ChunkHeight - 1) ? getCellLighting(x, y, z + 1) : 15.0f);

        // Bordure ouest
        if (x <= 0 && chunkW != nullptr)
        {
            if (chunkW->isCellEmpty(ChunkSize - 1, y, z))
            {
                lightingW = chunkW->getCellLighting(ChunkSize - 1, y, z);
            }
        }
        // Bordure est
        else if (x >= ChunkSize - 1 && chunkE != nullptr)
        {
            if (chunkE->isCellEmpty(0, y, z))
            {
                lightingE = chunkE->getCellLighting(0, y, z);
            }
        }

        // Bordure sud
        if (y <= 0 && chunkS != nullptr)
        {
            if (chunkS->isCellEmpty(x, ChunkSize - 1, z))
            {
                lightingS = chunkS->getCellLighting(x, ChunkSize - 1, z);
            }
        }
        // Bordure nord
        else if (y >= ChunkSize - 1 && chunkN != nullptr)
        {
            if (chunkN->isCellEmpty(x, 0, z))
            {
                lightingN = chunkN->getCellLighting(x, 0, z);
            }
        }

        if (lightingW > maxLighting)
            maxLighting = lightingW;
        if (lightingE > maxLighting)
            maxLighting = lightingE;
        if (lightingS > maxLighting)
            maxLighting = lightingS;
        if (lightingN > maxLighting)
            maxLighting = lightingN;
        if (lightingB > maxLighting)
            maxLighting = lightingB;
        if (lightingT > maxLighting)
            maxLighting = lightingT;

        setCellLighting(x, y, z, 0);
        updateCellLighting(x, y, z, maxLighting - 1);
    }


    // On cherche s'il y a de l'eau autour ou un bloc vide dont le niveau d'eau doit être mis à jour

    if (x > 0)
    {
        if (isFluidAbove)
        {
            if (isCellEmpty(x - 1, y, z))
            {
                setCellNeedUpdate(x - 1, y, z);
            }
        }
        else
        {
            CCell * cell = getCell(x - 1, y, z);
            if (cell && (cell->isWater() || cell->isLava()))
            {
                setCellNeedUpdate(x, y, z);
            }
        }
    }
    // Bordure ouest
    else if (x <= 0 && chunkW != nullptr)
    {
        if (isFluidAbove)
        {
            if (chunkW->isCellEmpty(ChunkSize - 1, y, z))
            {
                chunkW->setCellNeedUpdate(ChunkSize - 1, y, z);
            }
        }
        else
        {
            CCell * cell = chunkW->getCell(ChunkSize - 1, y, z);
            if (cell && (cell->isWater() || cell->isLava()))
            {
                setCellNeedUpdate(x, y, z);
            }
        }
    }

    if (x < ChunkSize - 1)
    {
        if (isFluidAbove)
        {
            if (isCellEmpty(x + 1, y, z))
            {
                setCellNeedUpdate(x + 1, y, z);
            }
        }
        else
        {
            CCell * cell = getCell(x + 1, y, z);
            if (cell && (cell->isWater() || cell->isLava()))
            {
                setCellNeedUpdate(x, y, z);
            }
        }
    }
    // Bordure est
    else if (x >= ChunkSize - 1 && chunkE != nullptr)
    {
        if (isFluidAbove)
        {
            if (chunkE->isCellEmpty(0, y, z))
            {
                chunkE->setCellNeedUpdate(0, y, z);
            }
        }
        else
        {
            CCell * cell = chunkE->getCell(0, y, z);
            if (cell && (cell->isWater() || cell->isLava()))
            {
                setCellNeedUpdate(x, y, z);
            }
        }
    }

    if (y > 0)
    {
        if (isFluidAbove)
        {
            if (isCellEmpty(x, y - 1, z))
            {
                setCellNeedUpdate(x, y - 1, z);
            }
        }
        else
        {
            CCell * cell = getCell(x, y - 1, z);
            if (cell && (cell->isWater() || cell->isLava()))
            {
                setCellNeedUpdate(x, y, z);
            }
        }
    }
    // Bordure sud
    else if (y <= 0 && chunkS != nullptr)
    {
        if (isFluidAbove)
        {
            if (chunkS->isCellEmpty(x, ChunkSize - 1, z))
            {
                chunkS->setCellNeedUpdate(x, ChunkSize - 1, z);
            }
        }
        else
        {
            CCell * cell = chunkS->getCell(x, ChunkSize - 1, z);
            if (cell && (cell->isWater() || cell->isLava()))
            {
                setCellNeedUpdate(x, y, z);
            }
        }
    }

    if (y < ChunkSize - 1)
    {
        if (isFluidAbove)
        {
            if (isCellEmpty(x, y + 1, z))
            {
                setCellNeedUpdate(x, y + 1, z);
            }
        }
        else
        {
            CCell * cell = getCell(x, y + 1, z);
            if (cell && (cell->isWater() || cell->isLava()))
            {
                setCellNeedUpdate(x, y, z);
            }
        }
    }
    // Bordure nord
    else if (y >= ChunkSize - 1 && chunkN != nullptr)
    {
        if (isFluidAbove)
        {
            if (chunkN->isCellEmpty(x, 0, z))
            {
                chunkN->setCellNeedUpdate(x, 0, z);
            }
        }
        else
        {
            CCell * cell = chunkN->getCell(x, 0, z);
            if (cell && (cell->isWater() || cell->isLava()))
            {
                setCellNeedUpdate(x, y, z);
            }
        }
    }


    // Mise à jour du buffer graphique
    m_needBufferUpdate = true;


    // Mise à jour des chunks adjacents
    if (x <= 0 && chunkW != nullptr)
    {
        chunkW->m_needBufferUpdate = true;
    }
    else if (x == ChunkSize - 1 && chunkE != nullptr)
    {
        chunkE->m_needBufferUpdate = true;
    }

    if (y == 0 && chunkS != nullptr)
    {
        chunkS->m_needBufferUpdate = true;
    }
    else if (y == ChunkSize - 1 && chunkN != nullptr)
    {
        chunkN->m_needBufferUpdate = true;
    }

    return content;
}


/**
 * Remplit une cellule vide.
 *
 * \param x       Coordonnée X de la cellule dans le chunk.
 * \param y       Coordonnée Y de la cellule dans le chunk.
 * \param z       Coordonnée Z de la cellule dans le chunk.
 * \param content Contenu à utiliser.
 * \return True si la cellule a été remplie, false sinon.
 */

bool CChunk::addCell(int x, int y, int z, TCellContent content, int data)
{
    if (content == ContentEmpty)
    {
        return false;
    }

    T_ASSERT(x >= 0);
    T_ASSERT(x < ChunkSize);
    T_ASSERT(y >= 0);
    T_ASSERT(y < ChunkSize);
    T_ASSERT(z >= 0);
    T_ASSERT(z < ChunkHeight);

    if (!isCellEmpty(x, y, z))
    {
        return false;
    }

    TCellContent oldContent = getCellContent(x, y, z);

    if (oldContent == content)
    {
        return false; // ? Réfléchir là-dessus
    }

    setCellContent(x, y, z, content, data);
    m_isModified = true;

    if (content != ContentWater && content != ContentWaterSource &&
        content != ContentLava  && content != ContentLavaSource)
    {
        setCellLighting(x, y, z, 0);

        if (oldContent == ContentWater || oldContent == ContentWaterSource)
        {
            setWaterLevel(x, y, z, 0);
        }
        else if (oldContent == ContentLava || oldContent == ContentLavaSource)
        {
            setLavaLevel(x, y, z, 0);
        }
    }

    // TODO: mettre à jour la luminosité des blocs adjacents

    if (y < ChunkSize - 1)
    {
        setCellNeedUpdate(x, y + 1, z, true);
    }
    else
    {
        CChunk * chunkN = m_world->getChunk(m_chunkX, m_chunkY + 1);

        if (chunkN != nullptr)
            chunkN->setCellNeedUpdate(x, 0, z, true);
    }

    if (x < ChunkSize - 1)
    {
        setCellNeedUpdate(x + 1, y, z, true);
    }
    else
    {
        CChunk * chunkE = m_world->getChunk(m_chunkX + 1, m_chunkY);

        if (chunkE != nullptr)
            chunkE->setCellNeedUpdate(0, y, z, true);
    }

    if (y > 0)
    {
        setCellNeedUpdate(x, y - 1, z, true);
    }
    else
    {
        CChunk * chunkS = m_world->getChunk(m_chunkX, m_chunkY - 1);

        if (chunkS != nullptr)
            chunkS->setCellNeedUpdate(x, ChunkSize - 1, z, true);
    }

    if (x > 0)
    {
        setCellNeedUpdate(x - 1, y, z, true);
    }
    else
    {
        CChunk * chunkW = m_world->getChunk(m_chunkX - 1, m_chunkY);

        if (chunkW != nullptr)
            chunkW->setCellNeedUpdate(ChunkSize - 1, y, z, true);
    }

    if (z < ChunkHeight && (getCell(x, y, z + 1)->content == ContentEmpty ||
                            getCell(x, y, z + 1)->content == ContentWater ||
                            getCell(x, y, z + 1)->content == ContentLava))
    {
        setCellNeedUpdate(x, y, z + 1, true);
    }

    // Cellule inférieure
    if (z > 0 && (getCellContent(x, y, z - 1) == ContentWater ||
                  getCellContent(x, y, z - 1) == ContentLava))
    {
        setCellNeedUpdate(x, y, z - 1, true);
    }


    // Mise à jour du buffer graphique
    m_needBufferUpdate = true;

    return true;
}


/**
 * Construit le chunk avec un algorithme de Perlin.
 *
 * \return Nombre de blocs solides du chunk.
 */

int CChunk::buildChunkPerlin()
{
    const int chunkMinX = getChunkMinX();
    const int chunkMinY = getChunkMinY();

    m_world->buildChunk(chunkMinX, chunkMinY, m_sectors, m_heightMap);

    unsigned int countCells = 0;
    for (int sector = 0; sector < SectorsInChunk; ++sector)
    {
        countCells += m_sectors[sector].getBlocsCount();
    }

#if 0

    // On parcours le chunk en 2D
    for (int x = 0; x < ChunkSize; ++x)
    {
        for (int y = 0; y < ChunkSize; ++y)
        {
            m_heightMap[x + y * ChunkSize] = 0;

            for (int z = 0; z < ChunkHeight; ++z)
            {
                CCell cellContent = m_world->buildCellContent(chunkMinX + x, chunkMinY + y, z);
                setCellContent(x, y, z, cellContent.getCellContent(), cellContent.data);

                if (!cellContent.isEmpty() && !cellContent.isWater())
                {
                    ++countCells;
                    m_heightMap[x + y * ChunkSize] = z;
                }
            }
/*
            TBiome biome = m_world->getBiome(chunkMinX + x, chunkMinY + y);
            int elevation = m_world->getElevation(chunkMinX + x, chunkMinY + y);

            float valDirtHeight = m_world->getPerlin((chunkMinX + x - 1024), (chunkMinY + y - 1024));

            m_heightMap[x + y * ChunkSize] = elevation;

            // Fond de bedrock
            setCellContent(x, y, 0, ContentBedRock);

            // On remplit le dessous du chunk
            for (int z = 1; z <= elevation; ++z)
            {
                if (biome == BiomeDesert || biome == BiomeOcean || biome == BiomeBeach)
                {
                    // Deux ou trois couches de sable
                    if (z == elevation || z == elevation - 1 || (valDirtHeight < 0.0f && z == elevation - 2))
                    {
                        setCellContent(x, y, z, ContentSand);
                    }
                    // Puis de la pierre
                    else
                    {
                        setCellContent(x, y, z, ContentCobbleStone);
                    }
                }
                else
                {
                    // Herbe au sommet
                    if (z == elevation)
                    {
                        if (elevation >= WaterLevel)
                        {
                            setCellContent(x, y, z, ContentGrass);
                        }
                        else
                        {
                            setCellContent(x, y, z, ContentDirt);
                        }
                    }
                    // Puis une ou deux couches de terre
                    else if (z == elevation - 1 || (valDirtHeight < 0.0f && z == elevation - 2))
                    {
                        setCellContent(x, y, z, ContentDirt);
                    }
                    // Puis de la pierre
                    else
                    {
                        setCellContent(x, y, z, ContentCobbleStone);
                    }
                }

                ++countCells;
            }

            if (elevation < WaterLevel)
            {
                for (int z = elevation + 1; z <= WaterLevel; ++z)
                {
                    setCellContent(x, y, z, ContentWaterSource);
                }
            }
*/
        }
    }

#endif // 0

    return countCells;
}


/**
 * Construit le chunk dans un monde plat, utile pour les tests.
 *
 * \return Nombre de blocs solides du chunk.
 */

int CChunk::buildChunkDebug()
{
    const int chunkMinX = getChunkMinX();
    const int chunkMinY = getChunkMinY();

    unsigned int countCells = 0;

    // On parcours le chunk en 2D
    for (int x = 0; x < ChunkSize; ++x)
    {
        for (int y = 0; y < ChunkSize; ++y)
        {
            int mz = (chunkMinX + x < 0 ? 63 : 64);
            m_heightMap[x + y * ChunkSize] = mz;

            for (int z = 0; z <= mz; ++z)
            {
                setCellContent(x, y, z, ContentCobbleStone);
            }

            if (mz < WaterLevel)
            {
                for (int z = mz + 1; z <= WaterLevel; ++z)
                {
                    setCellContent(x, y, z, ContentWaterSource);
                }
            }
        }
    }

    // Trou de 3x3x2
    if (chunkMinX == 0 && chunkMinY == 0)
    {
        for (int x = 3; x < 6; ++x)
        {
            for (int y = 3; y < 6; ++y)
            {
                setCellContent(x, y, 64, ContentEmpty);
                setCellContent(x, y, 63, ContentEmpty);

                m_heightMap[x + y * ChunkSize] = 62;
            }
        }
    }

    // Trou rempli de lave
    if (chunkMinX == 0 && chunkMinY == ChunkSize)
    {
        for (int x = 3; x < 6; ++x)
        {
            for (int y = 3; y < 6; ++y)
            {
                setCellContent(x, y, 64, ContentLavaSource);
                m_heightMap[x + y * ChunkSize] = 63;
            }
        }
    }

    return countCells;
}


/**
 * Détermine le contenu du chunk.
 */

void CChunk::build()
{
    if (m_isBuilding)
    {
        return;
    }

    m_isBuilding = true;

    std::cout << "Chunk " << m_chunkX << " x " << m_chunkY << " : building..." << std::endl;
    resetChunk();

    int countCells = buildChunkPerlin();
    //int countCells = buildChunkDebug();

#if 0

    // Terre au sommet, gemmes
    for (int x = 0; x < ChunkSize; ++x)
    {
        for (int y = 0; y < ChunkSize; ++y)
        {
            for (int z = 0; z < ChunkHeight - 1; ++z)
            {
                if (getCellContent(x, y, z) == ContentRock)
                {
                    // Les deux blocs supérieurs doivent être vides mais pas le blocs inférieur
                    if (getCellContent(x, y, z + 1) == ContentEmpty)
                    {
                        if (z != 0 && ((z == (ChunkHeight - 2)) || getCellContent(x, y, z + 2) == ContentEmpty) && getCellContent(x, y, z - 1) != ContentEmpty)
                        {
                            setCellContent(x, y, z, ContentDirt);
                        }
                    }
                    else
                    {
/*
                        float xf = (float) x / (float) WorldSize;
                        float yf = (float) y / (float) WorldSize;
                        float zf = (float) z / (float) WorldSize;

                        if (simplex_noise(3, xf * 10 + 3, yf * 10 + 3, zf * 10 + 3) > 3.65f)
                        {
                            setCellContent(x, y, z, ContentGems);
                        }
*/
                    }
                }
            }
        }
    }

#endif

    int emptySectors = 0;

    for (int sector = 0; sector < SectorsInChunk; ++sector)
    {
        if (m_sectors[sector].isEmpty())
        {
            ++emptySectors;
        }
    }


    std::cout << "Chunk " << m_chunkX << " x " << m_chunkY << " : built (" << countCells << " blocs, " << emptySectors << " secteurs vides)" << std::endl;

    m_needBufferUpdate = true;
    m_isBuild = true;
    m_isBuilding = false;

    // Demande de mise à jour graphique des chunks adjacents
    CChunk * chunkN = m_world->getChunk(m_chunkX, m_chunkY + 1);
    if (chunkN != nullptr)
    {
        chunkN->m_needBufferUpdate = true;
    }

    CChunk * chunkE = m_world->getChunk(m_chunkX + 1, m_chunkY);
    if (chunkE != nullptr)
    {
        chunkE->m_needBufferUpdate = true;
    }

    CChunk * chunkS = m_world->getChunk(m_chunkX, m_chunkY - 1);
    if (chunkS != nullptr)
    {
        chunkS->m_needBufferUpdate = true;
    }

    CChunk * chunkW = m_world->getChunk(m_chunkX - 1, m_chunkY);
    if (chunkW != nullptr)
    {
        chunkW->m_needBufferUpdate = true;
    }
}


/**
 * Réinitialise le contenu du chunk.
 */

void CChunk::resetChunk()
{
    if (m_isBuilding)
    {
        return;
    }

    m_isBuilding = true;

    for (int sector = 0; sector < SectorsInChunk; ++sector)
    {
        m_sectors[sector].reset();
    }

    m_needBufferUpdate = true;
    m_isModified = false;
    m_isBuild = false;
    m_isBuilding = false;
}


/**
 * Demande une mise à jour du buffer graphique du chunk.
 */

void CChunk::requestUpdateBuffer()
{
    m_needBufferUpdate = true;
}


/**
 * Met à jour le buffer graphique du chunk.
 */

void CChunk::updateBuffer()
{
    std::cout << "Chunk " << m_chunkX << " x " << m_chunkY << " : update buffer..." << std::endl;

    m_buffer->clear();
    m_bufferBlend->clear();

    if (m_isBuilding)
    {
        std::cout << "Chunk " << m_chunkX << " x " << m_chunkY << " : construction en cours" << std::endl;
        return;
    }


    std::vector<TVector3F>&    bufferVertices      = m_buffer->getVertices();
    std::vector<float>&        bufferColors        = m_buffer->getColors();
    std::vector<TVector2F>&    bufferCoords        = m_buffer->getTextCoords();
    TBufferTextureVector&      bufferTextures      = m_buffer->getTextures();
    std::vector<unsigned int>& bufferIndices       = m_buffer->getIndices();

    std::vector<TVector3F>&    bufferBlendVertices = m_bufferBlend->getVertices();
    std::vector<float>&        bufferBlendColors   = m_bufferBlend->getColors();
    std::vector<TVector2F>&    bufferBlendCoords   = m_bufferBlend->getTextCoords();
    TBufferTextureVector&      bufferBlendTextures = m_bufferBlend->getTextures();
    std::vector<unsigned int>& bufferBlendIndices  = m_bufferBlend->getIndices();


    const float lightingRange = lightingMax - lightingMin;

    const int chunkMinX = getChunkMinX();
    const int chunkMinY = getChunkMinY();

    unsigned int countFaces = 0;
    unsigned int countFacesBlend = 0;


    ::TDisplayMode mode = m_world->getGame()->getDisplayMode();


    for (int x = 0; x < ChunkSize; ++x)
    {
        for (int y = 0; y < ChunkSize; ++y)
        {
            for (int z = 0; z < ChunkHeight - 1; ++z)
            {
                CCell * cell = getCell(x, y, z);

                if (cell == nullptr || cell->content == ContentEmpty)
                {
                    continue;
                }


                // Cellules adjacentes
                CCell * cellT = getCell(x, y, z + 1);
                CCell * cellN = m_world->getCell(chunkMinX + x, chunkMinY + y + 1, z);
                CCell * cellNE = m_world->getCell(chunkMinX + x + 1, chunkMinY + y + 1, z);
                CCell * cellE = m_world->getCell(chunkMinX + x + 1, chunkMinY + y, z);
                CCell * cellSE = m_world->getCell(chunkMinX + x + 1, chunkMinY + y - 1, z);
                CCell * cellS = m_world->getCell(chunkMinX + x, chunkMinY + y - 1, z);
                CCell * cellSW = m_world->getCell(chunkMinX + x - 1, chunkMinY + y - 1, z);
                CCell * cellW = m_world->getCell(chunkMinX + x - 1, chunkMinY + y, z);
                CCell * cellNW = m_world->getCell(chunkMinX + x - 1, chunkMinY + y + 1, z);


                // Gestion de l'eau
                bool isWater = (cell->isWater());
                bool isWaterAbove = (z < (ChunkHeight - 1) && cellT != nullptr && cellT->isWater());
                bool isLava = (cell->isLava());
                bool isLavaAbove = (z < (ChunkHeight - 1) && cellT != nullptr && cellT->isLava());

                float topZ = z;

                if (isWater && !isWaterAbove)
                {
                    if (getCellContent(x, y, z) == ContentWaterSource)
                    {
                        topZ += FluidTopHeight;
                    }
                    else
                    {
                        topZ += FluidTopHeight * (getCellWaterLevel(x, y, z) + 1) / 8.0f;
                    }
                }
                else if (isLava && !isLavaAbove)
                {
                    if (getCellContent(x, y, z) == ContentLavaSource)
                    {
                        topZ += FluidTopHeight;
                    }
                    else
                    {
                        topZ += FluidTopHeight * (getCellLavaLevel(x, y, z) + 1) / 4.0f;
                    }
                }
                else
                {
                    topZ += 1.0f;
                }


                // Cellules adjacentes vides
                bool emptyCellW = (x > 0 && isCellEmpty(x - 1, y, z, !isWater, !isLava));
                bool emptyCellE = (x < (ChunkSize - 1) && isCellEmpty(x + 1, y, z, !isWater, !isLava));
                bool emptyCellS = (y > 0 && isCellEmpty(x, y - 1, z, !isWater, !isLava));
                bool emptyCellN = (y < (ChunkSize - 1) && isCellEmpty(x, y + 1, z, !isWater, !isLava));

                // Luminosités des cellules adjacentes
                float lightingW = (x > 0 ? getCellLighting(x - 1, y, z) : 0.0f);
                float lightingE = (x < (ChunkSize - 1) ? getCellLighting(x + 1, y, z) : 0.0f);
                float lightingS = (y > 0 ? getCellLighting(x, y - 1, z) : 0.0f);
                float lightingN = (y < (ChunkSize - 1) ? getCellLighting(x, y + 1, z) : 0.0f);

                // Bordure ouest
                if (x <= 0)
                {
                    CChunk * chunkW = m_world->getChunk(m_chunkX - 1, m_chunkY);

                    // Le chunk est chargé
                    if (chunkW != nullptr)
                    {
                        emptyCellW = chunkW->isCellEmpty(ChunkSize - 1, y, z, !isWater, !isLava);

                        if (emptyCellW)
                        {
                            lightingW = chunkW->getCellLighting(ChunkSize - 1, y, z);
                        }
                    }
                }
                // Bordure est
                else if (x >= ChunkSize - 1)
                {
                    CChunk * chunkE = m_world->getChunk(m_chunkX + 1, m_chunkY);

                    // Le chunk est chargé
                    if (chunkE != nullptr)
                    {
                        emptyCellE = chunkE->isCellEmpty(0, y, z, !isWater, !isLava);

                        if (emptyCellE)
                        {
                            lightingE = chunkE->getCellLighting(0, y, z);
                        }
                    }
                }

                // Bordure sud
                if (y <= 0)
                {
                    CChunk * chunkS = m_world->getChunk(m_chunkX, m_chunkY - 1);

                    // Le chunk est chargé
                    if (chunkS != nullptr)
                    {
                        emptyCellS = chunkS->isCellEmpty(x, ChunkSize - 1, z, !isWater, !isLava);

                        if (emptyCellS)
                            lightingS = chunkS->getCellLighting(x, ChunkSize - 1, z);
                    }
                }
                // Bordure nord
                else if (y >= ChunkSize - 1)
                {
                    CChunk * chunkN = m_world->getChunk(m_chunkX, m_chunkY + 1);

                    // Le chunk est chargé
                    if (chunkN != nullptr)
                    {
                        emptyCellN = chunkN->isCellEmpty(x, 0, z, !isWater, !isLava);

                        if (emptyCellN)
                            lightingN = chunkN->getCellLighting(x, 0, z);
                    }
                }


                // Calcul de la luminosité
                lightingW *= lightingRange / 15.0f;
                lightingE *= lightingRange / 15.0f;
                lightingS *= lightingRange / 15.0f;
                lightingN *= lightingRange / 15.0f;

                lightingW += lightingMin;
                lightingE += lightingMin;
                lightingS += lightingMin;
                lightingN += lightingMin;


                // Couleur de la cellule
                TVector3F colorCoef(1.0f, 1.0f, 1.0f);

                if (mode == DisplayBiome)
                {
                    switch (m_world->getBiome(chunkMinX + x, chunkMinY + y))
                    {
                        default:
                            colorCoef.set(1.0f, 1.0f, 1.0f);
                            break;

                        case BiomePlains:
                            colorCoef.set(0.72f, 0.79f, 0.47f);
                            break;
                        case BiomeDesert:
                            colorCoef.set(0.73f, 0.07f, 0.11f);
                            break;
                        case BiomeForest:
                            colorCoef.set(0.40f, 0.49f, 0.32f);
                            break;
                        case BiomeSavanna:
                            colorCoef.set(0.74f, 0.55f, 0.27f);
                            break;
                        case BiomeTundra:
                            colorCoef.set(0.02f, 0.39f, 0.50f);
                            break;
                        case BiomeRainForest:
                            colorCoef.set(0.00f, 0.18f, 0.18f);
                            break;
                        case BiomeIceDesert:
                            colorCoef.set(0.91f, 0.03f, 0.22f);
                            break;
                        case BiomeTaiga:
                            colorCoef.set(1.00f, 0.36f, 0.17f);
                            break;
                        case BiomeSeasonalForest:
                            colorCoef.set(0.69f, 0.80f, 0.60f);
                            break;
                        case BiomeSwampland:
                            colorCoef.set(0.54f, 0.35f, 0.35f);
                            break;
                        case BiomeShrubland:
                            colorCoef.set(0.55f, 0.78f, 0.84f);
                            break;
                        case BiomeOcean:
                            colorCoef.set(0.28f, 0.13f, 0.98f);
                            break;
                        case BiomeBeach:
                            colorCoef.set(0.85f, 0.54f, 0.12f);
                            break;
                    }
                }
                else if (mode == DisplayTemperature)
                {
                    float temperature = m_world->getTemperature(chunkMinX + x, chunkMinY + y);
                    colorCoef.set(temperature, 0.0f, 0.0f);
                }
                else if (mode == DisplayHumidity)
                {
                    float humidity = m_world->getHumidity(chunkMinX + x, chunkMinY + y);
                    colorCoef.set(0.0f, 0.0f, humidity);
                }


                // Hauteur de fluide pour chaque coin
                float cornerNE = topZ;
                float cornerSE = topZ;
                float cornerSW = topZ;
                float cornerNW = topZ;

                if ((isWater && !isWaterAbove) || (isLava && !isLavaAbove))
                {
                    // Coin Nord-Est
                    if (isWater && (cellN == nullptr || !cellN->isWater()) &&
                                   (cellE == nullptr || !cellE->isWater()))
                    {
                        // NOTHING
                    }
                    else if (isLava && (cellN == nullptr || !cellN->isLava()) &&
                                       (cellE == nullptr || !cellE->isLava()))
                    {
                        // NOTHING
                    }
                    else
                    {
                        int maxFluidLevel = std::max(getFluidLevel(cellN), std::max(getFluidLevel(cellNE), getFluidLevel(cellE)));
                        cornerNE = z + FluidTopHeight * (maxFluidLevel + 1) / (isLava ? 4.0f : 8.0f);

                        if (cornerNE < topZ)
                        {
                            cornerNE = topZ;
                        }
                    }

                    // Coin Sud-Est
                    if (isWater && (cellS == nullptr || !cellS->isWater()) &&
                                   (cellE == nullptr || !cellE->isWater()))
                    {
                        // NOTHING
                    }
                    else if (isLava && (cellS == nullptr || !cellS->isLava()) &&
                                       (cellE == nullptr || !cellE->isLava()))
                    {
                        // NOTHING
                    }
                    else
                    {
                        int maxFluidLevel = std::max(getFluidLevel(cellS), std::max(getFluidLevel(cellSE), getFluidLevel(cellE)));
                        cornerSE = z + FluidTopHeight * (maxFluidLevel + 1) / (isLava ? 4.0f : 8.0f);

                        if (cornerSE < topZ)
                        {
                            cornerSE = topZ;
                        }
                    }

                    // Coin Sud-Ouest
                    if (isWater && (cellS == nullptr || !cellS->isWater()) &&
                                   (cellW == nullptr || !cellW->isWater()))
                    {
                        // NOTHING
                    }
                    else if (isLava && (cellS == nullptr || !cellS->isLava()) &&
                                       (cellW == nullptr || !cellW->isLava()))
                    {
                        // NOTHING
                    }
                    else
                    {
                        int maxFluidLevel = std::max(getFluidLevel(cellS), std::max(getFluidLevel(cellSW), getFluidLevel(cellW)));
                        cornerSW = z + FluidTopHeight * (maxFluidLevel + 1) / (isLava ? 4.0f : 8.0f);

                        if (cornerSW < topZ)
                        {
                            cornerSW = topZ;
                        }
                    }

                    // Coin Nord-Ouest
                    if (isWater && (cellN == nullptr || !cellN->isWater()) &&
                                   (cellW == nullptr || !cellW->isWater()))
                    {
                        // NOTHING
                    }
                    else if (isLava && (cellN == nullptr || !cellN->isLava()) &&
                                       (cellW == nullptr || !cellW->isLava()))
                    {
                        // NOTHING
                    }
                    else
                    {
                        int maxFluidLevel = std::max(getFluidLevel(cellN), std::max(getFluidLevel(cellNW), getFluidLevel(cellW)));
                        cornerNW = z + FluidTopHeight * (maxFluidLevel + 1) / (isLava ? 4.0f : 8.0f);

                        if (cornerNW < topZ)
                        {
                            cornerNW = topZ;
                        }
                    }
                }


                if (emptyCellW)
                {
                    if (isWater || isLava)
                    {
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x) * CellSize, (chunkMinY + y    ) * CellSize, z        * CellSize));
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x) * CellSize, (chunkMinY + y + 1) * CellSize, z        * CellSize));
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x) * CellSize, (chunkMinY + y    ) * CellSize, cornerSW * CellSize));
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x) * CellSize, (chunkMinY + y + 1) * CellSize, cornerNW * CellSize));

                        for (int c = 0; c < 4; ++c)
                        {
                            bufferBlendColors.push_back(lightingW * colorCoef.X);
                            bufferBlendColors.push_back(lightingW * colorCoef.Y);
                            bufferBlendColors.push_back(lightingW * colorCoef.Z);
                            bufferBlendColors.push_back(1.0f);
                        }

                        bufferBlendCoords.push_back(TVector2F(0.0f, 1.0f));
                        bufferBlendCoords.push_back(TVector2F(1.0f, 1.0f));
                        bufferBlendCoords.push_back(TVector2F(0.0f, 1.0f - cornerSW + z));
                        bufferBlendCoords.push_back(TVector2F(1.0f, 1.0f - cornerNW + z));

                        TBufferTexture texture;
                        texture.nbr = 12;
                        texture.texture[0] = m_world->getTextureId(isWater ? TextureWater : TextureLava);
                        bufferBlendTextures.push_back(texture);

                        bufferBlendIndices.push_back(countFacesBlend * 4 + 0);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 3);

                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 0);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 3);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);

                        ++countFacesBlend;
                    }
                    else
                    {
                        bufferVertices.push_back(TVector3F((chunkMinX + x) * CellSize, (chunkMinY + y    ) * CellSize, z     * CellSize));
                        bufferVertices.push_back(TVector3F((chunkMinX + x) * CellSize, (chunkMinY + y + 1) * CellSize, z     * CellSize));
                        bufferVertices.push_back(TVector3F((chunkMinX + x) * CellSize, (chunkMinY + y    ) * CellSize, (z+1) * CellSize));
                        bufferVertices.push_back(TVector3F((chunkMinX + x) * CellSize, (chunkMinY + y + 1) * CellSize, (z+1) * CellSize));

                        for (int c = 0; c < 4; ++c)
                        {
                            bufferColors.push_back(lightingW * colorCoef.X);
                            bufferColors.push_back(lightingW * colorCoef.Y);
                            bufferColors.push_back(lightingW * colorCoef.Z);
                            bufferColors.push_back(1.0f);
                        }

                        bufferCoords.push_back(TVector2F(0.0f, 1.0f));
                        bufferCoords.push_back(TVector2F(1.0f, 1.0f));
                        bufferCoords.push_back(TVector2F(0.0f, 0.0f));
                        bufferCoords.push_back(TVector2F(1.0f, 0.0f));

                        TBufferTexture texture;
                        texture.nbr = 6;

                        switch (cell->content)
                        {
                            case ContentEmpty:
                            case ContentWater:
                            case ContentWaterSource:
                            case ContentLava:
                            case ContentLavaSource:
                                // On n'arrive jamais ici, mais ça évite un avertissement du compilateur.
                                break;

                            case ContentBedRock:
                                texture.texture[0] = m_world->getTextureId(TextureBedRock);
                                break;

                            case ContentWoodenPlank:
                                texture.texture[0] = m_world->getTextureId(TextureWoodenPlank);
                                break;

                            case ContentCobbleStone:
                                texture.texture[0] = m_world->getTextureId(TextureCobbleStone);
                                break;

                            case ContentRock:
                                texture.texture[0] = m_world->getTextureId(TextureRock);
                                break;

                            case ContentSand:
                                texture.texture[0] = m_world->getTextureId(TextureSand);
                                break;

                            case ContentBrick:
                                texture.texture[0] = m_world->getTextureId(TextureBrick);
                                break;

                            case ContentDirt:
                                texture.texture[0] = m_world->getTextureId(TextureDirt);
                                break;

                            case ContentIronOre:
                                texture.texture[0] = m_world->getTextureId(TextureGems);
                                break;

                            case ContentWood:
                                texture.texture[0] = m_world->getTextureId(TextureWoodSide);
                                break;

                            case ContentGrass:
                                texture.texture[0] = m_world->getTextureId(TextureGrassSide);
                                break;

                            case ContentFurnace:
                                if (cell->data == CELL_FURNACE_WEST)
                                    texture.texture[0] = m_world->getTextureId(TextureFurnaceFace);
                                else
                                    texture.texture[0] = m_world->getTextureId(TextureFurnaceSide);
                                break;
                        }

                        bufferTextures.push_back(texture);

                        bufferIndices.push_back(countFaces * 4 + 0);
                        bufferIndices.push_back(countFaces * 4 + 1);
                        bufferIndices.push_back(countFaces * 4 + 2);
                        bufferIndices.push_back(countFaces * 4 + 2);
                        bufferIndices.push_back(countFaces * 4 + 1);
                        bufferIndices.push_back(countFaces * 4 + 3);

                        ++countFaces;
                    }
                }
                else if ((isWater && isWaterAbove) || (isLava && isLavaAbove))
                {
                    if (cellW != nullptr && ((isWater && cellW->isWater()) || (isLava && cellW->isLava())))
                    {
                        CCell * cellWA = m_world->getCell(getChunkMinX() + x - 1, getChunkMinY() + y, z + 1);

                        if (cellWA != nullptr && ((isWater && !cellWA->isWater()) || (isLava && !cellWA->isLava())))
                        {
                            bufferBlendVertices.push_back(TVector3F((chunkMinX + x) * CellSize, (chunkMinY + y    ) * CellSize, (z + FluidTopHeight) * CellSize));
                            bufferBlendVertices.push_back(TVector3F((chunkMinX + x) * CellSize, (chunkMinY + y + 1) * CellSize, (z + FluidTopHeight) * CellSize));
                            bufferBlendVertices.push_back(TVector3F((chunkMinX + x) * CellSize, (chunkMinY + y    ) * CellSize, (z + 1)              * CellSize));
                            bufferBlendVertices.push_back(TVector3F((chunkMinX + x) * CellSize, (chunkMinY + y + 1) * CellSize, (z + 1)              * CellSize));

                            for (int c = 0; c < 4; ++c)
                            {
                                bufferBlendColors.push_back(lightingW * colorCoef.X);
                                bufferBlendColors.push_back(lightingW * colorCoef.Y);
                                bufferBlendColors.push_back(lightingW * colorCoef.Z);
                                bufferBlendColors.push_back(1.0f);
                            }

                            bufferBlendCoords.push_back(TVector2F(0.0f, 1.0f));
                            bufferBlendCoords.push_back(TVector2F(1.0f, 1.0f));
                            bufferBlendCoords.push_back(TVector2F(0.0f, FluidTopHeight));
                            bufferBlendCoords.push_back(TVector2F(1.0f, FluidTopHeight));

                            TBufferTexture texture;
                            texture.nbr = 12;
                            texture.texture[0] = m_world->getTextureId(isWater ? TextureWater : TextureLava);
                            bufferBlendTextures.push_back(texture);

                            bufferBlendIndices.push_back(countFacesBlend * 4 + 0);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 3);

                            bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 0);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 3);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 2);

                            ++countFacesBlend;
                        }
                    }
                }


                if (emptyCellE)
                {
                    if (isWater || isLava)
                    {
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y    ) * CellSize, cornerSE * CellSize));
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y + 1) * CellSize, cornerNE * CellSize));
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y    ) * CellSize, z        * CellSize));
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y + 1) * CellSize, z        * CellSize));

                        for (int c = 0; c < 4; ++c)
                        {
                            bufferBlendColors.push_back(lightingE * colorCoef.X);
                            bufferBlendColors.push_back(lightingE * colorCoef.Y);
                            bufferBlendColors.push_back(lightingE * colorCoef.Z);
                            bufferBlendColors.push_back(1.0f);
                        }

                        bufferBlendCoords.push_back(TVector2F(0.0f, 1.0f - cornerSE + z));
                        bufferBlendCoords.push_back(TVector2F(1.0f, 1.0f - cornerNE + z));
                        bufferBlendCoords.push_back(TVector2F(0.0f, 1.0f));
                        bufferBlendCoords.push_back(TVector2F(1.0f, 1.0f));

                        TBufferTexture texture;
                        texture.nbr = 12;
                        texture.texture[0] = m_world->getTextureId(isWater ? TextureWater : TextureLava);
                        bufferBlendTextures.push_back(texture);

                        bufferBlendIndices.push_back(countFacesBlend * 4 + 0);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 3);

                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 0);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 3);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);

                        ++countFacesBlend;
                    }
                    else
                    {
                        bufferVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y    ) * CellSize, (z+1) * CellSize));
                        bufferVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y + 1) * CellSize, (z+1) * CellSize));
                        bufferVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y    ) * CellSize, z     * CellSize));
                        bufferVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y + 1) * CellSize, z     * CellSize));

                        for (int c = 0; c < 4; ++c)
                        {
                            bufferColors.push_back(lightingE * colorCoef.X);
                            bufferColors.push_back(lightingE * colorCoef.Y);
                            bufferColors.push_back(lightingE * colorCoef.Z);
                            bufferColors.push_back(1.0f);
                        }

                        bufferCoords.push_back(TVector2F(0.0f, 0.0f));
                        bufferCoords.push_back(TVector2F(1.0f, 0.0f));
                        bufferCoords.push_back(TVector2F(0.0f, 1.0f));
                        bufferCoords.push_back(TVector2F(1.0f, 1.0f));

                        TBufferTexture texture;
                        texture.nbr = 6;

                        switch (cell->content)
                        {
                            case ContentEmpty:
                            case ContentWater:
                            case ContentWaterSource:
                            case ContentLava:
                            case ContentLavaSource:
                                // On n'arrive jamais ici, mais ça évite un avertissement du compilateur.
                                break;

                            case ContentBedRock:
                                texture.texture[0] = m_world->getTextureId(TextureBedRock);
                                break;

                            case ContentWoodenPlank:
                                texture.texture[0] = m_world->getTextureId(TextureWoodenPlank);
                                break;

                            case ContentCobbleStone:
                                texture.texture[0] = m_world->getTextureId(TextureCobbleStone);
                                break;

                            case ContentRock:
                                texture.texture[0] = m_world->getTextureId(TextureRock);
                                break;

                            case ContentSand:
                                texture.texture[0] = m_world->getTextureId(TextureSand);
                                break;

                            case ContentBrick:
                                texture.texture[0] = m_world->getTextureId(TextureBrick);
                                break;

                            case ContentDirt:
                                texture.texture[0] = m_world->getTextureId(TextureDirt);
                                break;

                            case ContentIronOre:
                                texture.texture[0] = m_world->getTextureId(TextureGems);
                                break;

                            case ContentWood:
                                texture.texture[0] = m_world->getTextureId(TextureWoodSide);
                                break;

                            case ContentGrass:
                                texture.texture[0] = m_world->getTextureId(TextureGrassSide);
                                break;

                            case ContentFurnace:
                                if (cell->data == CELL_FURNACE_EAST)
                                    texture.texture[0] = m_world->getTextureId(TextureFurnaceFace);
                                else
                                    texture.texture[0] = m_world->getTextureId(TextureFurnaceSide);
                                break;
                        }

                        bufferTextures.push_back(texture);

                        bufferIndices.push_back(countFaces * 4 + 0);
                        bufferIndices.push_back(countFaces * 4 + 1);
                        bufferIndices.push_back(countFaces * 4 + 2);
                        bufferIndices.push_back(countFaces * 4 + 2);
                        bufferIndices.push_back(countFaces * 4 + 1);
                        bufferIndices.push_back(countFaces * 4 + 3);

                        ++countFaces;
                    }
                }
                else if ((isWater && isWaterAbove) || (isLava && isLavaAbove))
                {
                    if (cellE != nullptr && ((isWater && cellE->isWater()) || (isLava && cellE->isLava())))
                    {
                        CCell * cellEA = m_world->getCell(getChunkMinX() + x + 1, getChunkMinY() + y, z + 1);

                        if (cellEA != nullptr && ((isWater && !cellEA->isWater()) || (isLava && !cellEA->isLava())))
                        {
                            bufferBlendVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y    ) * CellSize, (z + 1)              * CellSize));
                            bufferBlendVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y + 1) * CellSize, (z + 1)              * CellSize));
                            bufferBlendVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y    ) * CellSize, (z + FluidTopHeight) * CellSize));
                            bufferBlendVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y + 1) * CellSize, (z + FluidTopHeight) * CellSize));

                            for (int c = 0; c < 4; ++c)
                            {
                                bufferBlendColors.push_back(lightingE * colorCoef.X);
                                bufferBlendColors.push_back(lightingE * colorCoef.Y);
                                bufferBlendColors.push_back(lightingE * colorCoef.Z);
                                bufferBlendColors.push_back(1.0f);
                            }

                            bufferBlendCoords.push_back(TVector2F(0.0f, 1.0f));
                            bufferBlendCoords.push_back(TVector2F(1.0f, 1.0f));
                            bufferBlendCoords.push_back(TVector2F(0.0f, FluidTopHeight));
                            bufferBlendCoords.push_back(TVector2F(1.0f, FluidTopHeight));

                            TBufferTexture texture;
                            texture.nbr = 12;
                            texture.texture[0] = m_world->getTextureId(isWater ? TextureWater : TextureLava);
                            bufferBlendTextures.push_back(texture);

                            bufferBlendIndices.push_back(countFacesBlend * 4 + 0);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 3);

                            bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 0);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 3);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 2);

                            ++countFacesBlend;
                        }
                    }
                }

                if (emptyCellS)
                {
                    if (isWater || isLava)
                    {
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x    ) * CellSize, (chunkMinY + y) * CellSize, cornerSW * CellSize));
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y) * CellSize, cornerSE * CellSize));
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x    ) * CellSize, (chunkMinY + y) * CellSize, z        * CellSize));
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y) * CellSize, z        * CellSize));

                        for (int c = 0; c < 4; ++c)
                        {
                            bufferBlendColors.push_back(lightingS * colorCoef.X);
                            bufferBlendColors.push_back(lightingS * colorCoef.Y);
                            bufferBlendColors.push_back(lightingS * colorCoef.Z);
                            bufferBlendColors.push_back(1.0f);
                        }

                        bufferBlendCoords.push_back(TVector2F(0.0f, 1.0f - cornerSW + z));
                        bufferBlendCoords.push_back(TVector2F(1.0f, 1.0f - cornerSE + z));
                        bufferBlendCoords.push_back(TVector2F(0.0f, 1.0f));
                        bufferBlendCoords.push_back(TVector2F(1.0f, 1.0f));

                        TBufferTexture texture;
                        texture.nbr = 12;
                        texture.texture[0] = m_world->getTextureId(isWater ? TextureWater : TextureLava);
                        bufferBlendTextures.push_back(texture);

                        bufferBlendIndices.push_back(countFacesBlend * 4 + 0);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 3);

                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 0);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 3);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);

                        ++countFacesBlend;
                    }
                    else
                    {
                        bufferVertices.push_back(TVector3F((chunkMinX + x    ) * CellSize, (chunkMinY + y) * CellSize, (z+1) * CellSize));
                        bufferVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y) * CellSize, (z+1) * CellSize));
                        bufferVertices.push_back(TVector3F((chunkMinX + x    ) * CellSize, (chunkMinY + y) * CellSize, z     * CellSize));
                        bufferVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y) * CellSize, z     * CellSize));

                        for (int c = 0; c < 4; ++c)
                        {
                            bufferColors.push_back(lightingS * colorCoef.X);
                            bufferColors.push_back(lightingS * colorCoef.Y);
                            bufferColors.push_back(lightingS * colorCoef.Z);
                            bufferColors.push_back(1.0f);
                        }

                        bufferCoords.push_back(TVector2F(0.0f, 0.0f));
                        bufferCoords.push_back(TVector2F(1.0f, 0.0f));
                        bufferCoords.push_back(TVector2F(0.0f, 1.0f));
                        bufferCoords.push_back(TVector2F(1.0f, 1.0f));

                        TBufferTexture texture;
                        texture.nbr = 6;

                        switch (cell->content)
                        {
                            case ContentEmpty:
                            case ContentWater:
                            case ContentWaterSource:
                            case ContentLava:
                            case ContentLavaSource:
                                // On n'arrive jamais ici, mais ça évite un avertissement du compilateur.
                                break;

                            case ContentBedRock:
                                texture.texture[0] = m_world->getTextureId(TextureBedRock);
                                break;

                            case ContentWoodenPlank:
                                texture.texture[0] = m_world->getTextureId(TextureWoodenPlank);
                                break;

                            case ContentCobbleStone:
                                texture.texture[0] = m_world->getTextureId(TextureCobbleStone);
                                break;

                            case ContentRock:
                                texture.texture[0] = m_world->getTextureId(TextureRock);
                                break;

                            case ContentSand:
                                texture.texture[0] = m_world->getTextureId(TextureSand);
                                break;

                            case ContentBrick:
                                texture.texture[0] = m_world->getTextureId(TextureBrick);
                                break;

                            case ContentDirt:
                                texture.texture[0] = m_world->getTextureId(TextureDirt);
                                break;

                            case ContentIronOre:
                                texture.texture[0] = m_world->getTextureId(TextureGems);
                                break;

                            case ContentWood:
                                texture.texture[0] = m_world->getTextureId(TextureWoodSide);
                                break;

                            case ContentGrass:
                                texture.texture[0] = m_world->getTextureId(TextureGrassSide);
                                break;

                            case ContentFurnace:
                                if (cell->data == CELL_FURNACE_SOUTH)
                                    texture.texture[0] = m_world->getTextureId(TextureFurnaceFace);
                                else
                                    texture.texture[0] = m_world->getTextureId(TextureFurnaceSide);
                                break;
                        }

                        bufferTextures.push_back(texture);

                        bufferIndices.push_back(countFaces * 4 + 0);
                        bufferIndices.push_back(countFaces * 4 + 1);
                        bufferIndices.push_back(countFaces * 4 + 2);
                        bufferIndices.push_back(countFaces * 4 + 2);
                        bufferIndices.push_back(countFaces * 4 + 1);
                        bufferIndices.push_back(countFaces * 4 + 3);

                        ++countFaces;
                    }
                }
                else if ((isWater && isWaterAbove) || (isLava && isLavaAbove))
                {
                    if (cellS != nullptr && ((isWater && cellS->isWater()) || (isLava && cellS->isLava())))
                    {
                        CCell * cellSA = m_world->getCell(getChunkMinX() + x, getChunkMinY() + y - 1, z + 1);

                        if (cellSA != nullptr && ((isWater && !cellSA->isWater()) || (isLava && !cellSA->isLava())))
                        {
                            bufferBlendVertices.push_back(TVector3F((chunkMinX + x    ) * CellSize, (chunkMinY + y) * CellSize, (z + 1)              * CellSize));
                            bufferBlendVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y) * CellSize, (z + 1)              * CellSize));
                            bufferBlendVertices.push_back(TVector3F((chunkMinX + x    ) * CellSize, (chunkMinY + y) * CellSize, (z + FluidTopHeight) * CellSize));
                            bufferBlendVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y) * CellSize, (z + FluidTopHeight) * CellSize));

                            for (int c = 0; c < 4; ++c)
                            {
                                bufferBlendColors.push_back(lightingS * colorCoef.X);
                                bufferBlendColors.push_back(lightingS * colorCoef.Y);
                                bufferBlendColors.push_back(lightingS * colorCoef.Z);
                                bufferBlendColors.push_back(1.0f);
                            }

                            bufferBlendCoords.push_back(TVector2F(0.0f, 1.0f));
                            bufferBlendCoords.push_back(TVector2F(1.0f, 1.0f));
                            bufferBlendCoords.push_back(TVector2F(0.0f, FluidTopHeight));
                            bufferBlendCoords.push_back(TVector2F(1.0f, FluidTopHeight));

                            TBufferTexture texture;
                            texture.nbr = 12;
                            texture.texture[0] = m_world->getTextureId(isWater ? TextureWater : TextureLava);
                            bufferBlendTextures.push_back(texture);

                            bufferBlendIndices.push_back(countFacesBlend * 4 + 0);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 3);

                            bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 0);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 3);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 2);

                            ++countFacesBlend;
                        }
                    }
                }

                if (emptyCellN)
                {
                    if (isWater || isLava)
                    {
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x    ) * CellSize, (chunkMinY + y + 1) * CellSize, z        * CellSize));
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y + 1) * CellSize, z        * CellSize));
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x    ) * CellSize, (chunkMinY + y + 1) * CellSize, cornerNW * CellSize));
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y + 1) * CellSize, cornerNE * CellSize));

                        for (int c = 0; c < 4; ++c)
                        {
                            bufferBlendColors.push_back(lightingN * colorCoef.X);
                            bufferBlendColors.push_back(lightingN * colorCoef.Y);
                            bufferBlendColors.push_back(lightingN * colorCoef.Z);
                            bufferBlendColors.push_back(1.0f);
                        }

                        bufferBlendCoords.push_back(TVector2F(0.0f, 1.0f));
                        bufferBlendCoords.push_back(TVector2F(1.0f, 1.0f));
                        bufferBlendCoords.push_back(TVector2F(0.0f, 1.0f - cornerNW + z));
                        bufferBlendCoords.push_back(TVector2F(1.0f, 1.0f - cornerNE + z));

                        TBufferTexture texture;
                        texture.nbr = 12;
                        texture.texture[0] = m_world->getTextureId(isWater ? TextureWater : TextureLava);
                        bufferBlendTextures.push_back(texture);

                        bufferBlendIndices.push_back(countFacesBlend * 4 + 0);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 3);

                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 0);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 3);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);

                        ++countFacesBlend;
                    }
                    else
                    {
                        bufferVertices.push_back(TVector3F((chunkMinX + x    ) * CellSize, (chunkMinY + y + 1) * CellSize, z     * CellSize));
                        bufferVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y + 1) * CellSize, z     * CellSize));
                        bufferVertices.push_back(TVector3F((chunkMinX + x    ) * CellSize, (chunkMinY + y + 1) * CellSize, (z+1) * CellSize));
                        bufferVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y + 1) * CellSize, (z+1) * CellSize));

                        for (int c = 0; c < 4; ++c)
                        {
                            bufferColors.push_back(lightingN * colorCoef.X);
                            bufferColors.push_back(lightingN * colorCoef.Y);
                            bufferColors.push_back(lightingN * colorCoef.Z);
                            bufferColors.push_back(1.0f);
                        }

                        bufferCoords.push_back(TVector2F(0.0f, 1.0f));
                        bufferCoords.push_back(TVector2F(1.0f, 1.0f));
                        bufferCoords.push_back(TVector2F(0.0f, 0.0f));
                        bufferCoords.push_back(TVector2F(1.0f, 0.0f));

                        TBufferTexture texture;
                        texture.nbr = 6;

                        switch (cell->content)
                        {
                            case ContentEmpty:
                            case ContentWater:
                            case ContentWaterSource:
                            case ContentLava:
                            case ContentLavaSource:
                                // On n'arrive jamais ici, mais ça évite un avertissement du compilateur.
                                break;

                            case ContentBedRock:
                                texture.texture[0] = m_world->getTextureId(TextureBedRock);
                                break;

                            case ContentWoodenPlank:
                                texture.texture[0] = m_world->getTextureId(TextureWoodenPlank);
                                break;

                            case ContentCobbleStone:
                                texture.texture[0] = m_world->getTextureId(TextureCobbleStone);
                                break;

                            case ContentRock:
                                texture.texture[0] = m_world->getTextureId(TextureRock);
                                break;

                            case ContentSand:
                                texture.texture[0] = m_world->getTextureId(TextureSand);
                                break;

                            case ContentBrick:
                                texture.texture[0] = m_world->getTextureId(TextureBrick);
                                break;

                            case ContentDirt:
                                texture.texture[0] = m_world->getTextureId(TextureDirt);
                                break;

                            case ContentIronOre:
                                texture.texture[0] = m_world->getTextureId(TextureGems);
                                break;

                            case ContentWood:
                                texture.texture[0] = m_world->getTextureId(TextureWoodSide);
                                break;

                            case ContentGrass:
                                texture.texture[0] = m_world->getTextureId(TextureGrassSide);
                                break;

                            case ContentFurnace:
                                if (cell->data == CELL_FURNACE_NORTH)
                                    texture.texture[0] = m_world->getTextureId(TextureFurnaceFace);
                                else
                                    texture.texture[0] = m_world->getTextureId(TextureFurnaceSide);
                                break;
                        }

                        bufferTextures.push_back(texture);

                        bufferIndices.push_back(countFaces * 4 + 0);
                        bufferIndices.push_back(countFaces * 4 + 1);
                        bufferIndices.push_back(countFaces * 4 + 2);
                        bufferIndices.push_back(countFaces * 4 + 2);
                        bufferIndices.push_back(countFaces * 4 + 1);
                        bufferIndices.push_back(countFaces * 4 + 3);

                        ++countFaces;
                    }
                }
                else if ((isWater && isWaterAbove) || (isLava && isLavaAbove))
                {
                    if (cellN != nullptr && ((isWater && cellN->isWater()) || (isLava && cellN->isLava())))
                    {
                        CCell * cellNA = m_world->getCell(getChunkMinX() + x, getChunkMinY() + y + 1, z + 1);

                        if (cellNA != nullptr && ((isWater && !cellNA->isWater()) || (isLava && !cellNA->isLava())))
                        {
                            bufferBlendVertices.push_back(TVector3F((chunkMinX + x    ) * CellSize, (chunkMinY + y + 1) * CellSize, (z + FluidTopHeight) * CellSize));
                            bufferBlendVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y + 1) * CellSize, (z + FluidTopHeight) * CellSize));
                            bufferBlendVertices.push_back(TVector3F((chunkMinX + x    ) * CellSize, (chunkMinY + y + 1) * CellSize, (z + 1)              * CellSize));
                            bufferBlendVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y + 1) * CellSize, (z + 1)              * CellSize));

                            for (int c = 0; c < 4; ++c)
                            {
                                bufferBlendColors.push_back(lightingN * colorCoef.X);
                                bufferBlendColors.push_back(lightingN * colorCoef.Y);
                                bufferBlendColors.push_back(lightingN * colorCoef.Z);
                                bufferBlendColors.push_back(1.0f);
                            }

                            bufferBlendCoords.push_back(TVector2F(0.0f, 1.0f));
                            bufferBlendCoords.push_back(TVector2F(1.0f, 1.0f));
                            bufferBlendCoords.push_back(TVector2F(0.0f, FluidTopHeight));
                            bufferBlendCoords.push_back(TVector2F(1.0f, FluidTopHeight));

                            TBufferTexture texture;
                            texture.nbr = 12;
                            texture.texture[0] = m_world->getTextureId(isWater ? TextureWater : TextureLava);
                            bufferBlendTextures.push_back(texture);

                            bufferBlendIndices.push_back(countFacesBlend * 4 + 0);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 3);

                            bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 0);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 3);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                            bufferBlendIndices.push_back(countFacesBlend * 4 + 2);

                            ++countFacesBlend;
                        }
                    }
                }

                if (z <= 0 || isCellEmpty(x, y, z - 1, !isWater, !isLava))
                {
                    if (isWater || isLava)
                    {
                        // Affichage de la face inférieure de l'eau (en cas de chute d'eau en cours, ou de l'eau placé sur une surface transparente)

                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y    ) * CellSize, z * CellSize));
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y + 1) * CellSize, z * CellSize));
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x    ) * CellSize, (chunkMinY + y    ) * CellSize, z * CellSize));
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x    ) * CellSize, (chunkMinY + y + 1) * CellSize, z * CellSize));

                        float lighting = lightingMin + lightingRange * (z <= 0 ? 0 : getCellLighting(x, y, z - 1)) / 15.0f;

                        for (int c = 0; c < 4; ++c)
                        {
                            bufferBlendColors.push_back(lighting * colorCoef.X);
                            bufferBlendColors.push_back(lighting * colorCoef.Y);
                            bufferBlendColors.push_back(lighting * colorCoef.Z);
                            bufferBlendColors.push_back(1.0f);
                        }

                        bufferBlendCoords.push_back(TVector2F(0.0f, 0.0f));
                        bufferBlendCoords.push_back(TVector2F(1.0f, 0.0f));
                        bufferBlendCoords.push_back(TVector2F(0.0f, 1.0f));
                        bufferBlendCoords.push_back(TVector2F(1.0f, 1.0f));

                        TBufferTexture texture;
                        texture.nbr = 12;
                        texture.texture[0] = m_world->getTextureId(isWater ? TextureWater : TextureLava);
                        bufferBlendTextures.push_back(texture);

                        bufferBlendIndices.push_back(countFacesBlend * 4 + 0);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 3);

                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 0);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 3);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);

                        ++countFacesBlend;
                    }
                    else
                    {
                        bufferVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y    ) * CellSize, z * CellSize));
                        bufferVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y + 1) * CellSize, z * CellSize));
                        bufferVertices.push_back(TVector3F((chunkMinX + x    ) * CellSize, (chunkMinY + y    ) * CellSize, z * CellSize));
                        bufferVertices.push_back(TVector3F((chunkMinX + x    ) * CellSize, (chunkMinY + y + 1) * CellSize, z * CellSize));

                        float lighting = lightingMin + lightingRange * (z <= 0 ? 0 : getCellLighting(x, y, z - 1)) / 15.0f;

                        for (int c = 0; c < 4; ++c)
                        {
                            bufferColors.push_back(lighting * colorCoef.X);
                            bufferColors.push_back(lighting * colorCoef.Y);
                            bufferColors.push_back(lighting * colorCoef.Z);
                            bufferColors.push_back(1.0f);
                        }

                        bufferCoords.push_back(TVector2F(0.0f, 0.0f));
                        bufferCoords.push_back(TVector2F(1.0f, 0.0f));
                        bufferCoords.push_back(TVector2F(0.0f, 1.0f));
                        bufferCoords.push_back(TVector2F(1.0f, 1.0f));

                        TBufferTexture texture;
                        texture.nbr = 6;

                        switch (cell->content)
                        {
                            case ContentEmpty:
                            case ContentWater:
                            case ContentWaterSource:
                            case ContentLava:
                            case ContentLavaSource:
                                // On n'arrive jamais ici, mais ça évite un avertissement du compilateur.
                                break;

                            case ContentBedRock:
                                texture.texture[0] = m_world->getTextureId(TextureBedRock);
                                break;

                            case ContentWoodenPlank:
                                texture.texture[0] = m_world->getTextureId(TextureWoodenPlank);
                                break;

                            case ContentCobbleStone:
                                texture.texture[0] = m_world->getTextureId(TextureCobbleStone);
                                break;

                            case ContentRock:
                                texture.texture[0] = m_world->getTextureId(TextureRock);
                                break;

                            case ContentSand:
                                texture.texture[0] = m_world->getTextureId(TextureSand);
                                break;

                            case ContentBrick:
                                texture.texture[0] = m_world->getTextureId(TextureBrick);
                                break;

                            case ContentWood:
                                texture.texture[0] = m_world->getTextureId(TextureWood);
                                break;

                            case ContentDirt:
                            case ContentGrass:
                                texture.texture[0] = m_world->getTextureId(TextureDirt);
                                break;

                            case ContentIronOre:
                                texture.texture[0] = m_world->getTextureId(TextureGems);
                                break;

                            case ContentFurnace:
                                texture.texture[0] = m_world->getTextureId(TextureFurnaceTop);
                                break;
                        }

                        bufferTextures.push_back(texture);

                        bufferIndices.push_back(countFaces * 4 + 0);
                        bufferIndices.push_back(countFaces * 4 + 1);
                        bufferIndices.push_back(countFaces * 4 + 2);
                        bufferIndices.push_back(countFaces * 4 + 2);
                        bufferIndices.push_back(countFaces * 4 + 1);
                        bufferIndices.push_back(countFaces * 4 + 3);

                        ++countFaces;
                    }
                }

                if (isWater || isLava)
                {
                    if (z >= (ChunkHeight - 1) || (isWater && !getCell(x, y, z + 1)->isWater()) || (isLava && !getCell(x, y, z + 1)->isLava()))
                    {
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x    ) * CellSize, (chunkMinY + y    ) * CellSize, cornerSW * CellSize));
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x    ) * CellSize, (chunkMinY + y + 1) * CellSize, cornerNW * CellSize));
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y    ) * CellSize, cornerSE * CellSize));
                        bufferBlendVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y + 1) * CellSize, cornerNE * CellSize));

                        float lighting = lightingMin + lightingRange * getCellLighting(x, y, topZ) / 15.0f; // z ou z+1 pour la surface ?

                        for (int c = 0; c < 4; ++c)
                        {
                            bufferBlendColors.push_back(lighting * colorCoef.X);
                            bufferBlendColors.push_back(lighting * colorCoef.Y);
                            bufferBlendColors.push_back(lighting * colorCoef.Z);
                            bufferBlendColors.push_back(1.0f);
                        }

                        bufferBlendCoords.push_back(TVector2F(0.0f, 0.0f));
                        bufferBlendCoords.push_back(TVector2F(1.0f, 0.0f));
                        bufferBlendCoords.push_back(TVector2F(0.0f, 1.0f));
                        bufferBlendCoords.push_back(TVector2F(1.0f, 1.0f));

                        TBufferTexture texture;
                        texture.nbr = 12;
                        texture.texture[0] = m_world->getTextureId(isWater ? TextureWater : TextureLava);
                        bufferBlendTextures.push_back(texture);

                        bufferBlendIndices.push_back(countFacesBlend * 4 + 0);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 3);

                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 0);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 3);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 1);
                        bufferBlendIndices.push_back(countFacesBlend * 4 + 2);

                        ++countFacesBlend;
                    }
                }
                else
                {
                    if (z >= (ChunkHeight - 1) || isCellEmpty(x, y, z + 1))
                    {
                        bufferVertices.push_back(TVector3F((chunkMinX + x    ) * CellSize, (chunkMinY + y    ) * CellSize, (z+1) * CellSize));
                        bufferVertices.push_back(TVector3F((chunkMinX + x    ) * CellSize, (chunkMinY + y + 1) * CellSize, (z+1) * CellSize));
                        bufferVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y    ) * CellSize, (z+1) * CellSize));
                        bufferVertices.push_back(TVector3F((chunkMinX + x + 1) * CellSize, (chunkMinY + y + 1) * CellSize, (z+1) * CellSize));

                        float lighting = lightingMin + lightingRange * getCellLighting(x, y, z + 1) / 15.0f;

                        for (int c = 0; c < 4; ++c)
                        {
                            bufferColors.push_back(lighting * colorCoef.X);
                            bufferColors.push_back(lighting * colorCoef.Y);
                            bufferColors.push_back(lighting * colorCoef.Z);
                            bufferColors.push_back(1.0f);
                        }

                        bufferCoords.push_back(TVector2F(0.0f, 0.0f));
                        bufferCoords.push_back(TVector2F(1.0f, 0.0f));
                        bufferCoords.push_back(TVector2F(0.0f, 1.0f));
                        bufferCoords.push_back(TVector2F(1.0f, 1.0f));

                        TBufferTexture texture;
                        texture.nbr = 6;

                        switch (cell->content)
                        {
                            case ContentEmpty:
                            case ContentWater:
                            case ContentWaterSource:
                            case ContentLava:
                            case ContentLavaSource:
                                // On n'arrive jamais ici, mais ça évite un avertissement du compilateur.
                                break;

                            case ContentBedRock:
                                texture.texture[0] = m_world->getTextureId(TextureBedRock);
                                break;

                            case ContentWoodenPlank:
                                texture.texture[0] = m_world->getTextureId(TextureWoodenPlank);
                                break;

                            case ContentCobbleStone:
                                texture.texture[0] = m_world->getTextureId(TextureCobbleStone);
                                break;

                            case ContentRock:
                                texture.texture[0] = m_world->getTextureId(TextureRock);
                                break;

                            case ContentSand:
                                texture.texture[0] = m_world->getTextureId(TextureSand);
                                break;

                            case ContentBrick:
                                texture.texture[0] = m_world->getTextureId(TextureBrick);
                                break;

                            case ContentDirt:
                                texture.texture[0] = m_world->getTextureId(TextureDirt);
                                break;

                            case ContentIronOre:
                                texture.texture[0] = m_world->getTextureId(TextureGems);
                                break;

                            case ContentWood:
                                texture.texture[0] = m_world->getTextureId(TextureWood);
                                break;

                            case ContentFurnace:
                                texture.texture[0] = m_world->getTextureId(TextureFurnaceTop);
                                break;

                            case ContentGrass:
                                texture.texture[0] = m_world->getTextureId(TextureGrass);
                                break;
                        }

                        bufferTextures.push_back(texture);

                        bufferIndices.push_back(countFaces * 4 + 0);
                        bufferIndices.push_back(countFaces * 4 + 1);
                        bufferIndices.push_back(countFaces * 4 + 2);
                        bufferIndices.push_back(countFaces * 4 + 2);
                        bufferIndices.push_back(countFaces * 4 + 1);
                        bufferIndices.push_back(countFaces * 4 + 3);

                        ++countFaces;
                    }
                }
            }
        }
    }

    std::cout << "Chunk " << m_chunkX << " x " << m_chunkY << " : buffer updated (" << countFaces << " faces, " << countFacesBlend << " transparentes)" << std::endl;

    m_buffer->update();
    m_bufferBlend->update();
    m_needBufferUpdate = false;
}


/**
 * Met à jour le chunk : écoulement de l'eau.
 */

void CChunk::updateChunk(int frameTime)
{
    if (m_cellsUpdate.empty())
    {
        return;
    }

    std::cout << "Chunk " << m_chunkX << " x " << m_chunkY << " : updating..." << std::endl;
/*
    for (std::list<TCellCoords>::const_iterator it = m_cellsUpdate.begin(); it != m_cellsUpdate.end(); ++it)
    {
        std::cout << " - " << it->x << " x " << it->y << " x " << it->z << std::endl;
    }
*/

    std::list<TCellCoords> cellsUpdate = m_cellsUpdate;
    m_cellsUpdate.clear();

    for (std::list<TCellCoords>::const_iterator it = cellsUpdate.begin(); it != cellsUpdate.end(); ++it)
    {
        int x = it->x;
        int y = it->y;
        int z = it->z;

        CCell * cell = getCell(x, y, z);

        if (cell == nullptr)
        {
            continue;
        }

        if (cell->content == ContentEmpty ||
            cell->content == ContentWater ||
            cell->content == ContentLava)
        {
            CCell * cellT = getCell(x, y, z + 1);

            // Chute du fluide
            if (z < (ChunkHeight - 1) && cellT != nullptr)
            {
                if (cellT->isWater())
                {
                    if (getCellContent(x, y, z) != ContentWater || getCellWaterLevel(x, y, z) != 7)
                    {
                        setCellContent(x, y, z, ContentWater);
                        setWaterLevel(x, y, z, 7);
                        m_needBufferUpdate = true;

                        // Demande de mise à jour des cellules adjacentes
                        updateFluidInNeighboors(x, y, z);
                    }

                    setCellNeedUpdate(x, y, z, false);
                    continue;
                }
                else if (cellT->isLava())
                {
                    if (getCellContent(x, y, z) != ContentLava || getCellLavaLevel(x, y, z) != 3)
                    {
                        setCellContent(x, y, z, ContentLava);
                        setLavaLevel(x, y, z, 3);
                        m_needBufferUpdate = true;

                        // Demande de mise à jour des cellules adjacentes
                        updateFluidInNeighboors(x, y, z);
                    }

                    setCellNeedUpdate(x, y, z, false);
                    continue;
                }
            }


            // Écoulement du fluide
            int maxWaterLevel = 0;
            int maxLavaLevel = 0;
            int numSources = 0;

            bool waterFromN = false;
            bool waterFromE = false;
            bool waterFromS = false;
            bool waterFromW = false;

            bool lavaFromN = false;
            bool lavaFromE = false;
            bool lavaFromS = false;
            bool lavaFromW = false;

            // Cellules adjacentes
            CCell * cellN = m_world->getCell(getChunkMinX() + x, getChunkMinY() + y + 1, z);
            CCell * cellE = m_world->getCell(getChunkMinX() + x + 1, getChunkMinY() + y, z);
            CCell * cellS = m_world->getCell(getChunkMinX() + x, getChunkMinY() + y - 1, z);
            CCell * cellW = m_world->getCell(getChunkMinX() + x - 1, getChunkMinY() + y, z);

            if (cellN != nullptr)
            {
                if (cellN->content == ContentWaterSource)
                {
                    ++numSources;
                    maxWaterLevel = 7;
                    waterFromN = true;
                }
                else if (cellN->content == ContentWater)
                {
                    waterFromN = true;

                    if (cellN->getWaterLevel() > maxWaterLevel)
                    {
                        maxWaterLevel = cellN->getWaterLevel();
                    }
                }
                else if (cellN->content == ContentLavaSource)
                {
                    maxLavaLevel = 3;
                    lavaFromN = true;
                }
                else if (cellN->content == ContentLava)
                {
                    lavaFromN = true;

                    if (cellN->getLavaLevel() > maxLavaLevel)
                    {
                        maxLavaLevel = cellN->getLavaLevel();
                    }
                }
            }

            if (cellE != nullptr)
            {
                if (cellE->content == ContentWaterSource)
                {
                    ++numSources;
                    maxWaterLevel = 7;
                    waterFromE = true;
                }
                else if (cellE->content == ContentWater)
                {
                    waterFromE = true;

                    if (cellE->getWaterLevel() > maxWaterLevel)
                    {
                        maxWaterLevel = cellE->getWaterLevel();
                    }
                }
                else if (cellE->content == ContentLavaSource)
                {
                    maxLavaLevel = 3;
                    lavaFromE = true;
                }
                else if (cellE->content == ContentLava)
                {
                    lavaFromE = true;

                    if (cellE->getLavaLevel() > maxLavaLevel)
                    {
                        maxLavaLevel = cellE->getLavaLevel();
                    }
                }
            }

            if (cellS != nullptr)
            {
                if (cellS->content == ContentWaterSource)
                {
                    ++numSources;
                    maxWaterLevel = 7;
                    waterFromS = true;
                }
                else if (cellS->content == ContentWater)
                {
                    waterFromS = true;

                    if (cellS->getWaterLevel() > maxWaterLevel)
                    {
                        maxWaterLevel = cellS->getWaterLevel();
                    }
                }
                else if (cellS->content == ContentLavaSource)
                {
                    maxLavaLevel = 3;
                    lavaFromS = true;
                }
                else if (cellS->content == ContentLava)
                {
                    lavaFromS = true;

                    if (cellS->getLavaLevel() > maxLavaLevel)
                    {
                        maxLavaLevel = cellS->getLavaLevel();
                    }
                }
            }

            if (cellW != nullptr)
            {
                if (cellW->content == ContentWaterSource)
                {
                    ++numSources;
                    maxWaterLevel = 7;
                    waterFromW = true;
                }
                else if (cellW->content == ContentWater)
                {
                    waterFromW = true;

                    if (cellW->getWaterLevel() > maxWaterLevel)
                    {
                        maxWaterLevel = cellW->getWaterLevel();
                    }
                }
                else if (cellW->content == ContentLavaSource)
                {
                    maxLavaLevel = 3;
                    lavaFromW = true;
                }
                else if (cellW->content == ContentLava)
                {
                    lavaFromW = true;

                    if (cellW->getLavaLevel() > maxLavaLevel)
                    {
                        maxLavaLevel = cellW->getLavaLevel();
                    }
                }
            }

            // Cellule vide
            if (maxWaterLevel == 0 && maxLavaLevel == 0)
            {
                if (getCellContent(x, y, z) != ContentEmpty)
                {
                    setCellContent(x, y, z, ContentEmpty);
                    updateFluidInNeighboors(x, y, z);
                    m_needBufferUpdate = true;
                }
            }
            // Création de roche
            else if (maxWaterLevel > 0 && maxLavaLevel > 0)
            {
                if (getCellContent(x, y, z) != ContentRock)
                {
                    setCellContent(x, y, z, ContentRock);
                    m_needBufferUpdate = true;
                }
            }
            // Création d'une source
            else if (numSources >= 2)
            {
                std::cout << "Creation d'une source d'eau en " << x << " x " << y << " x " << z << std::endl;

                setCellContent(x, y, z, ContentWaterSource);
                updateFluidInNeighboors(x, y, z);
                m_needBufferUpdate = true;
            }
            else if (maxWaterLevel > 0 && (getCellContent(x, y, z) != ContentWater || getCellWaterLevel(x, y, z) != maxWaterLevel - 1))
            {
                // Changement du niveau de l'eau de la cellule.

                if (z > 0 && isCellEmpty(x, y, z - 1))
                {
                    // La cellule inférieure est vide, soit on est en bordure d'une chute
                    // d'eau, soit on est en bordure d'une falaise et l'eau doit y couler.
                    // On doit vérifier les cellules situées sous les blocs d'eau adjacents.

                    bool hasWater = false;

                    if (waterFromN && !hasWater)
                    {
                        CCell * cellNB = m_world->getCell(getChunkMinX() + x, getChunkMinY() + y + 1, z - 1);

                        if (cellNB != nullptr &&
                            cellNB->content != ContentEmpty &&
                            cellNB->content != ContentWater &&
                            cellNB->content != ContentWaterSource)
                        {
                            hasWater = true;
                        }
                    }

                    if (waterFromE && !hasWater)
                    {
                        CCell * cellEB = m_world->getCell(getChunkMinX() + x + 1, getChunkMinY() + y, z - 1);

                        if (cellEB != nullptr &&
                            cellEB->content != ContentEmpty &&
                            cellEB->content != ContentWater &&
                            cellEB->content != ContentWaterSource)
                        {
                            hasWater = true;
                        }
                    }

                    if (waterFromS && !hasWater)
                    {
                        CCell * cellSB = m_world->getCell(getChunkMinX() + x, getChunkMinY() + y - 1, z - 1);

                        if (cellSB != nullptr &&
                            cellSB->content != ContentEmpty &&
                            cellSB->content != ContentWater &&
                            cellSB->content != ContentWaterSource)
                        {
                            hasWater = true;
                        }
                    }

                    if (waterFromW && !hasWater)
                    {
                        CCell * cellWB = m_world->getCell(getChunkMinX() + x - 1, getChunkMinY() + y, z - 1);

                        if (cellWB != nullptr &&
                            cellWB->content != ContentEmpty &&
                            cellWB->content != ContentWater &&
                            cellWB->content != ContentWaterSource)
                        {
                            hasWater = true;
                        }
                    }

                    if (hasWater)
                    {
                        std::cout << "  DEBUG: cas special Water-123 : rien en-dessous -> chute d'eau" << std::endl;

                        setCellContent(x, y, z, ContentWater);
                        setWaterLevel(x, y, z, maxWaterLevel - 1);
                        updateFluidInNeighboors(x, y, z);
                        m_needBufferUpdate = true;
                    }
                    else
                    {
                        std::cout << "  DEBUG: cas special Water-123 : rien en-dessous -> pas d'eau" << std::endl;

                        if (getCellContent(x, y, z) != ContentEmpty)
                        {
                            setCellContent(x, y, z, ContentEmpty);
                            updateFluidInNeighboors(x, y, z);
                            m_needBufferUpdate = true;
                        }
                    }
                }
                else
                {
                    setCellContent(x, y, z, ContentWater);
                    setWaterLevel(x, y, z, maxWaterLevel - 1);
                    updateFluidInNeighboors(x, y, z);
                    m_needBufferUpdate = true;
                }
            }
            else if (maxLavaLevel > 0 && (getCellContent(x, y, z) != ContentLava || getCellLavaLevel(x, y, z) != maxLavaLevel - 1))
            {
                // Changement du niveau de la lave de la cellule.

                if (z > 0 && isCellEmpty(x, y, z - 1))
                {
                    // La cellule inférieure est vide, soit on est en bordure d'une chute
                    // de lave, soit on est en bordure d'une falaise et la lave doit y couler.
                    // On doit vérifier les cellules situées sous les blocs de lave adjacents.

                    bool hasLava = false;

                    if (lavaFromN && !hasLava)
                    {
                        CCell * cellNB = m_world->getCell(getChunkMinX() + x, getChunkMinY() + y + 1, z - 1);

                        if (cellNB != nullptr &&
                            cellNB->content != ContentEmpty &&
                            cellNB->content != ContentLava &&
                            cellNB->content != ContentLavaSource)
                        {
                            hasLava = true;
                        }
                    }

                    if (lavaFromE && !hasLava)
                    {
                        CCell * cellEB = m_world->getCell(getChunkMinX() + x + 1, getChunkMinY() + y, z - 1);

                        if (cellEB != nullptr &&
                            cellEB->content != ContentEmpty &&
                            cellEB->content != ContentLava &&
                            cellEB->content != ContentLavaSource)
                        {
                            hasLava = true;
                        }
                    }

                    if (lavaFromS && !hasLava)
                    {
                        CCell * cellSB = m_world->getCell(getChunkMinX() + x, getChunkMinY() + y - 1, z - 1);

                        if (cellSB != nullptr &&
                            cellSB->content != ContentEmpty &&
                            cellSB->content != ContentLava &&
                            cellSB->content != ContentLavaSource)
                        {
                            hasLava = true;
                        }
                    }

                    if (lavaFromW && !hasLava)
                    {
                        CCell * cellWB = m_world->getCell(getChunkMinX() + x - 1, getChunkMinY() + y, z - 1);

                        if (cellWB != nullptr &&
                            cellWB->content != ContentEmpty &&
                            cellWB->content != ContentLava &&
                            cellWB->content != ContentLavaSource)
                        {
                            hasLava = true;
                        }
                    }

                    if (hasLava)
                    {
                        std::cout << "  DEBUG: cas special Lava-123 : rien en-dessous -> chute de lave" << std::endl;

                        setCellContent(x, y, z, ContentLava);
                        setLavaLevel(x, y, z, maxLavaLevel - 1);
                        updateFluidInNeighboors(x, y, z);
                        m_needBufferUpdate = true;
                    }
                    else
                    {
                        std::cout << "  DEBUG: cas special Lava-123 : rien en-dessous -> pas de lave" << std::endl;

                        if (getCellContent(x, y, z) != ContentEmpty)
                        {
                            setCellContent(x, y, z, ContentEmpty);
                            updateFluidInNeighboors(x, y, z);
                            m_needBufferUpdate = true;
                        }
                    }
                }
                else
                {
                    setCellContent(x, y, z, ContentLava);
                    setLavaLevel(x, y, z, maxLavaLevel - 1);
                    updateFluidInNeighboors(x, y, z);
                    m_needBufferUpdate = true;
                }
            }

            setCellNeedUpdate(x, y, z, false);

            // Mise à jour graphique des chunks adjacents
            if (x == 0)
            {
                CChunk * chunk = m_world->getChunk(m_chunkX - 1, m_chunkY);

                if (chunk != nullptr)
                {
                    chunk->m_needBufferUpdate = true;
                }
            }
            else if (x == ChunkSize - 1)
            {
                CChunk * chunk = m_world->getChunk(m_chunkX + 1, m_chunkY);

                if (chunk != nullptr)
                {
                    chunk->m_needBufferUpdate = true;
                }
            }

            if (y == 0)
            {
                CChunk * chunk = m_world->getChunk(m_chunkX, m_chunkY - 1);

                if (chunk != nullptr)
                {
                    chunk->m_needBufferUpdate = true;
                }
            }
            else if (y == ChunkSize - 1)
            {
                CChunk * chunk = m_world->getChunk(m_chunkX, m_chunkY + 1);

                if (chunk != nullptr)
                {
                    chunk->m_needBufferUpdate = true;
                }
            }
        }
    }
}


/**
 * Affiche le chunk.
 */

void CChunk::display()
{
    m_buffer->draw();
}


/**
 * Affiche les surfaces transparentes du chunk.
 */

void CChunk::displayBlend()
{
    m_bufferBlend->draw();
}


/**
 * Calcule la luminosité des cellules vides du chunk.
 */

void CChunk::computeLighting()
{
    std::cout << "Chunk " << m_chunkX << " x " << m_chunkY << " : compute lighting" << std::endl;

    // Lumière directe
    for (int x = 0; x < ChunkSize; ++x)
    {
        for (int y = 0; y < ChunkSize; ++y)
        {
            for (int z = ChunkHeight - 1; z > m_heightMap[x + y * ChunkSize]; --z)
            {
                //if (getCellContent(x, y, z) == ContentEmpty)
                {
                    updateCellLighting(x, y, z, 15);
                }
            }
        }
    }
}


int CChunk::getHeight(int x, int y) const
{
    return m_heightMap[x + y * ChunkSize];
}


/**
 * Modifie le contenu d'une cellule.
 *
 * \param x Coordonnée X de la cellule dans le chunk.
 * \param y Coordonnée Y de la cellule dans le chunk.
 * \param z Coordonnée Z de la cellule dans le chunk.
 * \param content Contenu de la cellule.
 */

void CChunk::setCellContent(int x, int y, int z, TCellContent content, int data)
{
    // Vérification des bornes
         if (z < 0) z = 0;
    else if (z >= ChunkHeight) z = ChunkHeight - 1;

    m_sectors[z / ChunkSize].setCellContent(x, y, Modulo(z, ChunkSize), content, data);
}


void CChunk::updateCellLighting(int x, int y, int z, int lighting)
{
    // Vérification des bornes
         if (x < 0) x = 0;
    else if (x >= ChunkSize) x = ChunkSize - 1;
         if (y < 0) y = 0;
    else if (y >= ChunkSize) y = ChunkSize - 1;
         if (z < 0) z = 0;
    else if (z >= ChunkHeight) z = ChunkHeight - 1;

         if (lighting < 0)  lighting = 0;
    else if (lighting > 15) lighting = 15;

    if (getCellLighting(x, y, z) < lighting)
    {
        setCellLighting(x, y, z, lighting);
        --lighting;

        // Mise à jour des blocs adjacents
        if (lighting > 0)
        {
            if (x > 0 && isCellEmpty(x - 1, y, z))
            {
                updateCellLighting(x - 1, y, z, lighting);
            }
            else if (x <= 0)
            {
                CChunk * chunkW = m_world->getChunk(m_chunkX - 1, m_chunkY);

                // Le chunk est chargé
                if (chunkW != nullptr)
                {
                    if (chunkW->isCellEmpty(ChunkSize - 1, y, z))
                    {
                        chunkW->updateCellLighting(ChunkSize - 1, y, z, lighting);
                    }
                }
            }

            if (x < (ChunkSize - 1) && isCellEmpty(x + 1, y, z))
            {
                updateCellLighting(x + 1, y, z, lighting);
            }
            else
            {
                CChunk * chunkE = m_world->getChunk(m_chunkX + 1, m_chunkY);

                // Le chunk est chargé
                if (chunkE != nullptr)
                {
                    if (chunkE->isCellEmpty(0, y, z))
                    {
                        chunkE->updateCellLighting(0, y, z, lighting);
                    }
                }
            }

            if (y > 0 && isCellEmpty(x, y - 1, z))
            {
                updateCellLighting(x, y - 1, z, lighting);
            }
            else if (y <= 0)
            {
                CChunk * chunkS = m_world->getChunk(m_chunkX, m_chunkY - 1);

                // Le chunk est chargé
                if (chunkS != nullptr)
                {
                    if (chunkS->isCellEmpty(x, ChunkSize - 1, z))
                    {
                        chunkS->updateCellLighting(x, ChunkSize - 1, z, lighting);
                    }
                }
            }

            if (y < (ChunkSize - 1) && isCellEmpty(x, y + 1, z))
            {
                updateCellLighting(x, y + 1, z, lighting);
            }
            else
            {
                CChunk * chunkN = m_world->getChunk(m_chunkX, m_chunkY + 1);

                // Le chunk est chargé
                if (chunkN != nullptr)
                {
                    if (chunkN->isCellEmpty(x, 0, z))
                    {
                        chunkN->updateCellLighting(x, 0, z, lighting);
                    }
                }
            }

            if (z > 0 && isCellEmpty(x, y, z - 1))
            {
                updateCellLighting(x, y, z - 1, lighting);
            }

            if (z < (ChunkHeight - 1) && isCellEmpty(x, y, z + 1))
            {
                updateCellLighting(x, y, z + 1, lighting);
            }
        }
    }
}


/**
 * Modifie la luminosité d'une cellule du chunk.
 *
 * \param x Coordonnée X de la cellule dans le chunk.
 * \param x Coordonnée Y de la cellule dans le chunk.
 * \param z Coordonnée Z de la cellule dans le chunk.
 * \param lighting Niveau de luminosité (entre 0 et 15).
 */

void CChunk::setCellLighting(int x, int y, int z, int lighting)
{
    // Vérification des bornes
         if (z < 0) z = 0;
    else if (z >= ChunkHeight) z = ChunkHeight - 1;

    if (lighting == getCellLighting(x, y, Modulo(z, ChunkSize)))
    {
        //std::cout << "setCellLighting : pas de changement !" << std::endl;
        return;
    }

    m_sectors[z / ChunkSize].setCellLighting(x, y, Modulo(z, ChunkSize), lighting);
    m_needBufferUpdate = true;
}


void CChunk::setCellNeedUpdate(int x, int y, int z, bool needUpdate)
{
    m_cellsUpdate.remove(TCellCoords(x, y, z));

    if (needUpdate)
    {
        CCell * cell = getCell(x, y, z);

        if (cell != nullptr && (cell->content == ContentEmpty ||
                                cell->content == ContentWater ||
                                cell->content == ContentLava))
        {
            m_cellsUpdate.push_back(TCellCoords(x, y, z));
        }
    }
}


void CChunk::setWaterLevel(int x, int y, int z, int level)
{
    T_ASSERT(z >= 0);
    T_ASSERT(z < ChunkHeight);

    m_sectors[z / ChunkSize].setWaterLevel(x, y, Modulo(z, ChunkSize), level);
}


void CChunk::setLavaLevel(int x, int y, int z, int level)
{
    T_ASSERT(z >= 0);
    T_ASSERT(z < ChunkHeight);

    m_sectors[z / ChunkSize].setLavaLevel(x, y, Modulo(z, ChunkSize), level);
}


void CChunk::updateFluidInNeighboors(int x, int y, int z)
{
    T_ASSERT(x >= 0);
    T_ASSERT(x < ChunkSize);
    T_ASSERT(y >= 0);
    T_ASSERT(y < ChunkSize);
    T_ASSERT(z >= 0);
    T_ASSERT(z < ChunkHeight);

    // Ouest
    if (x > 0)
    {
        if (getCellContent(x - 1, y, z) == ContentEmpty ||
            getCellContent(x - 1, y, z) == ContentWater ||
            getCellContent(x - 1, y, z) == ContentLava)
        {
            setCellNeedUpdate(x - 1, y, z);
        }
    }
    else
    {
        CChunk * chunkW = m_world->getChunk(m_chunkX - 1, m_chunkY);

        if (chunkW != nullptr && (chunkW->getCellContent(ChunkSize - 1, y, z) == ContentEmpty ||
                                  chunkW->getCellContent(ChunkSize - 1, y, z) == ContentWater ||
                                  chunkW->getCellContent(ChunkSize - 1, y, z) == ContentLava))
        {
            chunkW->setCellNeedUpdate(ChunkSize - 1, y, z);
        }
    }

    // Est
    if (x < ChunkSize - 1)
    {
        if (getCellContent(x + 1, y, z) == ContentEmpty ||
            getCellContent(x + 1, y, z) == ContentWater ||
            getCellContent(x + 1, y, z) == ContentLava)
        {
            setCellNeedUpdate(x + 1, y, z);
        }
    }
    else
    {
        CChunk * chunkE = m_world->getChunk(m_chunkX + 1, m_chunkY);

        if (chunkE != nullptr && (chunkE->getCellContent(0, y, z) == ContentEmpty ||
                                  chunkE->getCellContent(0, y, z) == ContentWater ||
                                  chunkE->getCellContent(0, y, z) == ContentLava))
        {
            chunkE->setCellNeedUpdate(0, y, z);
        }
    }

    // Sud
    if (y > 0)
    {
        if (getCellContent(x, y - 1, z) == ContentEmpty ||
            getCellContent(x, y - 1, z) == ContentWater ||
            getCellContent(x, y - 1, z) == ContentLava)
        {
            setCellNeedUpdate(x, y - 1, z);
        }
    }
    else
    {
        CChunk * chunkS = m_world->getChunk(m_chunkX, m_chunkY - 1);

        if (chunkS != nullptr && (chunkS->getCellContent(x, ChunkSize - 1, z) == ContentEmpty ||
                                  chunkS->getCellContent(x, ChunkSize - 1, z) == ContentWater ||
                                  chunkS->getCellContent(x, ChunkSize - 1, z) == ContentLava))
        {
            chunkS->setCellNeedUpdate(x, ChunkSize - 1, z);
        }
    }

    // Nord
    if (y < ChunkSize - 1)
    {
        if (getCellContent(x, y + 1, z) == ContentEmpty ||
            getCellContent(x, y + 1, z) == ContentWater ||
            getCellContent(x, y + 1, z) == ContentLava)
        {
            setCellNeedUpdate(x, y + 1, z);
        }
    }
    else
    {
        CChunk * chunkN = m_world->getChunk(m_chunkX, m_chunkY + 1);

        if (chunkN != nullptr && (chunkN->getCellContent(x, 0, z) == ContentEmpty ||
                                  chunkN->getCellContent(x, 0, z) == ContentWater ||
                                  chunkN->getCellContent(x, 0, z) == ContentLava))
        {
            chunkN->setCellNeedUpdate(x, 0, z);
        }
    }

    bool hasBottom = (z == 0 || !isCellEmpty(x, y, z - 1));

    if (!hasBottom && getCellContent(x, y, z - 1) != ContentWaterSource && getCellContent(x, y, z - 1) != ContentLavaSource)
    {
        // En-dessous
        setCellNeedUpdate(x, y, z - 1);
    }
}


/**
 * Retourne la hauteur de fluide d'une cellule.
 *
 * \param cell Cellule.
 */

int CChunk::getFluidLevel(CCell * cell)
{
    if (cell == nullptr)
    {
        return 0;
    }

    if (cell->content == ContentLava)
    {
        return cell->getLavaLevel();
    }
    else if (cell->content == ContentLavaSource)
    {
        return 3;
    }
    else if (cell->content == ContentWater)
    {
        return cell->getWaterLevel();
    }
    else if (cell->content == ContentWaterSource)
    {
        return 7;
    }

    // Normalement on ne devrait pas arriver ici
    return 0;
}
