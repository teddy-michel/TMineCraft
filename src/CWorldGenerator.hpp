/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef T_FILE_CWORLDGENERATOR_HPP
#define T_FILE_CWORLDGENERATOR_HPP

#include "IWorldGenerator.hpp"
#include "CPerlin.hpp"
#include "CWorld.hpp"


class CWorldGenerator : public IWorldGenerator
{
public:

    CWorldGenerator(int seed);
    virtual ~CWorldGenerator();

    virtual CCell getCellContent(int x, int y, int z) const;
    int getElevation(int x, int y) const;
    virtual float getTemperature(int x, int y) const;
    virtual float getHumidity(int x, int y) const;
    virtual TBiome getBiome(int x, int y) const;

private:

    inline float getPerlin(float x, float y) const
    {
        return m_perlinRelief.Get(x, y);
    }

    CPerlin m_perlinRelief;      ///< Générateur aléatoire pour le relief.
    CPerlin m_perlinElevation;   ///< Générateur aléatoire pour l'altitude.
    CPerlin m_perlinTemperature; ///< Générateur aléatoire pour la température.
    CPerlin m_perlinHumidity;    ///< Générateur aléatoire pour l'humidité.
};

#endif // T_FILE_CWORLDGENERATOR_HPP
