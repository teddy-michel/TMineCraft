/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef T_FILE_CGAME_HPP
#define T_FILE_CGAME_HPP

#include "Core/Maths/CVector3.hpp"
#include "CFont.hpp"
#include <SFML/Window.hpp>
#include <map>


/**
 * \enum    TAction
 * \brief   Liste des actions possibles pour un joueur.
 ******************************/

enum TAction
{
    ActionNone,        ///< Aucune action.
    ActionForward,     ///< Déplacement vers l'avant.
    ActionBackward,    ///< Déplacement vers l'arrière.
    ActionStrafeLeft,  ///< Déplacement sur le côté gauche.
    ActionStrafeRight, ///< Déplacement sur le côté droit.
    ActionTurnLeft,    ///< Rotation vers la gauche.
    ActionTurnRight,   ///< Rotation vers la droite.
    ActionJump,        ///< Saut.
    ActionFly,         ///< Vol.
    ActionCrunch,      ///< S'accroupir.
    ActionFire1,       ///< Action principale.
    ActionFire2,       ///< Action secondaire.
    ActionInventory,   ///< Ouverture et fermeture de la fenêtre d'inventaire.
    ActionModeTexture,
    ActionModeLighting,
    ActionModeBiome,
    ActionModeTemperature,
    ActionModeHumidity
};


/**
 * Touche ou bouton associé à une action.
 */

struct TKey
{
    bool isKey; ///< True si on utilise le clavier, false pour la souris.

    union
    {
        sf::Keyboard::Key key;    ///< Touche du clavier.
        sf::Mouse::Button button; ///< Bouton de la souris.
    };

    inline TKey(const TKey& other) : isKey(other.isKey)
    {
        key = other.key;
        button = other.button;
    }

    inline TKey(sf::Keyboard::Key pkey = sf::Keyboard::Unknown) : isKey(true), key(pkey)
    {

    }

    inline TKey(sf::Mouse::Button pbutton) : isKey(false), button(pbutton)
    {

    }

    inline bool operator==(const TKey& other) const
    {
        return (isKey == other.isKey && (isKey ? (key == other.key) : (button == other.button)));
    }
};


enum TDisplayMode
{
    DisplayTexture,
    DisplayLighting,
    DisplayBiome,
    DisplayTemperature,
    DisplayHumidity
};


class CWorld;
class CPlayer;


/**
 * Classe principale de l'application.
 */

class CGame
{
public:

    /**
     * Liste des modes de l'application.
     */

    enum TMode
    {
        ModeMenu,     ///< Affichage du menu.
        ModeGame,     ///< Affichage du jeu.
        ModeInventory ///< Affichage de l'inventaire du joueur.
    };

    CGame();
    ~CGame();

    inline TDisplayMode getDisplayMode() const;
    void setDisplayMode(TDisplayMode mode);

    void createWindow(int width, int height, bool fullScreen);
    int getWindowWidth() const;
    int getWindowHeight() const;
    void mainLoop();
    bool initOpenGL() const;

    TAction getActionForKey(TKey key) const;
    TKey getKeyForAction(TAction action) const;
    bool isActionActive(TAction action) const;
    void setKeyForAction(TAction action, TKey key);

    inline CWorld * getWorld() const { return m_world; }
    inline CPlayer * getPlayer() const { return m_player; }
    inline TMode getMode() const { return m_mode; }
    inline const CFont * getFont() const { return &m_font; }

protected:

    void drawRepere(float length) const;
    void begin2D() const;
    void end2D() const;
    void displayMenu() const;

private:

    void clickOnMenu(int x, int y, sf::Mouse::Button button);

    sf::Window m_window;
    TDisplayMode m_displayMode;
    TMode m_mode;
    bool m_verticalSynchro;
    bool m_displayAllChunks;
    CWorld * m_world;
    CPlayer * m_player;
    std::map<TAction, TKey> m_keyActions; ///< Boutons associés aux actions.
    CFont m_font;
};


inline TDisplayMode CGame::getDisplayMode() const
{
    return m_displayMode;
}


void _gluLookAt(const Ted::TVector3F& position, const Ted::TVector3F& view);
void _gluPerspective(double fovy, double aspect, double znear, double zfar);

#endif // T_FILE_CGAME_HPP
