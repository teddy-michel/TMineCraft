/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CWorld.hpp"
#include "CCell.hpp"
#include "os.h"

#include <limits>
#include <iostream>
#include <fstream>
#include <cstring>
#include <algorithm>
#include <GL/glew.h>

#include "Core/Maths/CMatrix4.hpp"
#include "Core/Maths/MathsUtils.hpp"
#include "Graphic/CImage.hpp"
#include "CChunk.hpp"
#include "CPlayer.hpp"
//#include "CWorldGenerator.hpp"
//#include "CWorldGenerator2.hpp"
#include "CWorldGenerator3.hpp"
//#include "CCaveGenerator.hpp"


using namespace Ted;


/**
 * Initialise le monde.
 */

CWorld::CWorld(int seed, CGame * game) :
m_game              (game),
m_generator         (new CWorldGenerator3(seed)),
m_loadThread        (loadChunksAsynchronous, this),
m_isRunning         (true),
m_name              ("Test"),
m_displayAllChunks  (false),
m_numVisibleChunks  (2),
m_spawn             (TVector3I(0, 0, 65)),
m_currentChunkX     (0),
m_currentChunkY     (0),
m_time              (12 * 60 * 60 * 1000) // Midi au départ
{
    std::cout << "World initialization with seed " << seed << std::endl;
    loadTextures();

    // Création d'un thread pour charger les chunks
    m_loadThread.launch();
}


/**
 * Libère la mémoire occupée par le monde.
 */

CWorld::~CWorld()
{
    m_isRunning = false;
    m_loadThread.wait();
    resetWorld();
}


int CWorld::getSeed() const
{
    return m_generator->getSeed();
}


/**
 * Retourne une cellule du monde.
 *
 * \param x Coordonnée X de la cellule dans le monde.
 * \param y Coordonnée Y de la cellule dans le monde.
 * \param z Coordonnée Z de la cellule dans le monde.
 * \return Pointeur sur la cellule, ou nullptr si la cellule n'est pas chargée.
 */

CCell * CWorld::getCell(int x, int y, int z) const
{
    int chunkX = floor((double) x / ChunkSize);
    int chunkY = floor((double) y / ChunkSize);

    CChunk * chunk = getChunk(chunkX, chunkY);

    if (chunk == nullptr)
    {
        return nullptr;
    }

         if (z < 0) z = 0;
    else if (z >= ChunkHeight) z = ChunkHeight - 1;

    return chunk->getCell(x - chunkX * ChunkSize, y - chunkY * ChunkSize, z);
}


/**
 * Retourne le contenu d'une cellule.
 * Si la cellule appartient à un chunk non chargé, ContentEmpty est retourné.
 *
 * \param x Coordonnée X de la cellule dans le monde.
 * \param y Coordonnée Y de la cellule dans le monde.
 * \param z Coordonnée Z de la cellule dans le monde.
 * \return Contenu de la cellule.
 */

TCellContent CWorld::getCellContent(int x, int y, int z) const
{
    int chunkX = floor((double) x / ChunkSize);
    int chunkY = floor((double) y / ChunkSize);

    CChunk * chunk = getChunk(chunkX, chunkY);

    if (chunk == nullptr)
    {
        return ContentEmpty; // ou ContentUnknown ?
    }

         if (z < 0) z = 0;
    else if (z >= ChunkHeight) z = ChunkHeight - 1;

    return chunk->getCellContent(x - chunkX * ChunkSize, y - chunkY * ChunkSize, z);
}


/**
 * Indique si une cellule est vide.
 *
 * \param x Coordonnée X de la cellule dans le monde.
 * \param y Coordonnée Y de la cellule dans le monde.
 * \param z Coordonnée Z de la cellule dans le monde.
 * \param waterIsEmpty Indique si on considère les cellules contenant de l'eau comme étant vides.
 * \return True si la cellule est vide ou si elle appartient à un chunk non chargé, false sinon.
 */

bool CWorld::isCellEmpty(int x, int y, int z, bool waterIsEmpty) const
{
    int chunkX = floor((double) x / ChunkSize);
    int chunkY = floor((double) y / ChunkSize);

    CChunk * chunk = getChunk(chunkX, chunkY);

    if (chunk == nullptr)
    {
        return true;
    }

         if (z < 0) z = 0;
    else if (z >= ChunkHeight) z = ChunkHeight - 1;

    return chunk->isCellEmpty(x - chunkX * ChunkSize, y - chunkY * ChunkSize, z, waterIsEmpty);
}


/**
 * Indique si un point se trouve dans l'eau.
 *
 * \param position Position du point à tester dans le monde.
 * \return True si le point est dans l'eau, false sinon.
 */

bool CWorld::isUnderWater(const TVector3F& position) const
{
    int xCell0 = floor(position.X / CellSize);
    int yCell0 = floor(position.Y / CellSize);
    int zCell0 = floor(position.Z / CellSize);

    bool isWater = (getCellContent(xCell0, yCell0, zCell0) == ContentWater ||
                    getCellContent(xCell0, yCell0, zCell0) == ContentWaterSource);

    if (!isWater)
    {
        return false;
    }

    bool isWaterAbove = (zCell0 < (ChunkHeight - 1) && (getCellContent(xCell0, yCell0, zCell0 + 1) == ContentWater ||
                                                        getCellContent(xCell0, yCell0, zCell0 + 1) == ContentWaterSource));

    // À la surface, l'eau n'occupe pas un bloc complet
    if (!isWaterAbove)
    {
        CCell * cell = getCell(xCell0, yCell0, zCell0);
        T_ASSERT(cell != nullptr);

        float waterLevel = FluidTopHeight;
        if (getCellContent(xCell0, yCell0, zCell0) == ContentWater)
        {
             waterLevel *= (cell->getWaterLevel() + 1) / 8.0f;
        }

        return ((position.Z / CellSize) - zCell0 <= waterLevel);
    }

    return true;
}


/**
 * Indique si un point se trouve dans la lave.
 *
 * \param position Position du point à tester dans le monde.
 * \return True si le point est dans la lave, false sinon.
 */

bool CWorld::isUnderLava(const TVector3F& position) const
{
    int xCell0 = floor(position.X / CellSize);
    int yCell0 = floor(position.Y / CellSize);
    int zCell0 = floor(position.Z / CellSize);

    bool isLava = (getCellContent(xCell0, yCell0, zCell0) == ContentLava ||
                   getCellContent(xCell0, yCell0, zCell0) == ContentLavaSource);

    if (!isLava)
    {
        return false;
    }

    bool isLavaAbove = (zCell0 < (ChunkHeight - 1) && (getCellContent(xCell0, yCell0, zCell0 + 1) == ContentLava ||
                                                        getCellContent(xCell0, yCell0, zCell0 + 1) == ContentLavaSource));

    // À la surface, la lave n'occupe pas un bloc complet
    if (!isLavaAbove)
    {
        CCell * cell = getCell(xCell0, yCell0, zCell0);
        T_ASSERT(cell != nullptr);

        float level = FluidTopHeight;
        if (getCellContent(xCell0, yCell0, zCell0) == ContentLava)
        {
             level *= (cell->getLavaLevel() + 1) / 4.0f;
        }

        return ((position.Z / CellSize) - zCell0 <= level);
    }

    return true;
}


int CWorld::getHeight(int x, int y) const
{
    int chunkX = floor((double) x / ChunkSize);
    int chunkY = floor((double) y / ChunkSize);

    CChunk * chunk = getChunk(chunkX, chunkY);

    if (chunk == nullptr)
    {
        return 0;
    }

    return chunk->getHeight(x - chunkX * ChunkSize, y - chunkY * ChunkSize);
}


/**
 * Recherche l'intersection entre un segment et le monde.
 *
 * \param origin    Point d'origine du segment.
 * \param direction Vecteur de déplacement.
 * \param iPoint    Contiendra le point d'intersection.
 * \param iFace     Contiendra la face intersectée.
 * \param iCube     Contiendra les coordonnées du cube intersecté.
 * \param maxLength Longueur du segment.
 * \return True si le segment intersecte un bloc solide, false sinon.
 */

bool CWorld::findIntersection(const TVector3F& origin, const TVector3F& direction, TVector3F& iPoint, TFace& iFace, TVector3I& iCube, double maxLength)
{
    const double produitScalaire1 = VectorDot(TVector3F(1.0f, 0.0f, 0.0f), direction);
    const double produitScalaire2 = VectorDot(TVector3F(0.0f, 1.0f, 0.0f), direction);
    const double produitScalaire3 = VectorDot(TVector3F(0.0f, 0.0f, 1.0f), direction);

    // Cube courant
    TVector3I cube(floor(origin.X / CellSize),
                   floor(origin.Y / CellSize),
                   floor(origin.Z / CellSize));

    // Intersection
    bool intersection = false;

    while (1)
    {
        // Vers l'ouest
        if (produitScalaire1 < 0.0f)
        {
            double t = (cube.X * CellSize - origin.X) / direction.X;
            double y = (direction.Y * t + origin.Y) / CellSize;
            double z = (direction.Z * t + origin.Z) / CellSize;

            // Intersection avec la face ouest
            if ((int) floor(y) == cube.Y && (int) floor(z) == cube.Z)
            {
                iCube = TVector3I(cube.X - 1, cube.Y, cube.Z);
                iFace = FaceEast;
                iPoint = TVector3F(cube.X * CellSize, y * CellSize, z * CellSize);

                // Cube vide, déplacement
                if (isCellEmpty(iCube.X, iCube.Y, iCube.Z))
                {
                    // Trop loin
                    TVector3F segment = iPoint - origin;
                    if (segment.norm() >= maxLength)
                    {
                        break;
                    }

                    cube = iCube;
                    continue;
                }
                else
                {
                    intersection = true;
                    break;
                }
            }
        }
        // Vers l'est
        else
        {
            double t = ((cube.X + 1) * CellSize - origin.X) / direction.X;
            double y = (direction.Y * t + origin.Y) / CellSize;
            double z = (direction.Z * t + origin.Z) / CellSize;

            // Intersection avec la face est
            if ((int) floor(y) == cube.Y && (int) floor(z) == cube.Z)
            {
                iCube = TVector3I(cube.X + 1, cube.Y, cube.Z);
                iFace = FaceWest;
                iPoint = TVector3F((cube.X + 1) * CellSize, y * CellSize, z * CellSize);

                // Cube vide, déplacement
                if (isCellEmpty(iCube.X, iCube.Y, iCube.Z))
                {
                    // Trop loin
                    TVector3F segment = iPoint - origin;
                    if (segment.norm() >= maxLength)
                    {
                        break;
                    }

                    cube = iCube;
                    continue;
                }
                else
                {
                    intersection = true;
                    break;
                }
            }
        }

        // Vers le sud
        if (produitScalaire2 < 0.0f)
        {
            double t = (cube.Y * CellSize - origin.Y) / direction.Y;
            double x = (direction.X * t + origin.X) / CellSize;
            double z = (direction.Z * t + origin.Z) / CellSize;

            // Intersection avec la face sud
            if ((int) floor(x) == cube.X && (int) floor(z) == cube.Z)
            {
                iCube = TVector3I(cube.X, cube.Y - 1, cube.Z);
                iFace = FaceNorth;
                iPoint = TVector3F(x * CellSize, cube.Y * CellSize, z * CellSize);

                // Cube vide, déplacement
                if (isCellEmpty(iCube.X, iCube.Y, iCube.Z))
                {
                    // Trop loin
                    TVector3F segment = iPoint - origin;
                    if (segment.norm() >= maxLength)
                    {
                        break;
                    }

                    cube = iCube;
                    continue;
                }
                else
                {
                    intersection = true;
                    break;
                }
            }
        }
        // Vers le nord
        else
        {
            double t = ((cube.Y + 1) * CellSize - origin.Y) / direction.Y;
            double x = (direction.X * t + origin.X) / CellSize;
            double z = (direction.Z * t + origin.Z) / CellSize;

            // Intersection avec la face nord
            if ((int) floor(x) == cube.X && (int) floor(z) == cube.Z)
            {
                iCube = TVector3I(cube.X, cube.Y + 1, cube.Z);
                iFace = FaceSouth;
                iPoint = TVector3F(x * CellSize, (cube.Y + 1) * CellSize, z * CellSize);

                // Cube vide, déplacement
                if (isCellEmpty(iCube.X, iCube.Y, iCube.Z))
                {
                    // Trop loin
                    TVector3F segment = iPoint - origin;
                    if (segment.norm() >= maxLength)
                    {
                        break;
                    }

                    cube = iCube;
                    continue;
                }
                else
                {
                    intersection = true;
                    break;
                }
            }
        }

        // Vers le bas
        if (produitScalaire3 < 0.0f)
        {
            if (cube.Z == 0)
            {
                iPoint = Origin3F;
                iFace = FaceUnknown;

                return false;
            }

            double t = (cube.Z * CellSize - origin.Z) / direction.Z;
            double x = (direction.X * t + origin.X) / CellSize;
            double y = (direction.Y * t + origin.Y) / CellSize;

            // Intersection avec la face inférieure
            if ((int) floor(x) == cube.X && (int) floor(y) == cube.Y)
            {
                iCube = TVector3I(cube.X, cube.Y, cube.Z - 1);
                iFace = FaceTop;
                iPoint = TVector3F(x * CellSize, y * CellSize, cube.Z * CellSize);

                // Cube vide, déplacement
                if (isCellEmpty(iCube.X, iCube.Y, iCube.Z))
                {
                    // Trop loin
                    TVector3F segment = iPoint - origin;
                    if (segment.norm() >= maxLength)
                    {
                        break;
                    }

                    cube = iCube;
                    continue;
                }
                else
                {
                    intersection = true;
                    break;
                }
            }
        }
        // Vers le haut
        else
        {
            if (cube.Z == ChunkHeight - 1)
            {
                iPoint = Origin3F;
                iFace = FaceUnknown;

                return false;
            }

            double t = ((cube.Z + 1) * CellSize - origin.Z) / direction.Z;
            double x = (direction.X * t + origin.X) / CellSize;
            double y = (direction.Y * t + origin.Y) / CellSize;

            // Intersection avec la face supérieure
            if ((int) floor(x) == cube.X && (int) floor(y) == cube.Y)
            {
                iCube = TVector3I(cube.X, cube.Y, cube.Z + 1);
                iFace = FaceBottom;
                iPoint = TVector3F(x * CellSize, y * CellSize, (cube.Z + 1) * CellSize);

                // Cube vide, déplacement
                if (isCellEmpty(iCube.X, iCube.Y, iCube.Z))
                {
                    // Trop loin
                    TVector3F segment = iPoint - origin;
                    if (segment.norm() >= maxLength)
                    {
                        break;
                    }

                    cube = iCube;
                    continue;
                }
                else
                {
                    intersection = true;
                    break;
                }
            }
        }

        // Problème...
        std::cerr << "  Intersection segment : aucune face intersectee" << std::endl;
        break;
    }

    if (intersection)
    {
        // Trop loin
        TVector3F segment = iPoint - origin;
        if (segment.norm() >= maxLength)
        {
            iPoint = Origin3F;
            iFace = FaceUnknown;

            return false;
        }

        return true;
    }

    iPoint = Origin3F;
    iFace = FaceUnknown;

    return false;
}


/**
 * Cherche la hauteur de la première cellule non vide dans une colonne.
 *
 * \param x Coordonnée X de la colonne dans le monde.
 * \param y Coordonnée Y de la colonne dans le monde.
 * \param z Coordonnée Z de la cellule de départ.
 * \return Coordonnée Z de la première cellule non vide, ou -1 si le chunk n'est pas chargé.
 */

int CWorld::findFirstZ(int x, int y, int z) const
{
    if (z < 1)
    {
        return -1;
    }

    if (z > ChunkHeight)
    {
        z = ChunkHeight;
    }

    int chunkX = floor((double) x / ChunkSize);
    int chunkY = floor((double) y / ChunkSize);

    CChunk * chunk = getChunk(chunkX, chunkY);

    if (chunk == nullptr)
    {
        return -1;
    }

    for (int zt = z - 1; zt >= 0; --zt)
    {
        if (!chunk->isCellEmpty(Modulo(x, ChunkSize), Modulo(y, ChunkSize), zt))
        {
            return zt;
        }
    }

    return -1;
}


/**
 * Enlève le contenu d'une cellule.
 *
 * \param x Coordonnée X de la cellule dans le monde.
 * \param y Coordonnée Y de la cellule dans le monde.
 * \param z Coordonnée Z de la cellule dans le monde.
 * \return Contenu de la cellule.
 */

TCellContent CWorld::removeCell(int x, int y, int z)
{
    int chunkX = floor((float) x / ChunkSize);
    int chunkY = floor((float) y / ChunkSize);

    CChunk * chunk = getChunk(chunkX, chunkY);

    if (chunk == nullptr)
    {
        return ContentEmpty;
    }

         if (z < 0) z = 0;
    else if (z >= ChunkHeight) z = ChunkHeight - 1;

    return chunk->removeCell(x - chunkX * ChunkSize, y - chunkY * ChunkSize, z);
}


/**
 * Ajoute un bloc.
 *
 * \param x Coordonnée X de la cellule dans le monde.
 * \param y Coordonnée Y de la cellule dans le monde.
 * \param z Coordonnée Z de la cellule dans le monde.
 * \param content Contenu de la cellule.
 */

bool CWorld::addCell(int x, int y, int z, TCellContent content, int data)
{
    int chunkX = floor((float) x / ChunkSize);
    int chunkY = floor((float) y / ChunkSize);

    CChunk * chunk = getChunk(chunkX, chunkY);

    if (chunk == nullptr)
    {
        return false;
    }

         if (z < 0) z = 0;
    else if (z >= ChunkHeight) z = ChunkHeight - 1;

    return chunk->addCell(x - chunkX * ChunkSize, y - chunkY * ChunkSize, z, content, data);
}


/**
 * Charge le monde depuis un fichier.
 * Le monde actuel est détruit.
 *
 * \todo Implémentation.
 *
 * \param folder Nom du dossier contenant le monde à charger.
 * \return True si le chargement s'est bien passé.
 */

bool CWorld::loadWorld(const Ted::CString& folder)
{
    std::cout << "Chargement du monde depuis le dossier " << folder << std::endl;
    CString fileName = folder + "/world.dat";

    // Ouverture du fichier
    std::ifstream file(fileName.toCharArray(), std::ios::in | std::ios::binary);

    if (!file)
    {
        std::cerr << "CWorld::loadWorld : impossible de lire le fichier " << fileName << std::endl;
        return false;
    }

    file.seekg(0, std::ios_base::end);
    std::size_t size = file.tellg();

    if (size < sizeof(THeader))
    {
        file.close();
        std::cerr << "CWorld::loadWorld : le fichier " << fileName << " a une taille trop petite" << std::endl;
        return nullptr;
    }

    file.seekg(0, std::ios_base::beg);

    THeader header;
    file.read(reinterpret_cast<char *>(&header), sizeof(THeader));

    if (header.ident != ('T' + ('M'<<8) + ('C'<<16) + ('W'<<24)) || header.version != 1)
    {
        std::cerr << "CWorld::loadWorld : le fichier " << fileName << " a un format incorrect" << std::endl;
        return false;
    }

    //...

    file.close();

    return true;
}

#ifdef T_SYSTEM_WINDOWS
#include <windows.h>
#endif

/**
 * Enregistre le monde dans un dossier.
 *
 * \param folder Nom du dossier à utiliser.
 * \return True si la sauvegarde s'est bien passée.
 */

bool CWorld::saveWorld(const Ted::CString& folder) const
{
    std::cout << "Enregistrement du monde dans le dossier " << folder << std::endl;
    CString subFolder = folder + "/" + m_name;
    CString folderPlayers = subFolder + "/players";

    // Création des répertoires
#ifdef T_SYSTEM_WINDOWS
    CreateDirectory(folder.toCharArray(), NULL);
    CreateDirectory(subFolder.toCharArray(), NULL);
    CreateDirectory(folderPlayers.toCharArray(), NULL);
#else
#   error This operating system is not yet supported
#endif

    CString fileName = subFolder + "/world.dat";

    std::ofstream file(fileName.toCharArray(), std::ios::out | std::ios::binary);

    if (!file)
    {
        std::cerr << "CWorld::saveWorld : impossible d'ouvrir le fichier " << fileName << std::endl;
        return false;
    }

    std::list<TChunk> chunks;
    std::list<TSector> sectors;

    for (std::map<int, std::map<int, CChunk *> >::const_iterator it = m_chunks.begin(); it != m_chunks.end(); ++it)
    {
        for (std::map<int, CChunk *>::const_iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2)
        {
            TChunk chunk;
            chunk.x             = it->first;
            chunk.y             = it2->first;
            chunk.sectors       = 0;
            chunk.offsetSectors = 0;

            const uint8_t * heightMap = it2->second->getHeightMap();

            for (int x = 0; x < ChunkSize; ++x)
            {
                for (int y = 0; y < ChunkSize; ++y)
                {
                    chunk.heightMap[x + y * ChunkSize] = heightMap[x + y * ChunkSize];
                }
            }

            chunks.push_back(chunk);

            for (int s = 0; s < SectorsInChunk; ++s)
            {
                const CSector * sector = it2->second->getSector(s);

                if (!sector->isEmpty())
                {
                    chunk.sectors |= (1 << s);

                    CCell * cells = sector->getCells();

                    TSector sectorFile;
                    strncpy(reinterpret_cast<char *>(&sectorFile), reinterpret_cast<const char *>(cells), ChunkSize * ChunkSize * ChunkSize * sizeof(int16_t));
                    sectors.push_back(sectorFile);
                }
            }
        }
    }

    THeader header;
    header.ident        = ('T' + ('M'<<8) + ('C'<<16) + ('W'<<24));
    header.version      = 1;
    strncpy(header.name, m_name.toCharArray(), 64);
    header.seed         = m_generator->getSeed();
    header.spawn        = m_spawn;
    header.time         = m_time;
    header.numChunks    = chunks.size();
    header.offsetChunks = sizeof(THeader);

    file.write(reinterpret_cast<const char *>(&header), sizeof(THeader));

    if (file.fail())
    {
        std::cerr << "CWorld::saveToFile : erreur inconnue" << std::endl;
        file.close();
        return false;
    }

    uint32_t offsetSectors = sizeof(THeader) + chunks.size() * sizeof(TChunk);

    // Liste des chunks
    for (std::list<TChunk>::iterator it = chunks.begin(); it != chunks.end(); ++it)
    {
        it->offsetSectors = offsetSectors;

        int numEmptySectors = 16;
        if (it->sectors & 0b1000000000000000) --numEmptySectors;
        if (it->sectors & 0b0100000000000000) --numEmptySectors;
        if (it->sectors & 0b0010000000000000) --numEmptySectors;
        if (it->sectors & 0b0001000000000000) --numEmptySectors;
        if (it->sectors & 0b0000100000000000) --numEmptySectors;
        if (it->sectors & 0b0000010000000000) --numEmptySectors;
        if (it->sectors & 0b0000001000000000) --numEmptySectors;
        if (it->sectors & 0b0000000100000000) --numEmptySectors;
        if (it->sectors & 0b0000000010000000) --numEmptySectors;
        if (it->sectors & 0b0000000001000000) --numEmptySectors;
        if (it->sectors & 0b0000000000100000) --numEmptySectors;
        if (it->sectors & 0b0000000000010000) --numEmptySectors;
        if (it->sectors & 0b0000000000001000) --numEmptySectors;
        if (it->sectors & 0b0000000000000100) --numEmptySectors;
        if (it->sectors & 0b0000000000000010) --numEmptySectors;
        if (it->sectors & 0b0000000000000001) --numEmptySectors;

        file.write(reinterpret_cast<const char *>(&*it), sizeof(TChunk));

        if (file.fail())
        {
            std::cerr << "CWorld::saveToFile : erreur inconnue" << std::endl;
            file.close();
            return false;
        }

        offsetSectors += (16 - numEmptySectors) * sizeof(TSector);
    }

    // Liste des secteurs
    for (std::list<TSector>::const_iterator it = sectors.begin(); it != sectors.end(); ++it)
    {
        file.write(reinterpret_cast<const char *>(&*it), sizeof(TSector));

        if (file.fail())
        {
            std::cerr << "CWorld::saveToFile : erreur inconnue" << std::endl;
            file.close();
            return false;
        }
    }

    file.close();

    // Sauvegarde des informations des joueurs
    savePlayers(folderPlayers);

    return true;
}


/**
 * Sauvegarde les informations des joueurs présents dans le monde.
 */

void CWorld::savePlayers(const Ted::CString& folder) const
{
    for (std::list<CPlayer *>::const_iterator it = m_players.begin(); it != m_players.end(); ++it)
    {
        (*it)->savePlayer(folder);
    }
}


/**
 * Ajoute un joueur au monde.
 *
 * \todo Récupérer les données du joueur dans le dossier de sauvegarde.
 *
 * \param Name Nom du joueur.
 * \return Pointeur sur le joueur crée.
 */

CPlayer * CWorld::addPlayer(const CString& name)
{
    // Recherche d'un joueur portant ce nom
    for (std::list<CPlayer *>::const_iterator it = m_players.begin(); it != m_players.end(); ++it)
    {
        if ((*it)->getName() == name)
        {
            return *it;
        }
    }

    // Création du joueur
    CPlayer * player = new CPlayer(m_game);
    player->setName(name);

    m_spawn.Z = findFirstZ(m_spawn.X, m_spawn.Y, ChunkHeight - 1) + 1;

    player->setPosition(TVector3F((m_spawn.X + 0.5f) * CellSize, (m_spawn.Y + 0.5f) * CellSize, m_spawn.Z * CellSize));
    player->setDirection(TVector3F((m_spawn.X + 0.5f) * CellSize, (m_spawn.Y + 1.5f) * CellSize, m_spawn.Z * CellSize));

    m_players.push_back(player);
    return player;
}


/**
 * Réinitialise le monde.
 */

void CWorld::resetWorld()
{
    for (std::map<int, std::map<int, CChunk *> >::iterator it = m_chunks.begin(); it != m_chunks.end(); ++it)
    {
        for (std::map<int, CChunk *>::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2)
        {
            delete it2->second;
        }
    }

    m_chunks.clear();
}


/**
 * Construit les chunks visibles.
 */

void CWorld::build()
{
    std::cout << "Building world..." << std::endl;

    for (int chunkX = m_currentChunkX - m_numVisibleChunks; chunkX <= m_currentChunkX + m_numVisibleChunks; ++chunkX)
    {
        for (int chunkY = m_currentChunkY - m_numVisibleChunks; chunkY <= m_currentChunkY + m_numVisibleChunks; ++chunkY)
        {
            CChunk * chunk = new CChunk(chunkX, chunkY, this);
            m_chunks[chunkX][chunkY] = chunk;

            chunk->build();
        }
    }

    // Calcul de la luminosité
    for (int chunkX = m_currentChunkX - m_numVisibleChunks; chunkX <= m_currentChunkX + m_numVisibleChunks; ++chunkX)
    {
        for (int chunkY = m_currentChunkY - m_numVisibleChunks; chunkY <= m_currentChunkY + m_numVisibleChunks; ++chunkY)
        {
            m_chunks[chunkX][chunkY]->computeLighting();
        }
    }

    // Remplissage des buffers
    for (int chunkX = m_currentChunkX - m_numVisibleChunks; chunkX <= m_currentChunkX + m_numVisibleChunks; ++chunkX)
    {
        for (int chunkY = m_currentChunkY - m_numVisibleChunks; chunkY <= m_currentChunkY + m_numVisibleChunks; ++chunkY)
        {
            m_chunks[chunkX][chunkY]->updateBuffer();
        }
    }
}


void CWorld::updateBuffers()
{
    for (std::map<int, std::map<int, CChunk *> >::iterator it = m_chunks.begin(); it != m_chunks.end(); ++it)
    {
        for (std::map<int, CChunk *>::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2)
        {
            it2->second->requestUpdateBuffer();
        }
    }
}


/**
 * Affiche la partie visible du monde.
 */

void CWorld::display()
{
    int numUpdateBuffer = 1;

    if (m_displayAllChunks)
    {
        for (std::map<int, std::map<int, CChunk *> >::iterator it = m_chunks.begin(); it != m_chunks.end(); ++it)
        {
            for (std::map<int, CChunk *>::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2)
            {
                if (numUpdateBuffer > 0 && it2->second->needUpdateBuffer())
                {
                    it2->second->updateBuffer();
                    --numUpdateBuffer;
                }

                it2->second->display();
            }
        }
    }
    else
    {
        for (int chunkX = m_currentChunkX - m_numVisibleChunks; chunkX <= m_currentChunkX + m_numVisibleChunks; ++chunkX)
        {
            for (int chunkY = m_currentChunkY - m_numVisibleChunks; chunkY <= m_currentChunkY + m_numVisibleChunks; ++chunkY)
            {
                if (isChunkLoaded(chunkX, chunkY))
                {
                    if (numUpdateBuffer > 0 && m_chunks[chunkX][chunkY]->needUpdateBuffer())
                    {
                        m_chunks[chunkX][chunkY]->updateBuffer();
                        --numUpdateBuffer;
                    }

                    m_chunks[chunkX][chunkY]->display();
                }
            }
        }
    }
}


/**
 * Affiche les surfaces visibles et transparentes du monde.
 */

void CWorld::displayBlend()
{
    if (m_displayAllChunks)
    {
        for (std::map<int, std::map<int, CChunk *> >::iterator it = m_chunks.begin(); it != m_chunks.end(); ++it)
        {
            for (std::map<int, CChunk *>::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2)
            {
                it2->second->displayBlend();
            }
        }
    }
    else
    {
        for (int chunkX = m_currentChunkX - m_numVisibleChunks; chunkX <= m_currentChunkX + m_numVisibleChunks; ++chunkX)
        {
            for (int chunkY = m_currentChunkY - m_numVisibleChunks; chunkY <= m_currentChunkY + m_numVisibleChunks; ++chunkY)
            {
                m_chunks[chunkX][chunkY]->displayBlend();
            }
        }
    }
}


/**
 * Mise à jour du monde à chaque frame.
 *
 * \param frameTime Durée de la frame en millisecondes.
 */

void CWorld::update(int frameTime)
{
    m_time += frameTime * 72;

    if (m_time > 28 * 24 * 60 * 60 * 1000U)
    {
        m_time -= 28 * 24 * 60 * 60 * 1000U;
    }

    // Le monde est mis à jour à intervalle régulier
    static int nextUpdate = 0;

    nextUpdate -= frameTime;

    if (nextUpdate > 0)
    {
        return;
    }

    nextUpdate = UpdatePeriod;


    if (m_displayAllChunks)
    {
        for (std::map<int, std::map<int, CChunk *> >::iterator it = m_chunks.begin(); it != m_chunks.end(); ++it)
        {
            for (std::map<int, CChunk *>::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2)
            {
                it2->second->updateChunk(frameTime);
            }
        }
    }
    else
    {
        for (int chunkX = m_currentChunkX - m_numVisibleChunks; chunkX <= m_currentChunkX + m_numVisibleChunks; ++chunkX)
        {
            for (int chunkY = m_currentChunkY - m_numVisibleChunks; chunkY <= m_currentChunkY + m_numVisibleChunks; ++chunkY)
            {
                m_chunks[chunkX][chunkY]->updateChunk(frameTime);
            }
        }
    }
}


/**
 * Charge un chunk si celui-ci n'est pas déjà chargé.
 *
 * \param x Coordonnée X du chunk.
 * \param y Coordonnée Y du chunk.
 * \return Pointeur sur le chunk.
 */

CChunk * CWorld::loadChunk(int x, int y)
{
    CChunk * chunk = m_chunks[x][y];

    if (chunk == nullptr)
    {
        chunk = new CChunk(x, y, this);
        m_chunks[x][y] = chunk;
    }
    else if (chunk->isBuild())
    {
        return chunk;
    }

    chunk->build();
    chunk->computeLighting();
    chunk->updateBuffer();

    // Mise à jour graphique des chunks adjacents
    CChunk * chunkW = getChunk(x - 1, y);
    if (chunkW != nullptr)
    {
        chunkW->computeLighting();
        chunkW->updateBuffer();
    }

    CChunk * chunkE = getChunk(x + 1, y);
    if (chunkE != nullptr)
    {
        chunkE->computeLighting();
        chunkE->updateBuffer();
    }

    CChunk * chunkS = getChunk(x, y - 1);
    if (chunkS != nullptr)
    {
        chunkS->computeLighting();
        chunkS->updateBuffer();
    }

    CChunk * chunkN = getChunk(x, y + 1);
    if (chunkN != nullptr)
    {
        chunkN->computeLighting();
        chunkN->updateBuffer();
    }

    return chunk;
}


CChunk * CWorld::getChunk(int x, int y) const
{
    std::map<int, std::map<int, CChunk *> >::const_iterator it = m_chunks.find(x);

    if (it == m_chunks.end())
    {
        return nullptr;
    }
    else
    {
        std::map<int, CChunk *>::const_iterator it2 = it->second.find(y);

        if (it2 == it->second.end())
        {
            return nullptr;
        }
        else
        {
            return it2->second;
        }
    }
}


CChunk * CWorld::getChunk(int x, int y, bool load)
{
    std::map<int, std::map<int, CChunk *> >::const_iterator it = m_chunks.find(x);

    if (it == m_chunks.end())
    {
        if (load)
        {
            return loadChunk(x, y);
        }
        else
        {
            return nullptr;
        }
    }
    else
    {
        std::map<int, CChunk *>::const_iterator it2 = it->second.find(y);

        if (it2 == it->second.end())
        {
            if (load)
            {
                return loadChunk(x, y);
            }
            else
            {
                return nullptr;
            }
        }
        else
        {
            return it2->second;
        }
    }
}


/**
 * Indique si un chunk est chargé en mémoire.
 *
 * \param x Coordonnée X du chunk.
 * \param y Coordonnée Y du chunk.
 * \return True si le chunk est chargé, false sinon.
 */

bool CWorld::isChunkLoaded(int x, int y) const
{
    std::map<int, std::map<int, CChunk *> >::const_iterator it = m_chunks.find(x);

    if (it == m_chunks.end())
    {
        return false;
    }

    std::map<int, CChunk *>::const_iterator it2 = it->second.find(y);

    if (it2 == it->second.end())
    {
        return false;
    }

    return true;
}


void CWorld::setCurrentChunk(int chunkX, int chunkY)
{
    if (m_currentChunkX != chunkX || m_currentChunkY != chunkY)
    {
        std::cout << "Changement de chunk : " << chunkX << " x " << chunkY << std::endl;

        m_currentChunkX = chunkX;
        m_currentChunkY = chunkY;

        for (int x = m_currentChunkX - m_numVisibleChunks; x <= m_currentChunkX + m_numVisibleChunks; ++x)
        {
            for (int y = m_currentChunkY - m_numVisibleChunks; y <= m_currentChunkY + m_numVisibleChunks; ++y)
            {
                //loadChunk(x, y);

                if (!isChunkLoaded(x, y))
                {
                    m_chunks[x][y] = new CChunk(x, y, this);
                }

                if (!m_chunks[x][y]->isBuild())
                {
                    m_loadMutex.lock();
                    m_chunksUpdate.push(m_chunks[x][y]);
                    std::cout << " ajout du chunk to load: " << x << " x " << y << std::endl;
                    m_loadMutex.unlock();
                }

            }
        }
    }
}


/**
 * Chargement des textures.
 */

void CWorld::loadTextures()
{
    m_texturesId[TextureNone]        = 0;
    m_texturesId[TextureRock]        = Game::textureManager->loadTexture("textures/stone.png",        CTextureManager::FilterNone);
    m_texturesId[TextureDirt]        = Game::textureManager->loadTexture("textures/dirt.png",         CTextureManager::FilterNone);
    m_texturesId[TextureGems]        = Game::textureManager->loadTexture("textures/redstone_ore.png", CTextureManager::FilterNone);
    m_texturesId[TextureGrass]       = Game::textureManager->loadTexture("textures/grass.png",        CTextureManager::FilterNone);
    m_texturesId[TextureGrassSide]   = Game::textureManager->loadTexture("textures/grass_side.png",   CTextureManager::FilterNone);
    m_texturesId[TextureWater]       = Game::textureManager->loadTexture("textures/water.png",        CTextureManager::FilterNone);
    m_texturesId[TextureLava]        = Game::textureManager->loadTexture("textures/lava.png",         CTextureManager::FilterNone);
    m_texturesId[TextureSand]        = Game::textureManager->loadTexture("textures/sand.png",         CTextureManager::FilterNone);
    m_texturesId[TextureBrick]       = Game::textureManager->loadTexture("textures/brick.png",        CTextureManager::FilterNone);
    m_texturesId[TextureBedRock]     = Game::textureManager->loadTexture("textures/bedrock.png",      CTextureManager::FilterNone);
    m_texturesId[TextureCobbleStone] = Game::textureManager->loadTexture("textures/cobble_stone.png", CTextureManager::FilterNone);
    m_texturesId[TextureWood]        = Game::textureManager->loadTexture("textures/wood.png",         CTextureManager::FilterNone);
    m_texturesId[TextureWoodSide]    = Game::textureManager->loadTexture("textures/wood_side.png",    CTextureManager::FilterNone);
    m_texturesId[TextureWoodenPlank] = Game::textureManager->loadTexture("textures/wooden_plank.png", CTextureManager::FilterNone);
    m_texturesId[TextureFurnaceFace] = Game::textureManager->loadTexture("textures/furnace_face.png", CTextureManager::FilterNone);
    m_texturesId[TextureFurnaceSide] = Game::textureManager->loadTexture("textures/furnace_side.png", CTextureManager::FilterNone);
    m_texturesId[TextureFurnaceTop]  = Game::textureManager->loadTexture("textures/furnace_top.png",  CTextureManager::FilterNone);
}


void CWorld::loadChunksAsynchronous(CWorld * world)
{
    T_ASSERT(world != nullptr);

    while (world->isRunning())
    {
        CChunk * chunk = nullptr;

        world->m_loadMutex.lock();

        if (!world->m_chunksUpdate.empty())
        {
            chunk = world->m_chunksUpdate.front();
            world->m_chunksUpdate.pop();
            std::cout << " chunk to load : " << chunk->getChunkX() << " x " << chunk->getChunkY() << std::endl;
        }

        world->m_loadMutex.unlock();

        if (chunk != nullptr && !chunk->isBuild())
        {
            chunk->build();
            chunk->computeLighting();

            int x = chunk->getChunkX();
            int y = chunk->getChunkY();

            // Mise à jour de la luminosité des chunks adjacents
            CChunk * chunkW = world->getChunk(x - 1, y);
            if (chunkW != nullptr)
            {
                chunkW->computeLighting();
            }

            CChunk * chunkE = world->getChunk(x + 1, y);
            if (chunkE != nullptr)
            {
                chunkE->computeLighting();
            }

            CChunk * chunkS = world->getChunk(x, y - 1);
            if (chunkS != nullptr)
            {
                chunkS->computeLighting();
            }

            CChunk * chunkN = world->getChunk(x, y + 1);
            if (chunkN != nullptr)
            {
                chunkN->computeLighting();
            }
        }

        sf::sleep(sf::milliseconds(20));
    }

    std::cout << "*****************************************" << std::endl;
    std::cout << "*      Fin du thread de chargement      *" << std::endl;
    std::cout << "*****************************************" << std::endl;
}


void CWorld::buildChunk(int chunkX, int chunkY, CSector sectors[SectorsInChunk], uint8_t * heightMap)
{
    m_generator->buildChunk(chunkX, chunkY, sectors, heightMap);
}


CCell CWorld::buildCellContent(int x, int y, int z) const
{
    return m_generator->getCellContent(x, y, z);
}


float CWorld::getTemperature(int x, int y) const
{
    return m_generator->getTemperature(x, y);
}


float CWorld::getHumidity(int x, int y) const
{
    return m_generator->getHumidity(x, y);
}


TBiome CWorld::getBiome(int x, int y) const
{
    return m_generator->getBiome(x, y);
}


CString getBiomeName(TBiome biome)
{
    switch (biome)
    {
        default:
            return "Unknown";

        case BiomePlains:
            return "Plains";
        case BiomeDesert:
            return "Desert";
        case BiomeForest:
            return "Forest";
        case BiomeSavanna:
            return "Savanna";
        case BiomeTundra:
            return "Tundra";
        case BiomeRainForest:
            return "Rain forest";
        case BiomeIceDesert:
            return "Ice desert";
        case BiomeTaiga:
            return "Taiga";
        case BiomeSeasonalForest:
            return "Seasonal forest";
        case BiomeSwampland:
            return "Swampland";
        case BiomeShrubland:
            return "Shrublands";
        case BiomeOcean:
            return "Ocean";
        case BiomeBeach:
            return "Beach";
    }
}
