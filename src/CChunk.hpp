/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef T_FILE_CCHUNK_HPP
#define T_FILE_CCHUNK_HPP

#include "CWorld.hpp"
#include "CSector.hpp"
#include "Core/Exceptions.hpp"


/**
 * Un chunk est une colonne de 16x16 cellules, représente une partie du monde.
 * Les chunks servent à optimiser l'utilisation de la mémoire et à limiter
 * la quantité de données affichées.
 * Chaque chunk est divisé en 8 sections.
 */

class CChunk
{
public:

    CChunk(int x, int y, CWorld * world);
    ~CChunk();

    CCell * getCell(int x, int y, int z) const;
    TCellContent getCellContent(int x, int y, int z) const;
    bool isCellEmpty(int x, int y, int z, bool waterIsEmpty = true, bool lavaIsEmpty = true) const;
    int getCellLighting(int x, int y, int z) const;
    bool isCellNeedUpdate(int x, int y, int z) const;
    int getCellWaterLevel(int x, int y, int z) const;
    int getCellLavaLevel(int x, int y, int z) const;
    TCellContent removeCell(int x, int y, int z);
    bool addCell(int x, int y, int z, TCellContent content, int data = 0);

    inline bool isBuild() const { return m_isBuild; }
    inline bool needUpdateBuffer() const { return m_needBufferUpdate; }

    void build();
    void requestUpdateBuffer();
    void updateBuffer();
    void updateChunk(int frameTime);
    void display();
    void displayBlend();
    void computeLighting();

    inline int getChunkX() const { return m_chunkX; }
    inline int getChunkY() const { return m_chunkY; }

    inline int getChunkMinX() const { return m_chunkX * ChunkSize; }
    inline int getChunkMinY() const { return m_chunkY * ChunkSize; }

    //bool save() const;
    //bool load();

    // Utilisés uniquement pour la sauvegarde
    inline const CSector * getSector(int sector) const { T_ASSERT(sector >= 0 && sector < SectorsInChunk); return &m_sectors[sector]; }
    inline const uint8_t * getHeightMap() const { return m_heightMap; };
    int getHeight(int x, int y) const;

private:


#include "Core/struct_alignment_start.h"

    /// Coordonnées d'une cellule à l'intérieur d'un chunk.
    struct TCellCoords
    {
        uint16_t x:4; // SYNCHRO: ChunkSize
        uint16_t y:4; // SYNCHRO: ChunkSize
        uint16_t z:8; // SYNCHRO: ChunkHeight

        inline TCellCoords(int xx, int yy, int zz) : x(xx), y(yy), z(zz) { }

        inline bool operator==(const TCellCoords& other) const { return (x == other.x && y == other.y && z == other.z); }
    };

#include "Core/struct_alignment_end.h"


    int buildChunkPerlin();
    int buildChunkDebug();

    void resetChunk();
    void setCellContent(int x, int y, int z, TCellContent content, int data = 0);
    void updateCellLighting(int x, int y, int z, int lighting);
    void setCellLighting(int x, int y, int z, int lighting);
    void setCellNeedUpdate(int x, int y, int z, bool needUpdate = true);
    void setWaterLevel(int x, int y, int z, int level);
    void setLavaLevel(int x, int y, int z, int level);
    void updateFluidInNeighboors(int x, int y, int z);

    static int getFluidLevel(CCell * cell);


    CWorld * m_world;
    int m_chunkX;                 ///< Coordonnée X du chunk.
    int m_chunkY;                 ///< Coordonnée Y du chunk.
    Ted::CBuffer * m_buffer;      ///< Buffer graphique pour les surfaces non transparentes.
    Ted::CBuffer * m_bufferBlend; ///< Buffer graphique pour les surfaces transparentes.
    bool m_isModified;            ///< Indique si le chunk a été modifié par le joueur.
    bool m_isBuilding;            ///< Construction du chunk en cours.
    bool m_isBuild;               ///< Indique si le chunk a été construit.
    bool m_needBufferUpdate;      ///< Le buffer graphique doit être mis à jour.
    CSector m_sectors[SectorsInChunk];
    uint8_t m_heightMap[ChunkSize * ChunkSize];
    std::list<TCellCoords> m_cellsUpdate; ///< Cellule qui doivent être mise à jour.
};

#endif // T_FILE_CCHUNK_HPP
