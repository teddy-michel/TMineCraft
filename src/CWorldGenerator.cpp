/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CWorldGenerator.hpp"


const int HeightBase = 64;
const int HeightMin = 32;
const int HeightMax = 128;

const float OceanPerlinValue = -0.5f;
const float BeachPerlinValue = -0.48f;


CWorldGenerator::CWorldGenerator(int seed) :
IWorldGenerator     (seed),
m_perlinRelief      (16, 6 * 0.0006f, 1, m_seed + 0),
m_perlinElevation   (16, 6 * 0.0002f, 1, m_seed + 1),
m_perlinTemperature (16, 6 * 0.0004f, 1, m_seed + 2),
m_perlinHumidity    (16, 6 * 0.0004f, 1, m_seed + 3)
{

}


CWorldGenerator::~CWorldGenerator()
{

}


CCell CWorldGenerator::getCellContent(int x, int y, int z) const
{
    if (z <= 0)
    {
        return CCell(ContentBedRock);
    }

    int elevation = getElevation(x, y);

    if (z > elevation)
    {
        return CCell(ContentEmpty);
    }

    TBiome biome = getBiome(x, y);
    float valDirtHeight = getPerlin(x - 1024, y - 1024);
    TCellContent content = ContentEmpty;

    if (z > elevation && z <= WaterLevel)
    {
        content = ContentWaterSource;
    }
    else if (biome == BiomeDesert || biome == BiomeOcean || biome == BiomeBeach)
    {
        // Deux ou trois couches de sable
        if (z == elevation || z == elevation - 1 || (valDirtHeight < 0.0f && z == elevation - 2))
        {
            content = ContentSand;
        }
        // Puis de la pierre
        else
        {
            content = ContentCobbleStone;
        }
    }
    else
    {
        // Herbe au sommet
        if (z == elevation)
        {
            if (elevation >= WaterLevel)
            {
                content = ContentGrass;
            }
            else
            {
                content = ContentDirt;
            }
        }
        // Puis une ou deux couches de terre
        else if (z == elevation - 1 || (valDirtHeight < 0.0f && z == elevation - 2))
        {
            content = ContentDirt;
        }
        // Puis de la pierre
        else
        {
            content = ContentCobbleStone;
        }
    }

    return CCell(content);
}


/**
 * Donne l'altitude d'une cellule du monde.
 *
 * \param x Coordonnée X de la cellule dans le monde.
 * \param y Coordonnée Y de la cellule dans le monde.
 * \return Altitude de la cellule.
 */

int CWorldGenerator::getElevation(int x, int y) const
{
    float valR = m_perlinRelief.Get(x, y);
    float valE = m_perlinElevation.Get(x, y);

    float rnorm = (valR + 1.0f) * 0.5f;
    float e = HeightBase;

    if (valE <= OceanPerlinValue)
        e += rnorm * (HeightMin - HeightBase) * (1.0f - (valE + 1.0f) / (OceanPerlinValue + 1.0f));
    else
        e += rnorm * (HeightMax - HeightBase) * (valE - OceanPerlinValue) / (1.0f - OceanPerlinValue);

    // Ajustement de la hauteur (entre 32 et 160 (12.5% et 62.5%))
    //int mz = static_cast<int>(valR * ChunkHeight * 0.25f + ChunkHeight * 0.375f);
    int mz = static_cast<int>(e);

    mz %= ChunkHeight;

    if (mz < 1)
    {
        mz = 1;
    }

    return mz;
}


/**
 * Donne la température d'une cellule du monde.
 *
 * \param x Coordonnée X de la cellule dans le monde.
 * \param y Coordonnée Y de la cellule dans le monde.
 * \return Température de la cellule (entre 0 et 1).
 */

float CWorldGenerator::getTemperature(int x, int y) const
{
    float val = m_perlinTemperature.Get(x, y);
    return (val + 1.0f) * 0.5f;
}


/**
 * Donne l'humidité d'une cellule du monde.
 *
 * \param x Coordonnée X de la cellule dans le monde.
 * \param y Coordonnée Y de la cellule dans le monde.
 * \return Humidité de la cellule (entre 0 et 1).
 */

float CWorldGenerator::getHumidity(int x, int y) const
{
    float val = m_perlinHumidity.Get(x, y);
    return (val + 1.0f) * 0.5f;
}


/**
 * Donne le biome d'une cellule du monde.
 *
 * \param x Coordonnée X de la cellule dans le monde.
 * \param y Coordonnée Y de la cellule dans le monde.
 * \return Biome auquel appartient la cellule, déterminé à partir de
 *         sa température et de son humidité.
 */

TBiome CWorldGenerator::getBiome(int x, int y) const
{
    float valE = m_perlinElevation.Get(x, y);
    if (valE <= OceanPerlinValue)
        return BiomeOcean;
    if (valE <= BeachPerlinValue)
        return BiomeBeach;

    float temperature = getTemperature(x, y);
    float humidy = getHumidity(x, y);
    float precipitation = humidy * temperature;

    if (temperature < 0.1f)
        return BiomeIceDesert;

    if (precipitation < 0.2f)
    {
        if (temperature < 0.5f)
            return BiomeTundra;
        if (temperature < 0.95f)
            return BiomeSavanna;
        return BiomeDesert;
    }

    if ((precipitation > 0.5f) && (temperature < 0.7f))
        return BiomeSwampland;

    if (temperature < 0.5f)
        return BiomeTaiga;

    if (temperature < 0.97f)
    {
        if (precipitation < 0.35f)
            return BiomeShrubland;
        return BiomeForest;
    }

    if (precipitation < 0.45f)
        return BiomePlains;

    if (precipitation < 0.9f)
        return BiomeSeasonalForest;

    return BiomeRainForest;
}
