/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#include "IWorldGenerator.hpp"
#include "CSector.hpp"


IWorldGenerator::IWorldGenerator(int seed) :
m_seed (seed)
{

}


IWorldGenerator::~IWorldGenerator()
{

}


void IWorldGenerator::buildChunk(int chunkMinX, int chunkMinY, CSector sectors[SectorsInChunk], uint8_t * heightMap)
{
    for (int x = 0; x < ChunkSize; ++x)
    {
        for (int y = 0; y < ChunkSize; ++y)
        {
            int height = 0;

            for (int sector = 0; sector < SectorsInChunk; ++sector)
            {
                for (int z = 0; z < ChunkSize; ++z)
                {
                    CCell cellContent = getCellContent(chunkMinX + x, chunkMinY + y, sector * ChunkSize + z);
                    sectors[sector].setCellContent(x, y, z, cellContent.getCellContent(), cellContent.data);

                    if (!cellContent.isEmpty() && !cellContent.isWater())
                    {
                        height = sector * ChunkSize + z;
                    }
                }
            }

            heightMap[x + y * ChunkSize] = height;
        }
    }
}


float IWorldGenerator::getTemperature(int x, int y) const
{
    return 0.5f;
}


float IWorldGenerator::getHumidity(int x, int y) const
{
    return 0.5f;
}
