/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef T_FILE_IWORLDGENERATOR_HPP
#define T_FILE_IWORLDGENERATOR_HPP

#include "Biome.hpp"
#include "CCell.hpp"
#include "CWorld.hpp"


class CSector;


class IWorldGenerator
{
public:

    IWorldGenerator(int seed);
    virtual ~IWorldGenerator();

    inline int getSeed() const;

    virtual void buildChunk(int chunkX, int chunkY, CSector sectors[SectorsInChunk], uint8_t * heightMap);

    virtual CCell getCellContent(int x, int y, int z) const = 0;
    virtual float getTemperature(int x, int y) const;
    virtual float getHumidity(int x, int y) const;
    virtual TBiome getBiome(int x, int y) const = 0;

protected:

    int32_t m_seed; ///< Graine utilisée pour générer le monde.
};


inline int IWorldGenerator::getSeed() const
{
    return m_seed;
}

#endif // T_FILE_IWORLDGENERATOR_HPP
