/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef T_FILE_CFONT_HPP
#define T_FILE_CFONT_HPP

#include "Core/CString.hpp"
#include "Graphic/CColor.hpp"


class CFont
{
public:

    CFont();
    ~CFont();

    bool loadFromFile(const Ted::CString& fileName);
    void displayText(const Ted::CString& text, int x, int y, const Ted::CColor& color) const;

private:

    unsigned int m_textureId;
    int m_size;
};

#endif // T_FILE_CFONT_HPP
