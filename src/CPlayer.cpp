/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CPlayer.hpp"
#include "CGame.hpp"
#include "CCell.hpp"
#include <GL/glew.h>
#include <SFML/Window.hpp>
#include <fstream>
#include <cstring>


using namespace Ted;


const float PlayerMouseMotionSpeed = 0.2f; ///< Vitesse de déplacement du curseur.

const float PlayerSpeed = 4.0f * CellSize / 1000; ///< Vitesse de déplacement du joueur en unité/ms.
const float PlayerFlyingSpeed = PlayerSpeed * 2;  ///< Vitesse de déplacement en vol.
const float PlayerMaxFallSpeed = 0.05f * CellSize; ///< Vitesse maximale de chute en unité/ms.
const float Gravity = 1.5f * 9.80665f * CellSize / 1000000; ///< Accélération de la pesanteur en unité/ms².
const float PlayerJumpSpeed = 6.0f * CellSize / 1000;  ///< Vitesse verticale de saut en unité/ms.
const float PlayerJumpMoveSpeed = PlayerSpeed * 0.01f; ///< Vitesse horizontale en saut.
const float PlayerJumpMaxSpeed = PlayerSpeed * 0.5f;   ///< Vitesse horizontale initiale maximale en saut.

const float PiOver180 = 0.0174532925f; ///< Pi sur 180.
const float SinCos45 = 0.707106781f;


/**
 * Initialise le joueur.
 *
 * \param game      Classe principale de l'application.
 * \param position  Position de départ du joueur.
 * \param direction Direction du joueur.
 */

CPlayer::CPlayer(CGame * game, const TVector3F& position, const TVector3F& direction) :
m_name                ("Player"),
m_game                (game),
m_world               (m_game->getWorld()),
m_position            (position),
m_direction           (direction),
m_isFlying            (false),
m_isOnGround          (true),
m_life                (100.0f),
m_foodLevel           (100.0f),
m_foodSaturationLevel (100.0f),
m_foodExhaustionLevel (0.0f),
m_timeAction          (DefaultTimeAction),
m_currentItem         (0),
m_forward             (Origin3F),
m_left                (Origin3F),
m_theta               (0.0f),
m_phi                 (0.0f)
{
    anglesFromVectors();
}


CPlayer::~CPlayer()
{

}


void CPlayer::updateDirection(int xrel, int yrel)
{
    m_theta -= xrel * PlayerMouseMotionSpeed;
    m_phi   -= yrel * PlayerMouseMotionSpeed;
    vectorsFromAngles();
}


/**
 * Effectue les actions et met à jour l'état du joueur.
 *
 * \param frameTime Durée de la frame.
 */

void CPlayer::doActions(unsigned int frameTime)
{
    // ----------------------------------------------
    // Points de vie
    // ----------------------------------------------

    static int nextLifeUpdate = TimeBetweenLifeUpdate;

    // Mise à jour des points de vie selon le niveau de faim
    if (m_life < 100.0f && m_foodLevel >= LifeUpdateRegenerateFoodLevel)
    {
        nextLifeUpdate -= frameTime;

        while (nextLifeUpdate <= 0)
        {
            nextLifeUpdate += TimeBetweenLifeUpdate;
            addLife(LifeUpdateRegeneratePoints);
            increaseExhaustion(LifeUpdateRegenerateExhaustion);
        }
    }
    else if (m_foodLevel <= 0.0f)
    {
        nextLifeUpdate -= frameTime;

        while (nextLifeUpdate <= 0)
        {
            nextLifeUpdate += TimeBetweenLifeUpdate;
            addLife(LifeUpdateDepletePoints);
        }
    }
    else
    {
        nextLifeUpdate = TimeBetweenLifeUpdate;
    }


    // ----------------------------------------------
    // Déplacement du joueur
    // ----------------------------------------------

    bool forward = m_game->isActionActive(ActionForward);
    bool backward = !forward && m_game->isActionActive(ActionBackward);

    TVector3F newPosition = m_position;

    // Saut
    if (m_isOnGround && m_game->isActionActive(ActionJump))
    {
        std::cout << "Player: JUMP" << std::endl;
        m_motion.Z = PlayerJumpSpeed;
        m_isOnGround = false;

        // On limite la vitesse horizontale
        TVector2F moveH(m_motion.X, m_motion.Y);
        if (moveH.norm() > PlayerJumpMaxSpeed)
        {
            moveH /= moveH.norm() / PlayerJumpMaxSpeed;
        }

        m_motion.X = moveH.X;
        m_motion.Y = moveH.Y;
    }

    // Déplacement libre
    if (m_isFlying && (!m_isOnGround || (forward  && m_phi > 0) || (backward && m_phi < 0)))
    {
        // Réinitialisation de la vitesse horizontale
        m_motion.X = 0.0f;
        m_motion.Y = 0.0f;

        TVector3F tmp = m_forward * PlayerFlyingSpeed;

        // Déplacement vers l'avant
        if (forward)
        {
            m_motion.X += tmp.X;
            m_motion.Y += tmp.Y;
            m_motion.Z += tmp.Z;
        }
        // Déplacement vers l'arrière
        else if (backward)
        {
            m_motion.X -= tmp.X;
            m_motion.Y -= tmp.Y;
            m_motion.Z -= tmp.Z;
        }

        tmp = m_left * PlayerFlyingSpeed;

        // Déplacement vers la gauche
        if (m_game->isActionActive(ActionStrafeLeft))
        {
            m_motion.X += tmp.X;
            m_motion.Y += tmp.Y;
            m_motion.Z += tmp.Z;
        }
        // Déplacement vers la droite
        else if (m_game->isActionActive(ActionStrafeRight))
        {
            m_motion.X -= tmp.X;
            m_motion.Y -= tmp.Y;
            m_motion.Z -= tmp.Z;
        }
    }
    else
    {
        // Réinitialisation de la vitesse horizontale
        if (m_isOnGround)
        {
            m_motion.X = 0.0f;
            m_motion.Y = 0.0f;
        }

        TVector3F forwardXY(m_forward.X, m_forward.Y, 0.0f);
        forwardXY.normalize();
        TVector3F tmp = forwardXY * (m_isOnGround ? PlayerSpeed : PlayerJumpMoveSpeed);

        // Déplacement vers l'avant
        if (forward)
        {
            m_motion.X += tmp.X;
            m_motion.Y += tmp.Y;
        }
        // Déplacement vers l'arrière
        else if (backward)
        {
            m_motion.X -= tmp.X;
            m_motion.Y -= tmp.Y;
        }

        TVector3F leftXY(m_left.X, m_left.Y, 0.0f);
        leftXY.normalize();
        tmp = leftXY * (m_isOnGround ? PlayerSpeed : PlayerJumpMoveSpeed);

        // Déplacement vers la gauche
        if (m_game->isActionActive(ActionStrafeLeft))
        {
            m_motion.X += tmp.X;
            m_motion.Y += tmp.Y;
        }
        // Déplacement vers la droite
        else if (m_game->isActionActive(ActionStrafeRight))
        {
            m_motion.X -= tmp.X;
            m_motion.Y -= tmp.Y;
        }
    }

    newPosition += m_motion * frameTime;

    if (m_isOnGround || m_isFlying)
    {
        TVector3F move = newPosition - m_position;

        if (move != Origin3F)
        {
            newPosition = getCorrectPosition(m_position, move);
        }
    }
    // Chute ou saut
    else
    {
        newPosition.Z -= 0.5f * Gravity * frameTime * frameTime;
        m_motion.Z -= Gravity * frameTime;

        // Vitesse limite
        if (m_motion.Z < -PlayerMaxFallSpeed)
        {
            m_motion.Z = -PlayerMaxFallSpeed;
        }



        TVector3F move = newPosition - m_position;

        if (move != Origin3F)
        {
            newPosition = getCorrectPosition(m_position, move);

            // Collision
            if (m_position + move != newPosition)
            {
                m_motion = Origin3F;
            }
        }



/*
        int x1 = floor((m_position.X - PlayerHalfSize) / CellSize);
        int x2 = floor((m_position.X + PlayerHalfSize) / CellSize);
        int y1 = floor((m_position.Y - PlayerHalfSize) / CellSize);
        int y2 = floor((m_position.Y + PlayerHalfSize) / CellSize);
        int z = floor(m_position.Z / CellSize);

        int z1 = m_world->findFirstZ(x1, y1, z);
        int z2 = m_world->findFirstZ(x1, y2, z);
        int z3 = m_world->findFirstZ(x2, y1, z);
        int z4 = m_world->findFirstZ(x2, y2, z);
        float correctZ = (std::max(z1, std::max(z2, std::max(z3, z4))) + 1) * CellSize;

        //std::cout << " frametime = " << frameTime << ", vitesse th = " << m_motion.Z << ", real = " << ((m_position.Z - newPosition.Z) / frameTime) << std::endl;

        if (newPosition.Z <= correctZ)
        {
            newPosition.Z = correctZ;
            m_isOnGround = true;
            m_motion = Origin3F;
        }
*/
    }

    //int oldChunkX = floor(m_position.X / (CellSize * ChunkSize));
    //int oldChunkY = floor(m_position.Y / (CellSize * ChunkSize));

    int newChunkX = floor(newPosition.X / (CellSize * ChunkSize));
    int newChunkY = floor(newPosition.Y / (CellSize * ChunkSize));

    m_world->setCurrentChunk(newChunkX, newChunkY);

    //std::cout << "  Deplacement : " << (newPosition - m_position).norm() << std::endl;

    m_position = newPosition;
    m_direction = getEyesPosition() + m_forward;

        int x1 = floor((m_position.X - PlayerHalfSize) / CellSize);
        int x2 = floor((m_position.X + PlayerHalfSize) / CellSize);
        int y1 = floor((m_position.Y - PlayerHalfSize) / CellSize);
        int y2 = floor((m_position.Y + PlayerHalfSize) / CellSize);
        int z = floor(m_position.Z / CellSize);

        int z1 = m_world->findFirstZ(x1, y1, z);
        int z2 = m_world->findFirstZ(x1, y2, z);
        int z3 = m_world->findFirstZ(x2, y1, z);
        int z4 = m_world->findFirstZ(x2, y2, z);
        float correctZ = (std::max(z1, std::max(z2, std::max(z3, z4))) + 1) * CellSize;

    // On vérifie qu'on est toujours sur le sol
    if (m_isOnGround || m_isFlying)
    {
        if (correctZ < m_position.Z)
        {
            m_isOnGround = false;
            m_motion.Z = 0.0f;
        }
        else
        {
            m_isOnGround = true;
            m_position.Z = correctZ;
        }
    }
    else
    {
        if (m_position.Z - correctZ <= 0.0001f)
        {
            m_position.Z = correctZ;
            m_isOnGround = true;
            m_motion = Origin3F;
        }
    }


    // Rotation vers la gauche
    if (m_game->isActionActive(ActionTurnLeft))
    {
        m_theta += frameTime * PlayerMouseMotionSpeed;
        vectorsFromAngles();
    }

    // Rotation vers la droite
    if (m_game->isActionActive(ActionTurnRight))
    {
        m_theta -= frameTime * PlayerMouseMotionSpeed;
        vectorsFromAngles();
    }


    // ----------------------------------------------
    // Actions
    // ----------------------------------------------

    if (m_game->isActionActive(ActionFire1))
    {
        m_timeAction -= frameTime;

        if (m_timeAction <= 0)
        {
            TVector3F iPoint;
            TFace iFace;
            TVector3I iCube;

            // Destruction du cube
            if (m_world->findIntersection(getEyesPosition(), m_direction - getEyesPosition(), iPoint, iFace, iCube))
            {
                TCellContent content = m_world->removeCell(iCube.X, iCube.Y, iCube.Z);
                addInventoryItem(content, 1);
            }

            m_timeAction = DefaultTimeAction;
        }
    }
    else
    {
        m_timeAction = DefaultTimeAction;

        if (m_game->isActionActive(ActionFire2))
        {
            int type = ContentEmpty;
            int count = 0;
            int data = 0;
            getInventoryItem(getCurrentItem(), type, count, data);

            if (type > ContentEmpty && type < ContentCount)
            {
                TVector3F iPoint;
                TFace iFace;
                TVector3I iCube;

                // Ajout du cube
                if (m_world->findIntersection(getEyesPosition(), m_direction - getEyesPosition(), iPoint, iFace, iCube))
                {
                    TVector3I newCube;

                    switch (iFace)
                    {
                        case FaceNorth:
                            newCube = TVector3I(iCube.X, iCube.Y + 1, iCube.Z);
                            break;

                        case FaceEast:
                            newCube = TVector3I(iCube.X + 1, iCube.Y, iCube.Z);
                            break;

                        case FaceSouth:
                            newCube = TVector3I(iCube.X, iCube.Y - 1, iCube.Z);
                            break;

                        case FaceWest:
                            newCube = TVector3I(iCube.X - 1, iCube.Y, iCube.Z);
                            break;

                        case FaceTop:
                            newCube = TVector3I(iCube.X, iCube.Y, iCube.Z + 1);
                            break;

                        case FaceBottom:
                            newCube = TVector3I(iCube.X, iCube.Y, iCube.Z - 1);
                            break;
                    }

                    if ((newCube.X + 1) * CellSize < m_position.X - PlayerHalfSize ||
                        (newCube.X * CellSize) > m_position.X + PlayerHalfSize ||
                        (newCube.Y + 1) * CellSize < m_position.Y - PlayerHalfSize ||
                        (newCube.Y * CellSize) > m_position.Y + PlayerHalfSize ||
                        (newCube.Z + 1) * CellSize < m_position.Z ||
                        (newCube.Z * CellSize) > (m_position.Z + PlayerHeight))
                    {
                        // Recherche de l'orientation
                        if (type == ContentFurnace)
                        {
                            TVector3F ray3D = iPoint - m_position;
                            TVector2F ray(ray3D.X, ray3D.Y);
                            ray.normalize();

                            if (ray.Y > SinCos45)
                            {
                                data = CELL_FURNACE_SOUTH;
                            }
                            else if (ray.Y < -SinCos45)
                            {
                                data = CELL_FURNACE_NORTH;
                            }
                            else if (ray.X > 0)
                            {
                                data = CELL_FURNACE_WEST;
                            }
                            else
                            {
                                data = CELL_FURNACE_EAST;
                            }
                        }

                        std::cout << "Pose d'un bloc de type " << type << std::endl;

                        if (m_world->addCell(newCube.X, newCube.Y, newCube.Z, (TCellContent) type, data))
                        {
                            removeInventoryItem(getCurrentItem());
                        }
                    }
                }
            }
        }
    }
}


/**
 * Convertit les angles en direction.
 ******************************/

void CPlayer::vectorsFromAngles()
{
    if (m_phi > 89.0f)
    {
        m_phi = 89.0f;
    }
    else if (m_phi < -89.0f)
    {
        m_phi = -89.0f;
    }

    // Pour éviter un dépassement de valeur ou une perte de précision
    if (m_theta < -180.0f)
    {
        m_theta += 360.0f;
    }
    else if (m_theta > 180.0f)
    {
        m_theta -= 360.0f;
    }

    const double r_temp = cos(m_phi * PiOver180);
    m_forward.X = r_temp * cos(m_theta * PiOver180);
    m_forward.Y = r_temp * sin(m_theta * PiOver180);
    m_forward.Z = sin(m_phi * PiOver180);

    m_left = VectorCross(TVector3F(0.0f, 0.0f, 1.0f), m_forward);
    m_left.normalize();

    m_direction = getEyesPosition() + m_forward;
}


/**
 * Convertit la position et la direction en angles.
 ******************************/

void CPlayer::anglesFromVectors()
{
    m_forward = (m_direction - m_position).getNormalized();
    m_left = VectorCross(TVector3F(0.0f, 0.0f, 1.0f), m_forward);
    m_left.normalize();
    m_phi = asin(m_forward.Z) / PiOver180;

    if (std::abs(m_forward.X) < std::numeric_limits<float>::epsilon())
    {
        m_theta = 90;
    }
    else
    {
        m_theta = atan(m_forward.Y / m_forward.X) / PiOver180;
    }

    if (m_phi > 89.0f)
    {
        m_phi = 89.0f;
    }
    else if (m_phi < -89.0f)
    {
        m_phi = -89.0f;
    }

    // Pour éviter un dépassement de valeur ou une perte de précision
    if (m_theta < -180.0f)
    {
        m_theta += 360.0f;
    }
    else if (m_theta > 180.0f)
    {
        m_theta -= 360.0f;
    }

    m_direction = getEyesPosition() + m_forward;
}


TVector3F CPlayer::getCorrectPosition(const TVector3F& position, const TVector3F& move) const
{
    TVector3F s[8];

    // Sommets du volume englobant
    s[0] = position + TVector3F(-PlayerHalfSize, -PlayerHalfSize, 0.0f);
    s[1] = position + TVector3F(-PlayerHalfSize,  PlayerHalfSize, 0.0f);
    s[2] = position + TVector3F( PlayerHalfSize, -PlayerHalfSize, 0.0f);
    s[3] = position + TVector3F( PlayerHalfSize,  PlayerHalfSize, 0.0f);
    s[4] = position + TVector3F(-PlayerHalfSize, -PlayerHalfSize, PlayerHeight);
    s[5] = position + TVector3F(-PlayerHalfSize,  PlayerHalfSize, PlayerHeight);
    s[6] = position + TVector3F( PlayerHalfSize, -PlayerHalfSize, PlayerHeight);
    s[7] = position + TVector3F( PlayerHalfSize,  PlayerHalfSize, PlayerHeight);
/*
    s[0] = position + TVector3F(-PlayerHalfSize, -PlayerHalfSize, 0.0f);
    s[1] = position + TVector3F(-PlayerHalfSize,  PlayerHalfSize - 0.01f, 0.0f);
    s[2] = position + TVector3F( PlayerHalfSize - 0.01f, -PlayerHalfSize, 0.0f);
    s[3] = position + TVector3F( PlayerHalfSize - 0.01f,  PlayerHalfSize - 0.01f, 0.0f);
    s[4] = position + TVector3F(-PlayerHalfSize, -PlayerHalfSize, PlayerHeight);
    s[5] = position + TVector3F(-PlayerHalfSize,  PlayerHalfSize - 0.01f, PlayerHeight);
    s[6] = position + TVector3F( PlayerHalfSize - 0.01f, -PlayerHalfSize, PlayerHeight);
    s[7] = position + TVector3F( PlayerHalfSize - 0.01f,  PlayerHalfSize - 0.01f, PlayerHeight);
*/
    TVector3F iPoint[8];
    TFace iFace[8];
    TVector3I iCube[8];
    double minDist = move.norm();
    int minS = -1;

    for (int i = 0; i < 8; ++i)
    {

        // Correction de la position des sommets s'ils sont à la limite d'une cellule
        if (s[i].X / CellSize == floor(s[i].X / CellSize))
        {
            if (s[i].X > position.X)
            {
                s[i].X -= 0.00001f;
                //std::cout << " # correction en X : -0.00001f" << std::endl;
            }
            else if (s[i].X < position.X)
            {
                s[i].X += 0.00001f;
                //std::cout << " # correction en X : +0.00001f" << std::endl;
            }
        }

        if (s[i].Y / CellSize == floor(s[i].Y / CellSize))
        {
            if (s[i].Y > position.Y)
            {
                s[i].Y -= 0.00001f;
                //std::cout << " # correction en Y : -0.00001f" << std::endl;
            }
            else if (s[i].Y < position.Y)
            {
                s[i].Y += 0.00001f;
                //std::cout << " # correction en Y : +0.00001f" << std::endl;
            }
        }

        if (s[i].Z / CellSize == floor(s[i].Z / CellSize))
        {
            if (s[i].Z > position.Z)
            {
                s[i].Z -= 0.00001f;
                //std::cout << " # correction en Z : -0.00001f" << std::endl;
            }
            else if (s[i].Z < position.Z)
            {
                s[i].Z += 0.00001f;
                //std::cout << " # correction en Z : +0.00001f" << std::endl;
            }
        }


        if (m_world->findIntersection(s[i], move, iPoint[i], iFace[i], iCube[i], minDist))
        {
/*
            switch (iFace[i])
            {
                case FaceEast:
                    iPoint[i].X += 0.01f;
                    break;

                case FaceNorth:
                    iPoint[i].Y += 0.01f;
                    break;

                case FaceTop:
                    iPoint[i].Z += 0.01f;
                    break;
            }
*/
            minDist = (iPoint[i] - s[i]).norm();
            minS = i;
            //std::cout << "Collision ! sommet " << i << ", dist = " << minDist << std::endl;
        }
    }

    // Glissement sur une surface
    if (minS != -1)
    {
        //std::cout << "COLLISION:" << std::endl;

        TVector3F correctMove = move * (minDist / move.norm());
        //std::cout << "  correctMove = " << correctMove << " (norm = " << correctMove.norm() << "), move = " << move << std::endl;

        TVector3F newPosition = position;
        TVector3F moveRest = move;

        if (correctMove.norm() > 0.0001f)
        {
            newPosition += correctMove;
            moveRest -= correctMove;

            //std::cout << "  position: " << position << " -> " << newPosition << " (" << position + move << ")" << std::endl;
        }
        else
        {
            //std::cout << "  position: " << position << " -> idem" << std::endl;
        }

        // Glissement sur la face
        switch (iFace[minS])
        {
            case FaceEast:
            case FaceWest:
                moveRest.X = 0.0f;
                break;

            case FaceNorth:
            case FaceSouth:
                moveRest.Y = 0.0f;
                break;

            case FaceTop:
            case FaceBottom:
                moveRest.Z = 0.0f;
                break;
        }

        //std::cout << "  move restant = " << moveRest << std::endl;
        minDist = moveRest.norm();
        minS = -1;

        for (int i = 0; i < 8; ++i)
        {
            s[i] += correctMove;

            // Correction de la position des sommets s'ils sont à la limite d'une cellule
            if (s[i].X / CellSize == floor(s[i].X / CellSize))
            {
                if (s[i].X > newPosition.X)
                {
                    s[i].X -= 0.00001f;
                    //std::cout << " # correction en X : -0.00001f" << std::endl;
                }
                else if (s[i].X < newPosition.X)
                {
                    s[i].X += 0.00001f;
                    //std::cout << " # correction en X : +0.00001f" << std::endl;
                }
            }

            if (s[i].Y / CellSize == floor(s[i].Y / CellSize))
            {
                if (s[i].Y > newPosition.Y)
                {
                    s[i].Y -= 0.00001f;
                    //std::cout << " # correction en Y : -0.00001f" << std::endl;
                }
                else if (s[i].Y < newPosition.Y)
                {
                    s[i].Y += 0.00001f;
                    //std::cout << " # correction en Y : +0.00001f" << std::endl;
                }
            }

            if (s[i].Z / CellSize == floor(s[i].Z / CellSize))
            {
                if (s[i].Z > newPosition.Z)
                {
                    s[i].Z -= 0.00001f;
                    //std::cout << " # correction en Z : -0.00001f" << std::endl;
                }
                else if (s[i].Z < newPosition.Z)
                {
                    s[i].Z += 0.00001f;
                    //std::cout << " # correction en Z : +0.00001f" << std::endl;
                }
            }


            if (m_world->findIntersection(s[i], moveRest, iPoint[i], iFace[i], iCube[i], minDist))
            {
                minDist = (iPoint[i] - s[i]).norm();
                minS = i;
            }
        }

        // Bloquer par un bloc
        if (minS != -1)
        {
            //std::cout << "  COLLISION:" << std::endl;
            TVector3F correctMove2 = moveRest * (minDist / moveRest.norm());

            if (correctMove2.norm() > 0.0001f)
            {
                //std::cout << "    position: " << position << " -> " << (newPosition + correctMove2) << " (" << newPosition + moveRest << ")" << std::endl;
                newPosition += correctMove2;
                moveRest -= correctMove2;
            }
            else
            {
                //std::cout << "    position: " << position << " -> idem" << std::endl;
            }

            // Glissement sur la face
            switch (iFace[minS])
            {
                case FaceEast:
                case FaceWest:
                    moveRest.X = 0.0f;
                    break;

                case FaceNorth:
                case FaceSouth:
                    moveRest.Y = 0.0f;
                    break;

                case FaceTop:
                case FaceBottom:
                    moveRest.Z = 0.0f;
                    break;
            }

            //std::cout << "  move restant = " << moveRest << std::endl;
            minDist = moveRest.norm();
            minS = -1;

            for (int i = 0; i < 8; ++i)
            {
                s[i] += correctMove2;

                // Correction de la position des sommets s'ils sont à la limite d'une cellule
                if (s[i].X / CellSize == floor(s[i].X / CellSize))
                {
                    if (s[i].X > newPosition.X)
                    {
                        s[i].X -= 0.00001f;
                        //std::cout << " # correction en X : -0.00001f" << std::endl;
                    }
                    else if (s[i].X < newPosition.X)
                    {
                        s[i].X += 0.00001f;
                        //std::cout << " # correction en X : +0.00001f" << std::endl;
                    }
                }

                if (s[i].Y / CellSize == floor(s[i].Y / CellSize))
                {
                    if (s[i].Y > newPosition.Y)
                    {
                        s[i].Y -= 0.00001f;
                        //std::cout << " # correction en Y : -0.00001f" << std::endl;
                    }
                    else if (s[i].Y < newPosition.Y)
                    {
                        s[i].Y += 0.00001f;
                        //std::cout << " # correction en Y : +0.00001f" << std::endl;
                    }
                }

                if (s[i].Z / CellSize == floor(s[i].Z / CellSize))
                {
                    if (s[i].Z > newPosition.Z)
                    {
                        s[i].Z -= 0.00001f;
                        //std::cout << " # correction en Z : -0.00001f" << std::endl;
                    }
                    else if (s[i].Z < newPosition.Z)
                    {
                        s[i].Z += 0.00001f;
                        //std::cout << " # correction en Z : +0.00001f" << std::endl;
                    }
                }


                if (m_world->findIntersection(s[i], moveRest, iPoint[i], iFace[i], iCube[i], minDist))
                {
                    minDist = (iPoint[i] - s[i]).norm();
                    minS = i;
                }
            }

            // Bloquer par un bloc
            if (minS != -1)
            {
                //std::cout << "  COLLISION:" << std::endl;
                TVector3F correctMove3 = moveRest * (minDist / moveRest.norm());

                if (correctMove3.norm() > 0.0001f)
                {
                    //std::cout << "    position: " << position << " -> " << (newPosition + correctMove3) << " (" << newPosition + moveRest << ")" << std::endl;
                    newPosition += correctMove3;
                }
                else
                {
                    //std::cout << "    position: " << position << " -> idem" << std::endl;
                }
            }
            else
            {
                newPosition += moveRest;
                //std::cout << "    position: " << position << " -> " << newPosition << std::endl;
            }
        }
        else
        {
            newPosition += moveRest;
            //std::cout << "    position: " << position << " -> " << newPosition << std::endl;
        }

        return newPosition;
    }
    else
    {
        TVector3F newPosition = position + move;
        //std::cout << "MOVE: position: " << position << " -> " << newPosition << std::endl;
        return newPosition;
    }
}


void CPlayer::updateCamera()
{
    _gluLookAt(getEyesPosition(), m_direction);
}


/**
 * Affiche les objets 3D liés au joueur.
 */

void CPlayer::display() const
{
    // Affichage de l'objet tenu en main
    //...

    TVector3F iPoint;
    TFace iFace;
    TVector3I iCube;

    if (m_world->findIntersection(getEyesPosition(), m_direction - getEyesPosition(), iPoint, iFace, iCube))
    {
        // Affichage du cube pointé

        glPushAttrib(GL_ALL_ATTRIB_BITS);
        glPushMatrix();

        glDisable(GL_LIGHTING);
        glDisable(GL_TEXTURE_2D);
        glEnable(GL_LINE_SMOOTH);
        glLineWidth(1);
        glTranslatef(iCube.X * CellSize, iCube.Y * CellSize, iCube.Z * CellSize);
        glColor3ub(0, 0, 0); // Noir

        const int s = CellSize;

        // Boite englobant le bloc
        glBegin(GL_LINES);

            glVertex3f(0.0f, 0.0f, 0.0f);
            glVertex3f(s, 0.0f, 0.0f);

            glVertex3f(s, 0.0f, 0.0f);
            glVertex3f(s, s, 0.0f);

            glVertex3f(s, s, 0.0f);
            glVertex3f(0.0f, s, 0.0f);

            glVertex3f(0.0f, s, 0.0f);
            glVertex3f(0.0f, 0.0f, 0.0f);

            glVertex3f(0.0f, 0.0f, s);
            glVertex3f(s, 0.0f, s);

            glVertex3f(s, 0.0f, s);
            glVertex3f(s, s, s);

            glVertex3f(s, s, s);
            glVertex3f(0.0f, s, s);

            glVertex3f(0.0f, s, s);
            glVertex3f(0.0f, 0.0f, s);

            glVertex3f(0.0f, 0.0f, 0.0f);
            glVertex3f(0.0f, 0.0f, s);

            glVertex3f(s, 0.0f, 0.0f);
            glVertex3f(s, 0.0f, s);

            glVertex3f(0.0f, s, 0.0f);
            glVertex3f(0.0f, s, s);

            glVertex3f(s, s, 0.0f);
            glVertex3f(s, s, s);

        glEnd();

        glPopMatrix();
        glPopAttrib();
    }
}


/**
 * Affiche le HUD du joueur.
 *
 * \param width  Largeur de la fenêtre.
 * \param height Hauteur de la fenêtre.
 */

void CPlayer::displayHUD() const
{
    int width = m_game->getWindowWidth();
    int height = m_game->getWindowHeight();

    // Réticule au centre de la fenêtre
    glEnable(GL_LINE_SMOOTH);
    glLineWidth(2);

    glBegin(GL_LINES);

        glColor3ub(255, 255, 255);

        glVertex2i(width / 2 - 10, height / 2);
        glVertex2i(width / 2 + 10, height / 2);

        glVertex2i(width / 2, height / 2 - 10);
        glVertex2i(width / 2, height / 2 + 10);

    glEnd();


    const CFont * font = m_game->getFont();


    // Informations de Debug
    font->displayText(CString("position = %1 x %2 x %3").arg(m_position.X).arg(m_position.Y).arg(m_position.Z), 10, 10, CColor::White);
    font->displayText(CString("angles = %1 %2").arg(m_theta).arg(m_phi), 10, 20, CColor::White);
    font->displayText(CString("chunk = %1 x %2").arg((int) floor(m_position.X / ChunkSize)).arg((int) floor(m_position.Y / ChunkSize)), 10, 30, CColor::White);

    font->displayText(CString("height = %1").arg(m_world->getHeight(m_position.X, m_position.Y)), 10, 40, CColor::White);
    font->displayText(CString("temperature = %1").arg(m_world->getTemperature(m_position.X, m_position.Y)), 10, 50, CColor::White);
    font->displayText(CString("humidity = %1").arg(m_world->getHumidity(m_position.X, m_position.Y)), 10, 60, CColor::White);
    font->displayText(CString("biome = %1").arg(getBiomeName(m_world->getBiome(m_position.X, m_position.Y))), 10, 70, CColor::White);


    const int ItemSize = InventoryItemSize;
    const int ItemSpacing = 8;
    const int InventoryMargin = 8;

    const int bottom = height - InventoryMargin - ItemSize;
    const int left = (width - InventoryCols * ItemSize - (InventoryCols - 1) * ItemSpacing) / 2;

    displayLifeBar(left - InventoryMargin, bottom - InventoryMargin - 10);
    displayFoodBar(left + InventoryMargin + InventoryCols * ItemSize + (InventoryCols - 1) * ItemSpacing - HUD_FoodBarWidth, bottom - InventoryMargin - 10);


    // Affichage de l'inventaire
    if (m_game->getMode() == CGame::ModeInventory)
    {
        // Fond gris transparent
        glBegin(GL_TRIANGLES);

            glColor4ub(0, 0, 0, 150);

            glVertex2i(0, 0);
            glVertex2i(width, 0);
            glVertex2i(0, height);

            glVertex2i(0, height);
            glVertex2i(width, 0);
            glVertex2i(width, height);

        glEnd();

        displayInventory(width, height);
    }

    // Fond transparent
    glBegin(GL_TRIANGLES);

        glColor4ub(255, 255, 255, 64);

        glVertex2i(left - InventoryMargin, bottom - InventoryMargin);
        glVertex2i(left + InventoryCols * ItemSize + (InventoryCols - 1) * ItemSpacing + InventoryMargin, bottom - InventoryMargin);
        glVertex2i(left - InventoryMargin, bottom + ItemSize + InventoryMargin);

        glVertex2i(left - InventoryMargin, bottom + ItemSize + InventoryMargin);
        glVertex2i(left + InventoryCols * ItemSize + (InventoryCols - 1) * ItemSpacing + InventoryMargin, bottom - InventoryMargin);
        glVertex2i(left + InventoryCols * ItemSize + (InventoryCols - 1) * ItemSpacing + InventoryMargin, bottom + ItemSize + InventoryMargin);

    glEnd();

    // Affichage des objets de la barre d'inventaire
    for (int i = 0; i < InventoryCols; ++i)
    {
        int x = left + i * (ItemSize + ItemSpacing);
        int y = bottom;

        // Élément sélectionné
        if (m_currentItem == i)
        {
            glBegin(GL_TRIANGLES);

                glColor3ub(0, 0, 0);

                glVertex2i(x - 4, y - 4);
                glVertex2i(x + ItemSize + 4, y - 4);
                glVertex2i(x - 4, y + ItemSize + 4);

                glVertex2i(x - 4, y + ItemSize + 4);
                glVertex2i(x + ItemSize + 4, y - 4);
                glVertex2i(x + ItemSize + 4, y + ItemSize + 4);

            glEnd();
        }

        int content;
        int count;
        int data;

        getInventoryItem(i, content, count, data);
        displayContainerSlot(m_world, m_game->getFont(), x, y, content, count, data);
    }
}


/**
 * Affichage d'un fond bleu si le joueur est sous l'eau et un fond
 * orange si le joueur est sous la lave.
 */

void CPlayer::displayFluid() const
{
    int width = m_game->getWindowWidth();
    int height = m_game->getWindowHeight();

    // Le joueur est sous l'eau
    if (m_world->isUnderWater(getEyesPosition()))
    {
        // Fond bleu transparent
        glBegin(GL_TRIANGLES);

            glColor4ub(0, 0, 255, 64);

            glVertex2i(0, 0);
            glVertex2i(width, 0);
            glVertex2i(0, height);

            glVertex2i(0, height);
            glVertex2i(width, 0);
            glVertex2i(width, height);

        glEnd();
    }
    // Le joueur est sous la lave
    else if (m_world->isUnderLava(getEyesPosition()))
    {
        // Fond bleu transparent
        glBegin(GL_TRIANGLES);

            glColor4ub(255, 128, 0, 128);

            glVertex2i(0, 0);
            glVertex2i(width, 0);
            glVertex2i(0, height);

            glVertex2i(0, height);
            glVertex2i(width, 0);
            glVertex2i(width, height);

        glEnd();
    }
}


/**
 * Retourne le nombre de points de vie du joueur.
 */

float CPlayer::getLife() const
{
    return m_life;
}


/**
 * Modifie le nombre de points de vie du joueur.
 */

void CPlayer::setLife(float life)
{
    if (life < 0.0f)
    {
        m_life = 0.0f;
    }
    else if (life > 100.0f)
    {
        m_life = 100.0f;
    }
    else
    {
        m_life = life;
    }
}


void CPlayer::addLife(float add)
{
    setLife(m_life + add);
}


void CPlayer::eatFood(float food, float saturation)
{
    m_foodLevel += food;
    m_foodSaturationLevel += saturation;
    m_foodExhaustionLevel = 0.0f;

    if (m_foodLevel > 100.0f)
    {
        m_foodLevel = 100.0f;
    }
    else if (m_foodLevel < 0.0f)
    {
        m_foodLevel = 0.0f;
    }

    if (m_foodSaturationLevel > m_foodLevel)
    {
        m_foodSaturationLevel = m_foodLevel;
    }
    else if (m_foodSaturationLevel < 0.0f)
    {
        m_foodSaturationLevel = 0.0f;
        m_foodLevel -= 5.0f;

        if (m_foodLevel < 0.0f)
        {
            m_foodLevel = 0.0f;
        }
    }
}


void CPlayer::increaseExhaustion(float points)
{
    m_foodExhaustionLevel += points;

    while (m_foodExhaustionLevel >= 100.0f)
    {
        m_foodExhaustionLevel -= 100.0f;

        if (m_foodSaturationLevel <= 0.0f)
        {
            m_foodSaturationLevel = 0.0f;
            m_foodLevel -= 5.0f;

            if (m_foodLevel < 0.0f)
            {
                m_foodLevel = 0.0f;
            }
        }
        else
        {
            m_foodSaturationLevel -= 5.0f;

            if (m_foodSaturationLevel <= 0)
            {
                m_foodSaturationLevel = 0.0f;
            }
        }
    }
}


/**
 * Affiche la fenêtre d'inventaire du joueur.
 *
 * \param width  Largeur de la fenêtre.
 * \param height Hauteur de la fenêtre.
 */

void CPlayer::displayInventory(int width, int height) const
{
    const int InventoryWindowWidth = InventoryCols * InventoryItemSize + (InventoryCols - 1) * ItemSpacing + 2 * WindowMargin;
    const int InventoryWindowHeight = InventoryRows * InventoryItemSize + (InventoryRows - 1) * ItemSpacing + 2 * WindowMargin +
                                      SpaceSeparating + SpaceSeparating + 4 * InventoryItemSize + 4 * ItemSpacing;

    const int windowLeft = (width - InventoryWindowWidth) / 2;
    const int windowTop = (height - InventoryWindowHeight) / 2;

    // Fond de la fenêtre
    glBegin(GL_TRIANGLES);

        glColor3ub(0, 0, 0);

        glVertex2i(windowLeft - 1, windowTop - 1);
        glVertex2i(windowLeft + InventoryWindowWidth + 1, windowTop - 1);
        glVertex2i(windowLeft - 1, windowTop + InventoryWindowHeight + 1);

        glVertex2i(windowLeft - 1, windowTop + InventoryWindowHeight + 1);
        glVertex2i(windowLeft + InventoryWindowWidth + 1, windowTop - 1);
        glVertex2i(windowLeft + InventoryWindowWidth + 1, windowTop + InventoryWindowHeight + 1);

        glColor3ub(198, 198, 198);

        glVertex2i(windowLeft, windowTop);
        glVertex2i(windowLeft + InventoryWindowWidth, windowTop);
        glVertex2i(windowLeft, windowTop + InventoryWindowHeight);

        glVertex2i(windowLeft, windowTop + InventoryWindowHeight);
        glVertex2i(windowLeft + InventoryWindowWidth, windowTop);
        glVertex2i(windowLeft + InventoryWindowWidth, windowTop + InventoryWindowHeight);

    glEnd();


    // Armure
    for (int a = 0; a < 4; ++a)
    {
        displayContainerSlot(m_world,
                             m_game->getFont(),
                             windowLeft + WindowMargin,
                             windowTop + WindowMargin + a * (InventoryItemSize + ItemSpacing),
                             m_armor[a].type,
                             m_armor[a].count,
                             m_armor[a].data);
    }


    // Vue 3D du joueur
    glBegin(GL_TRIANGLES);

        glColor3ub(139, 139, 139);

        glTexCoord2i(0, 0); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 4 - 2,
                                       windowTop + WindowMargin - 2);
        glTexCoord2i(1, 0); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 2 * 54 + 2,
                                       windowTop + WindowMargin - 2);
        glTexCoord2i(0, 1); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 4 - 2,
                                       windowTop + WindowMargin + 4 * InventoryItemSize + 3 * ItemSpacing + 2);

        glTexCoord2i(0, 1); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 4 - 2,
                                       windowTop + WindowMargin + 4 * InventoryItemSize + 3 * ItemSpacing + 2);
        glTexCoord2i(1, 0); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 2 * 54 + 2,
                                       windowTop + WindowMargin - 2);
        glTexCoord2i(1, 1); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 2 * 54 + 2,
                                       windowTop + WindowMargin + 4 * InventoryItemSize + 3 * ItemSpacing + 2);

        glColor3ub(55, 55, 55);

        glTexCoord2i(0, 0); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 4 - 2,
                                       windowTop + WindowMargin - 2);
        glTexCoord2i(1, 0); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 2 * 54,
                                       windowTop + WindowMargin - 2);
        glTexCoord2i(0, 1); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 4 - 2,
                                       windowTop + WindowMargin + 4 * InventoryItemSize + 3 * ItemSpacing);

        glTexCoord2i(0, 1); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 4 - 2,
                                       windowTop + WindowMargin + 4 * InventoryItemSize + 3 * ItemSpacing);
        glTexCoord2i(1, 0); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 2 * 54,
                                       windowTop + WindowMargin - 2);
        glTexCoord2i(1, 1); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 2 * 54,
                                       windowTop + WindowMargin + 4 * InventoryItemSize + 3 * ItemSpacing);

        glColor3ub(255, 255, 255);

        glTexCoord2i(0, 0); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 4,
                                       windowTop + WindowMargin);
        glTexCoord2i(1, 0); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 2 * 54 + 2,
                                       windowTop + WindowMargin);
        glTexCoord2i(0, 1); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 4,
                                       windowTop + WindowMargin + 4 * InventoryItemSize + 3 * ItemSpacing + 2);

        glTexCoord2i(0, 1); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 4,
                                       windowTop + WindowMargin + 4 * InventoryItemSize + 3 * ItemSpacing + 2);
        glTexCoord2i(1, 0); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 2 * 54 + 2,
                                       windowTop + WindowMargin);
        glTexCoord2i(1, 1); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 2 * 54 + 2,
                                       windowTop + WindowMargin + 4 * InventoryItemSize + 3 * ItemSpacing + 2);

        glColor3ub(0, 0, 0);

        glTexCoord2i(0, 0); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 4,
                                       windowTop + WindowMargin);
        glTexCoord2i(1, 0); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 2 * 54,
                                       windowTop + WindowMargin);
        glTexCoord2i(0, 1); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 4,
                                       windowTop + WindowMargin + 4 * InventoryItemSize + 3 * ItemSpacing);

        glTexCoord2i(0, 1); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 4,
                                       windowTop + WindowMargin + 4 * InventoryItemSize + 3 * ItemSpacing);
        glTexCoord2i(1, 0); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 2 * 54,
                                       windowTop + WindowMargin);
        glTexCoord2i(1, 1); glVertex2i(windowLeft + WindowMargin + InventoryItemSize + 2 * 54,
                                       windowTop + WindowMargin + 4 * InventoryItemSize + 3 * ItemSpacing);

    glEnd();

    //...


    // Crafting
    displayContainerSlot(m_world, m_game->getFont(), windowLeft + WindowMargin + InventoryItemSize + 2 * 54 + 20,
                         windowTop + WindowMargin + 1 * (InventoryItemSize + ItemSpacing),
                         m_crafting[0].type, m_crafting[0].count, m_crafting[0].data);
    displayContainerSlot(m_world, m_game->getFont(), windowLeft + WindowMargin + InventoryItemSize + 2 * 54 + 20,
                         windowTop + WindowMargin + 2 * (InventoryItemSize + ItemSpacing),
                         m_crafting[1].type, m_crafting[1].count, m_crafting[1].data);
    displayContainerSlot(m_world, m_game->getFont(), windowLeft + WindowMargin + InventoryItemSize + 2 * 54 + 20 + InventoryItemSize + ItemSpacing,
                         windowTop + WindowMargin + 1 * (InventoryItemSize + ItemSpacing),
                         m_crafting[2].type, m_crafting[2].count, m_crafting[2].data);
    displayContainerSlot(m_world, m_game->getFont(), windowLeft + WindowMargin + InventoryItemSize + 2 * 54 + 20 + InventoryItemSize + ItemSpacing,
                         windowTop + WindowMargin + 2 * (InventoryItemSize + ItemSpacing),
                         m_crafting[3].type, m_crafting[3].count, m_crafting[3].data);
    displayContainerSlot(m_world, m_game->getFont(), windowLeft + WindowMargin + InventoryItemSize + 2 * 54 + 20 + 2 * (InventoryItemSize + ItemSpacing) + 44,
                         windowTop + WindowMargin + 1 * (InventoryItemSize + ItemSpacing) + 20,
                         m_craftingResult.type, m_craftingResult.count, m_craftingResult.data);

    // DEBUG
    displayContainerSlot(m_world, m_game->getFont(), 100, 20,
                         m_selectedObject.type, m_selectedObject.count, m_selectedObject.data);
    //...


    // Objets de l'inventaire
    for (int row = 0; row < InventoryRows; ++row)
    {
        int y = windowTop + WindowMargin + 4 * InventoryItemSize + 4 * ItemSpacing + SpaceSeparating;

        if (row == 0)
        {
            y += (InventoryRows - 1) * (InventoryItemSize + ItemSpacing) + SpaceSeparating;
        }
        else
        {
            y += (row - 1) * (InventoryItemSize + ItemSpacing);
        }

        for (int col = 0; col < InventoryCols; ++col)
        {
            int x = windowLeft + WindowMargin + col * (InventoryItemSize + ItemSpacing);

            int content;
            int count;
            int data;

            getInventoryItem(row * InventoryCols + col, content, count, data);
            displayContainerSlot(m_world, m_game->getFont(), x, y, content, count, data);
        }
    }
}


/**
 * Affiche la barre de vie du joueur.
 */

void CPlayer::displayLifeBar(int left, int top) const
{
    // Fond de la barre
    glBegin(GL_TRIANGLES);

        glColor4ub(128, 128, 128, 128);

        glVertex2i(left, top - HUD_LifeBarHeight);
        glVertex2i(left + HUD_LifeBarWidth, top - HUD_LifeBarHeight);
        glVertex2i(left, top);

        glVertex2i(left, top);
        glVertex2i(left + HUD_LifeBarWidth, top - HUD_LifeBarHeight);
        glVertex2i(left + HUD_LifeBarWidth, top);

    glEnd();

    // Barre rouge
    glBegin(GL_TRIANGLES);

        glColor4ub(255, 0, 0, 128);

        glVertex2i(left, top - HUD_LifeBarHeight);
        glVertex2i(left + HUD_LifeBarWidth * m_life / 100.0f, top - HUD_LifeBarHeight);
        glVertex2i(left, top);

        glVertex2i(left, top);
        glVertex2i(left + HUD_LifeBarWidth * m_life / 100.0f, top - HUD_LifeBarHeight);
        glVertex2i(left + HUD_LifeBarWidth * m_life / 100.0f, top);

    glEnd();
}


/**
 * Affiche la barre de faim du joueur.
 */

void CPlayer::displayFoodBar(int left, int top) const
{
    // Fond de la barre
    glBegin(GL_TRIANGLES);

        glColor4ub(128, 128, 128, 128);

        glVertex2i(left, top - HUD_FoodBarHeight);
        glVertex2i(left + HUD_FoodBarWidth, top - HUD_FoodBarHeight);
        glVertex2i(left, top);

        glVertex2i(left, top);
        glVertex2i(left + HUD_FoodBarWidth, top - HUD_FoodBarHeight);
        glVertex2i(left + HUD_FoodBarWidth, top);

    glEnd();

    // Barre rouge
    glBegin(GL_TRIANGLES);

        glColor4ub(128, 128, 0, 128);

        glVertex2i(left, top - HUD_FoodBarHeight);
        glVertex2i(left + HUD_FoodBarWidth * m_life / 100.0f, top - HUD_FoodBarHeight);
        glVertex2i(left, top);

        glVertex2i(left, top);
        glVertex2i(left + HUD_FoodBarWidth * m_life / 100.0f, top - HUD_FoodBarHeight);
        glVertex2i(left + HUD_FoodBarWidth * m_life / 100.0f, top);

    glEnd();
}


/**
 * Ajoute des objets dans l'inventaire.
 *
 * \todo Retourner le nombre d'objets restants.
 *
 * \param content Objet à ajouter.
 * \param count   Nombre d'objets à ajouter.
 * \param data    Données supplémentaires.
 * \return True si tous les objets ont pu être ajoutés, false sinon.
 */

bool CPlayer::addInventoryItem(int content, int count, int data)
{
    if (count <= 0 || content == ContentEmpty)
    {
        return true;
    }

    int maxItemsPerSlot = InventoryMaxItem;
/*
    if (outil)
        maxItemsPerSlot = 1;
*/

    // Recherche d'un objet similaire dans l'inventaire
    if (maxItemsPerSlot > 1)
    {
        for (int i = 0; i < InventoryRows * InventoryCols; ++i)
        {
            if (m_slots[i].type == content && m_slots[i].data == data && m_slots[i].count < maxItemsPerSlot)
            {
                if (m_slots[i].count + count <= maxItemsPerSlot)
                {
                    m_slots[i].count += count;
                    return true;
                }

                count -= (maxItemsPerSlot - m_slots[i].count);
                m_slots[i].count = maxItemsPerSlot;
            }
        }
    }

    // Recherche d'une case vide
    if (count > 0)
    {
        for (int i = 0; i < InventoryRows * InventoryCols; ++i)
        {
            if (m_slots[i].type == ContentEmpty)
            {
                m_slots[i].type = content;
                m_slots[i].data = data;

                if (count <= maxItemsPerSlot)
                {
                    m_slots[i].count = count;
                    return true;
                }

                count -= maxItemsPerSlot;
                m_slots[i].count = maxItemsPerSlot;
            }
        }
    }

    return (count <= 0);
}


/**
 * Enlève un objet de l'inventaire.
 *
 * \param position Position de l'objet à enlever.
 * \param count    Nombre d'objets à enlever.
 */

void CPlayer::removeInventoryItem(int position, int count)
{
    std::cout << "removeInventoryItem " << position << ", count = " << count << std::endl;

    if (count <= 0 || count > InventoryMaxItem)
    {
        std::cerr << "  count pas bon" << std::endl;
        return;
    }

    if (position < 0 || position >= InventoryRows * InventoryCols)
    {
        std::cerr << "  position pas bonne" << std::endl;
        return;
    }

    // Rien à enlever
    if (m_slots[position].type == ContentEmpty)
    {
        std::cout << "  content = Empty" << std::endl;
        return;
    }

    if (m_slots[position].count <= count)
    {
        std::cout << "  on enleve tout" << std::endl;
        m_slots[position].count = 0;
        m_slots[position].type = ContentEmpty;
        m_slots[position].data = 0;
        return;
    }

    std::cout << "  OK !   ancien count = " << m_slots[position].count << std::endl;
    m_slots[position].count -= count;
}


// count contient le nombre d'éléments à ajouter, et sera remplacé par le nombre d'éléments restant
bool CPlayer::setInventoryItem(int position, int content, int& count, int data)
{
    std::cout << "setInventoryItem " << position << ", content = " << content << ", count = " << count << std::endl;

    if (position < 0 || position >= InventoryRows * InventoryCols)
    {
        std::cerr << "  position pas bon" << std::endl;
        return false;
    }

    if (count <= 0 || count > InventoryMaxItem)
    {
        std::cerr << "  count pas bon" << std::endl;
        return false;
    }

    int type = m_slots[position].type;
    if (type != ContentEmpty && type != content)
    {
        std::cerr << "  pas le meme type" << std::endl;
        return false;
    }

    if (m_slots[position].data != data)
    {
        std::cerr << "  pas les memes data" << std::endl;
        return false;
    }

    if (m_slots[position].count == InventoryMaxItem)
    {
        std::cerr << "  plus de place disponible" << std::endl;
        return false;
    }

    if (m_slots[position].count + count <= InventoryMaxItem)
    {
        m_slots[position].count += count;
        count = 0;
    }
    else
    {
        count -= (InventoryMaxItem - m_slots[position].count);
        m_slots[position].count = InventoryMaxItem;
    }

    return true;
}


/**
 * Récupère un objet dans l'inventaire.
 *
 * \param position Position dans l'inventaire (entre 0 et 35).
 * \param type     Type d'objet présent à cet emplacement.
 * \param count    Nombre d'objets dans le emplacement.
 * \param data     Données supplémentaires des objets.
 * \return True si un objet est présent, false sinon.
 */

bool CPlayer::getInventoryItem(int position, int& type, int& count, int& data) const
{
    if (position < 0 || position >= InventoryRows * InventoryCols)
    {
        type = ContentEmpty;
        count = 0;
        data = 0;
        return false;
    }

    type = m_slots[position].type;

    if (type != ContentEmpty)
    {
        count = m_slots[position].count;
        data = m_slots[position].data;
        return true;
    }
    else
    {
        count = 0;
        data = 0;
        return false;
    }
}


/**
 * Récupère un objet dans l'inventaire.
 *
 * \param position Position dans l'inventaire (entre 0 et 35).
 * \return Type d'objet présent à cet emplacement.
 */

int CPlayer::getInventoryItem(int position) const
{
    if (position < 0 || position >= InventoryRows * InventoryCols)
    {
        return ContentEmpty;
    }

    return m_slots[position].type;
}


/**
 * Change l'objet sélectionné dans la barre d'inventaire.
 * Si \a currentItem est inférieur à 0, le dernier objet est sélectionné, si
 * \a currentItem est supérieur ou égal au nombre d'objet dans la barre
 * d'inventaire, le premier objet est sélectionné.
 *
 * \param currentItem Numéro de l'objet sélectionné.
 */

void CPlayer::setCurrentItem(int currentItem)
{
    if (currentItem < 0)
    {
        m_currentItem = InventoryCols - 1;
    }
    else if (currentItem >= InventoryCols)
    {
        m_currentItem = 0;
    }
    else
    {
        m_currentItem = currentItem;
    }
}


/**
 * Charge les informations du joueur depuis un dossier de sauvegarde.
 * Le dossier de sauvegarde est de la forme "WorldName/players/".
 *
 * \todo Charger l'inventaire
 *
 * \param folder Nom du dossier de sauvegarde.
 * \return True si le chargement s'est bien passé, false sinon.
 */

bool CPlayer::loadPlayer(const Ted::CString& folder)
{
    std::cout << "Chargement du joueur " << m_name << std::endl;

    CString fileName = folder + "/" + m_name + ".dat";
    std::ifstream file(fileName.toCharArray(), std::ios::in | std::ios::binary);

    if (!file)
    {
        std::cerr << "CPlayer::loadPlayer : impossible d'ouvrir le fichier " << fileName << std::endl;
        return false;
    }

    file.seekg(0, std::ios_base::end);
    std::size_t size = file.tellg();

    if (size < sizeof(THeader))
    {
        file.close();
        std::cerr << "CPlayer::loadPlayer : le fichier " << fileName << " a une taille trop petite" << std::endl;
        return nullptr;
    }

    file.seekg(0, std::ios_base::beg);

    THeader header;
    file.read(reinterpret_cast<char *>(&header), sizeof(THeader));

    if (header.ident != ('T' + ('M'<<8) + ('C'<<16) + ('M'<<24)) || header.version != 1)
    {
        std::cerr << "CPlayer::loadPlayer : le fichier " << fileName << " a un format incorrect" << std::endl;
        return false;
    }

    m_name = header.name;
    setPosition(header.position);
    setDirection(header.direction);
    setLife(header.life);
    m_foodLevel = header.foodLevel;
    m_foodSaturationLevel = header.foodSaturationLevel;
    m_foodExhaustionLevel = header.foodExhaustionLevel;
    setCurrentItem(header.currentItem);

    file.close();

    return true;
}


/**
 * Sauvegarde les informations du joueur.
 */

bool CPlayer::savePlayer(const Ted::CString& folder) const
{
    std::cout << "Enregistrement du joueur " << m_name << std::endl;

    CString fileName = folder + "/" + m_name + ".dat";
    std::ofstream file(fileName.toCharArray(), std::ios::out | std::ios::binary);

    if (!file)
    {
        std::cerr << "CPlayer::savePlayer : impossible d'ouvrir le fichier " << fileName << std::endl;
        return false;
    }


    THeader header;
    header.ident               = ('T' + ('M'<<8) + ('C'<<16) + ('P'<<24));
    header.version             = 1;
    strncpy(header.name, m_name.toCharArray(), 64);
    header.position            = m_position;
    header.direction           = m_direction;
    header.life                = m_life;
    header.foodLevel           = m_foodLevel;
    header.foodSaturationLevel = m_foodSaturationLevel;
    header.foodExhaustionLevel = m_foodExhaustionLevel;
    header.currentItem         = m_currentItem;

    for (int i = 0; i < InventoryRows * InventoryCols; ++i)
    {
        header.objects[i] = m_slots[i];
    }

    for (int i = 0; i < 4; ++i)
    {
        header.armor[i] = m_armor[i];
        header.crafting[i] = m_crafting[i];
    }

    header.craftingResult = m_craftingResult;
    header.selectedObject = m_selectedObject;

    file.write(reinterpret_cast<const char *>(&header), sizeof(THeader));

    if (file.fail())
    {
        std::cerr << "CPlayer::savePlayer : erreur inconnue" << std::endl;
        file.close();
        return false;
    }

    file.close();

    return true;
}


void CPlayer::clickOnInventory(int x, int y, sf::Mouse::Button button)
{
    int width = m_game->getWindowWidth();
    int height = m_game->getWindowHeight();

    const int InventoryWindowWidth = InventoryCols * InventoryItemSize + (InventoryCols - 1) * ItemSpacing + 2 * WindowMargin;
    const int InventoryWindowHeight = InventoryRows * InventoryItemSize + (InventoryRows - 1) * ItemSpacing + 2 * WindowMargin +
                                      SpaceSeparating + SpaceSeparating + 4 * InventoryItemSize + 4 * ItemSpacing;

    const int windowLeft = (width - InventoryWindowWidth) / 2;
    const int windowTop = (height - InventoryWindowHeight) / 2;

    if (x <= windowLeft || x >= windowLeft + InventoryWindowWidth ||
        y <= windowTop || y >= windowTop + InventoryWindowHeight)
    {
        return;
    }

    // Armure
    if (button == sf::Mouse::Left)
    {
        if (x >= windowLeft + WindowMargin && x <= windowLeft + WindowMargin + InventoryItemSize)
        {
            for (int a = 0; a < 4; ++a)
            {
                if (y >= windowTop + WindowMargin + a * (InventoryItemSize + ItemSpacing) &&
                    y <= windowTop + WindowMargin + a * (InventoryItemSize + ItemSpacing) + InventoryItemSize)
                {
                    std::cout << "Clic sur la case ARMURE-" << a << std::endl;
                    if (m_selectedObject.type == ContentEmpty)
                    {
                        m_selectedObject = m_armor[a];
                        m_armor[a] = CInventorySlot();
                    }
                    else
                    {
                        //TODO: vérifier que l'objet selected correspond à la pièce d'armure
                        //...
                    }

                    return;
                }
            }
        }
    }

    // Crafting
    if (x >= windowLeft + WindowMargin + InventoryItemSize + 2 * 54 + 20 &&
        x <= windowLeft + WindowMargin + InventoryItemSize + 2 * 54 + 20 + InventoryItemSize &&
        y >= windowTop + WindowMargin + 1 * (InventoryItemSize + ItemSpacing) &&
        y <= windowTop + WindowMargin + 1 * (InventoryItemSize + ItemSpacing) + InventoryItemSize)
    {
        std::cout << "Clic sur la case CRAFT-1" << std::endl;
        if (button == sf::Mouse::Left)
        {
            actionInventorySlotsMouse1(&m_selectedObject, &m_crafting[0]);
            updateInventoryCraft();
        }
        else if (button == sf::Mouse::Right)
        {
            actionInventorySlotsMouse2(&m_selectedObject, &m_crafting[0]);
            updateInventoryCraft();
        }

        return;
    }

    if (x >= windowLeft + WindowMargin + InventoryItemSize + 2 * 54 + 20 &&
        x <= windowLeft + WindowMargin + InventoryItemSize + 2 * 54 + 20 + InventoryItemSize &&
        y >= windowTop + WindowMargin + 2 * (InventoryItemSize + ItemSpacing) &&
        y <= windowTop + WindowMargin + 2 * (InventoryItemSize + ItemSpacing) + InventoryItemSize)
    {
        std::cout << "Clic sur la case CRAFT-2" << std::endl;
        if (button == sf::Mouse::Left)
        {
            actionInventorySlotsMouse1(&m_selectedObject, &m_crafting[1]);
            updateInventoryCraft();
        }
        else if (button == sf::Mouse::Right)
        {
            actionInventorySlotsMouse2(&m_selectedObject, &m_crafting[1]);
            updateInventoryCraft();
        }

        return;
    }

    if (x >= windowLeft + WindowMargin + InventoryItemSize + 2 * 54 + 20 + InventoryItemSize + ItemSpacing &&
        x <= windowLeft + WindowMargin + InventoryItemSize + 2 * 54 + 20 + InventoryItemSize + ItemSpacing + InventoryItemSize &&
        y >= windowTop + WindowMargin + 1 * (InventoryItemSize + ItemSpacing) &&
        y <= windowTop + WindowMargin + 1 * (InventoryItemSize + ItemSpacing) + InventoryItemSize)
    {
        std::cout << "Clic sur la case CRAFT-3" << std::endl;
        if (button == sf::Mouse::Left)
        {
            actionInventorySlotsMouse1(&m_selectedObject, &m_crafting[2]);
            updateInventoryCraft();
        }
        else if (button == sf::Mouse::Right)
        {
            actionInventorySlotsMouse2(&m_selectedObject, &m_crafting[2]);
            updateInventoryCraft();
        }

        return;
    }

    if (x >= windowLeft + WindowMargin + InventoryItemSize + 2 * 54 + 20 + InventoryItemSize + ItemSpacing &&
        x <= windowLeft + WindowMargin + InventoryItemSize + 2 * 54 + 20 + InventoryItemSize + ItemSpacing + InventoryItemSize &&
        y >= windowTop + WindowMargin + 2 * (InventoryItemSize + ItemSpacing) &&
        y <= windowTop + WindowMargin + 2 * (InventoryItemSize + ItemSpacing) + InventoryItemSize)
    {
        std::cout << "Clic sur la case CRAFT-4" << std::endl;
        if (button == sf::Mouse::Left)
        {
            actionInventorySlotsMouse1(&m_selectedObject, &m_crafting[3]);
            updateInventoryCraft();
        }
        else if (button == sf::Mouse::Right)
        {
            actionInventorySlotsMouse2(&m_selectedObject, &m_crafting[3]);
            updateInventoryCraft();
        }

        return;
    }

    if (x >= windowLeft + WindowMargin + InventoryItemSize + 2 * 54 + 20 + 2 * (InventoryItemSize + ItemSpacing) + 44 &&
        x <= windowLeft + WindowMargin + InventoryItemSize + 2 * 54 + 20 + 2 * (InventoryItemSize + ItemSpacing) + 44 + InventoryItemSize &&
        y >= windowTop + WindowMargin + 2 * (InventoryItemSize + ItemSpacing) &&
        y <= windowTop + WindowMargin + 2 * (InventoryItemSize + ItemSpacing) + InventoryItemSize)
    {
        std::cout << "Clic sur la case CRAFT-5" << std::endl;
        if (m_selectedObject.type == ContentEmpty)
        {
            if (button == sf::Mouse::Left)
            {
                m_selectedObject = m_craftingResult;
                m_craftingResult = CInventorySlot();

                for (int c = 0; c < 4; ++c)
                {
                    if (m_crafting[c].count - m_selectedObject.count > 0)
                    {
                        m_crafting[c].count -= m_selectedObject.count;
                    }
                    else
                    {
                        m_crafting[c] = CInventorySlot();
                    }
                }

                updateInventoryCraft();
            }
            else if (button == sf::Mouse::Right)
            {
                actionInventorySlotsMouse2(&m_selectedObject, &m_craftingResult);

                for (int c = 0; c < 4; ++c)
                {
                    if (m_crafting[c].count - m_selectedObject.count > 0)
                    {
                        m_crafting[c].count -= m_selectedObject.count;
                    }
                    else
                    {
                        m_crafting[c] = CInventorySlot();
                    }
                }

                updateInventoryCraft();
            }
        }

        return;
    }

    // Objets de l'inventaire
    for (int row = 0; row < InventoryRows; ++row)
    {
        int cell_y = windowTop + WindowMargin + 4 * InventoryItemSize + 4 * ItemSpacing + SpaceSeparating;

        if (row == 0)
        {
            cell_y += (InventoryRows - 1) * (InventoryItemSize + ItemSpacing) + SpaceSeparating;
        }
        else
        {
            cell_y += (row - 1) * (InventoryItemSize + ItemSpacing);
        }

        if (y >= cell_y && y <= cell_y + InventoryItemSize)
        {
            for (int col = 0; col < InventoryCols; ++col)
            {
                int cell_x = windowLeft + WindowMargin + col * (InventoryItemSize + ItemSpacing);

                if (x >= cell_x && x <= cell_x + InventoryItemSize)
                {
                    std::cout << "Clic sur la case OBJECT-" << row << "-" << col << std::endl;
                    int slotPosition = row * InventoryCols + col;

                    if (button == sf::Mouse::Left)
                    {
                        actionInventorySlotsMouse1(&m_selectedObject, &m_slots[slotPosition]);
                    }
                    else if (button == sf::Mouse::Right)
                    {
                        actionInventorySlotsMouse2(&m_selectedObject, &m_slots[slotPosition]);
                    }

                    return;
                }
            }
        }
    }
}


void CPlayer::actionInventorySlotsMouse1(CInventorySlot * source, CInventorySlot * dest)
{
    if (source == nullptr || dest == nullptr || source == dest)
    {
        return;
    }

    // Même type d'objet
    if (dest->type == source->type && dest->data == source->data)
    {
        if (dest->count + source->count <= InventoryMaxItem)
        {
            dest->count += source->count;
            *source = CInventorySlot();
        }
        else
        {
            source->count -= (InventoryMaxItem - dest->count);
            dest->count = InventoryMaxItem;
        }
    }
    else
    {
        CInventorySlot slot = *dest;
        *dest = *source;
        *source = slot;
    }
}


void CPlayer::actionInventorySlotsMouse2(CInventorySlot * source, CInventorySlot * dest)
{
    if (source == nullptr || dest == nullptr || source == dest)
    {
        return;
    }

    if (source->type == ContentEmpty && dest->type == ContentEmpty)
    {
        return;
    }

    // Même type d'objet
    if (dest->type == source->type && dest->data == source->data)
    {
        int c = source->count;

        if (source->count > 1)
        {
            c /= 2;
        }

        if (dest->count + c <= InventoryMaxItem)
        {
            dest->count += c;
            *source = CInventorySlot();
        }
        else
        {
            source->count -= (InventoryMaxItem - dest->count);
            dest->count = InventoryMaxItem;
        }
    }
    else if (source->type == ContentEmpty)
    {
        if (dest->count < 2)
        {
            *source = *dest;
            *dest = CInventorySlot();
        }
        else
        {
            int count = dest->count / 2;
            source->type = dest->type;
            source->data = dest->data;
            source->count = count;
            dest->count -= count;
        }
    }
    else if (dest->type == ContentEmpty)
    {
        dest->type = source->type;
        dest->data = source->data;
        dest->count = 1;

        if (source->count > 1)
        {
            --(source->count);
        }
        else
        {
            *source = CInventorySlot();
        }
    }
}


void CPlayer::updateInventoryCraft()
{
    m_craftingResult = CInventorySlot();

    // Etabli ou four
    if (m_crafting[0].type == m_crafting[1].type && m_crafting[1].type == m_crafting[2].type && m_crafting[2].type == m_crafting[3].type)
    {
        int c = std::min(m_crafting[0].count, std::min(m_crafting[1].count, std::min(m_crafting[2].count, m_crafting[3].count)));

        if (m_crafting[0].type == ContentWood)
        {
            //m_craftingResult.type = ContentEtabli;
            //m_craftingResult.data = 0;
            //m_craftingResult.count = c;
        }
        else if (m_crafting[0].type == ContentCobbleStone)
        {
            m_craftingResult.type = ContentFurnace;
            m_craftingResult.data = 0;
            m_craftingResult.count = c;
        }
    }

    // Bois -> Planches
    if ((m_crafting[0].type == ContentWood  && m_crafting[1].type == ContentEmpty && m_crafting[2].type == ContentEmpty && m_crafting[3].type == ContentEmpty) ||
        (m_crafting[0].type == ContentEmpty && m_crafting[1].type == ContentWood  && m_crafting[2].type == ContentEmpty && m_crafting[3].type == ContentEmpty) ||
        (m_crafting[0].type == ContentEmpty && m_crafting[1].type == ContentEmpty && m_crafting[2].type == ContentWood  && m_crafting[3].type == ContentEmpty) ||
        (m_crafting[0].type == ContentEmpty && m_crafting[1].type == ContentEmpty && m_crafting[2].type == ContentEmpty && m_crafting[3].type == ContentWood))
    {
        int c = std::max(m_crafting[0].count, std::max(m_crafting[1].count, std::max(m_crafting[2].count, m_crafting[3].count)));

        m_craftingResult.type = ContentWoodenPlank;
        m_craftingResult.data = 0;
        m_craftingResult.count = c;
    }

    //...
}
