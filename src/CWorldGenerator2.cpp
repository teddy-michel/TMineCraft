/*
Copyright (C) 2013-2015 Teddy Michel

This file is part of TMineCraft.

TMineCraft is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TMineCraft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TMineCraft. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CWorldGenerator2.hpp"
#include "CSector.hpp"


const int HeightMin = 32;
const int HeightMax = 128;


CWorldGenerator2::CWorldGenerator2(int seed) :
IWorldGenerator     (seed),
m_perlinDensity     (16, 6 * 0.0008f, 1, m_seed + 0),
m_perlinTemperature (16, 6 * 0.0004f, 1, m_seed + 1),
m_perlinHumidity    (16, 6 * 0.0004f, 1, m_seed + 2)
{

}


CWorldGenerator2::~CWorldGenerator2()
{

}


void CWorldGenerator2::buildChunk(int chunkMinX, int chunkMinY, CSector sectors[SectorsInChunk], uint8_t * heightMap)
{
    for (int x = 0; x < ChunkSize; ++x)
    {
        for (int y = 0; y < ChunkSize; ++y)
        {
            int height = 0;

            for (int sector = 0; sector < SectorsInChunk; ++sector)
            {
                for (int z = 0; z < ChunkSize; ++z)
                {
                    TCellContent content = ContentEmpty;

                    if (sector * ChunkSize + z <= 0)
                    {
                        content = ContentBedRock;
                    }
                    else if (sector * ChunkSize + z < HeightMin)
                    {
                        content = ContentCobbleStone;
                    }
                    else if (sector * ChunkSize + z > HeightMax)
                    {
                        content = ContentEmpty;
                    }
                    else
                    {
                        float density = getDensity(chunkMinX + x, chunkMinY + y, sector * ChunkSize + z);

                        if (density > 0.0f)
                        {
                            content = ContentCobbleStone;
                        }
                        else
                        {
                            if (sector * ChunkSize + z <= WaterLevel)
                            {
                                content = ContentWaterSource;
                            }
                            else
                            {
                                content = ContentEmpty;
                            }
                        }
                    }

                    sectors[sector].setCellContent(x, y, z, content);

                    if (content != ContentEmpty && content != ContentWater && content != ContentWaterSource)
                    {
                        height = sector * ChunkSize + z;
                    }
                }
            }

            heightMap[x + y * ChunkSize] = height;

            {
                int sector = height / ChunkSize;
                int z = height % ChunkSize;
                CCell * cell = sectors[sector].getCell(x, y, z);
                if (cell != nullptr && !cell->isEmpty() && !cell->isWater())
                {
                    if (sector * ChunkSize + z < WaterLevel)
                    {
                        sectors[sector].setCellContent(x, y, z, ContentDirt);
                    }
                    else
                    {
                        sectors[sector].setCellContent(x, y, z, ContentGrass);
                    }
                }
            }
            {
                int sector = (height - 1) / ChunkSize;
                int z = (height - 1) % ChunkSize;
                CCell * cell = sectors[sector].getCell(x, y, z);
                if (cell != nullptr && !cell->isEmpty() && !cell->isWater())
                {
                    sectors[sector].setCellContent(x, y, z, ContentDirt);
                }
            }
        }
    }
}


float CWorldGenerator2::getDensity(int x, int y, int z) const
{
/*
    if (z < HeightMin)
    {
        return 1.0f;
    }

    if (z > HeightMax)
    {
        return -1.0f;
    }
*/
    float density = m_perlinDensity.Get(x, y, z);
    float gradZ = 1.0f + 2.0f * static_cast<float>(HeightMin - z) / static_cast<float>(HeightMax - HeightMin);
    gradZ = (gradZ < -1.0f ? -1.0f : (gradZ > 1.0f ? 1.0f : gradZ));
    float absGradZ = std::abs(gradZ);
    density = absGradZ * gradZ + (1.0f - absGradZ) * density;
    return density;
}


int CWorldGenerator2::getElevation(int x, int y) const
{
    int elevation = HeightMin;

    for (int z = HeightMin; z <= HeightMax; ++z)
    {
        float density = getDensity(x, y, z);
        if (density > 0.0f)
        {
            elevation = z;
        }
    }

    return elevation;
}


CCell CWorldGenerator2::getCellContent(int x, int y, int z) const
{
    if (z <= 0)
    {
        return CCell(ContentBedRock);
    }

    if (z < HeightMin)
    {
        return CCell(ContentCobbleStone);
    }

    if (z > HeightMax)
    {
        return CCell(ContentEmpty);
    }

    float density = getDensity(x, y, z);

    if (density > 0.0f)
    {
        int elevation = getElevation(x, y);

        // Herbe au sommet
        if (z == elevation)
        {
            if (elevation >= WaterLevel)
            {
                return CCell(ContentGrass);
            }
            else
            {
                return CCell(ContentDirt);
            }
        }
        // Puis une couche de terre
        else if (z == elevation - 1)
        {
            return CCell(ContentDirt);
        }
        // Puis de la pierre
        else
        {
            return CCell(ContentCobbleStone);
        }
    }
    else
    {
        if (z <= WaterLevel)
        {
            return CCell(ContentWaterSource);
        }
        else
        {
            return CCell(ContentEmpty);
        }
    }
}


/**
 * Donne la température d'une cellule du monde.
 *
 * \param x Coordonnée X de la cellule dans le monde.
 * \param y Coordonnée Y de la cellule dans le monde.
 * \return Température de la cellule (entre 0 et 1).
 */

float CWorldGenerator2::getTemperature(int x, int y) const
{
    float val = m_perlinTemperature.Get(x, y);
    return (val + 1.0f) * 0.5f;
}


/**
 * Donne l'humidité d'une cellule du monde.
 *
 * \param x Coordonnée X de la cellule dans le monde.
 * \param y Coordonnée Y de la cellule dans le monde.
 * \return Humidité de la cellule (entre 0 et 1).
 */

float CWorldGenerator2::getHumidity(int x, int y) const
{
    float val = m_perlinHumidity.Get(x, y);
    return (val + 1.0f) * 0.5f;
}


/**
 * Donne le biome d'une cellule du monde.
 *
 * \param x Coordonnée X de la cellule dans le monde.
 * \param y Coordonnée Y de la cellule dans le monde.
 * \return Biome auquel appartient la cellule, déterminé à partir de
 *         sa température et de son humidité.
 */

TBiome CWorldGenerator2::getBiome(int x, int y) const
{/*
    float valE = m_perlinElevation.Get(x, y);
    if (valE <= OceanPerlinValue)
        return BiomeOcean;
    if (valE <= BeachPerlinValue)
        return BiomeBeach;
*/
    float temperature = getTemperature(x, y);
    float humidy = getHumidity(x, y);
    float precipitation = humidy * temperature;

    if (temperature < 0.1f)
        return BiomeIceDesert;

    if (precipitation < 0.2f)
    {
        if (temperature < 0.5f)
            return BiomeTundra;
        if (temperature < 0.95f)
            return BiomeSavanna;
        return BiomeDesert;
    }

    if ((precipitation > 0.5f) && (temperature < 0.7f))
        return BiomeSwampland;

    if (temperature < 0.5f)
        return BiomeTaiga;

    if (temperature < 0.97f)
    {
        if (precipitation < 0.35f)
            return BiomeShrubland;
        return BiomeForest;
    }

    if (precipitation < 0.45f)
        return BiomePlains;

    if (precipitation < 0.9f)
        return BiomeSeasonalForest;

    return BiomeRainForest;
}
