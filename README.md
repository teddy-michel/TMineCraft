TMineCraft
==========

Clone du jeu Minecraft.

Écrit en C++ et utilisant (en partie) le TEngine, l'objectif de ce projet est
de comprendre les mécanismes internes du jeu Minecraft (génération du terrain,
gestion de l'eau, de la luminosité, inventaire et crafting...) et d'utiliser
le TEngine pour réaliser un jeu basique mais fonctionnel.
